/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.material;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import net.worldsynth.common.color.WsColor;

public class MaterialRegistry {
	
	public static ArrayList<Material> REGISTER = new ArrayList<Material>();
	
	public MaterialRegistry(File worldSynthConfigDirectory) {
		registerMaterials(new File(worldSynthConfigDirectory, "materials"));
	}
	
	private void registerMaterials(File materialsDirectory) {
		//Air is an automatically registered material
		REGISTER.add(Material.AIR);
		
		//Load materials from files
		if(!materialsDirectory.exists()) {
			materialsDirectory.mkdir();
		}
		loadeMaterialsLib(materialsDirectory);
	}
	
	private void loadeMaterialsLib(File materialsDirectory) {
		
		if(!materialsDirectory.isDirectory()) {
			System.err.println("Lib directory \"" + materialsDirectory.getAbsolutePath() + "\" does not exist");
			return;
		}
		
		for(File sub: materialsDirectory.listFiles()) {
			if(sub.isDirectory()) {
				loadeMaterialsLib(sub);
			}
			else if(sub.getName().endsWith(".csv")) {
				try {
					loadMaterialsFromFile(sub);
				} catch (IOException e) {
					System.err.println("Problem occoured while trying to read material file: " + sub.getName());
					e.printStackTrace();
				}
			}
		}
	}
	
	private void loadMaterialsFromFile(File materialsFile) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(materialsFile));
		
		String line = null;
		while((line = reader.readLine()) != null) {
			//Remove any tabs in the line
			line = line.replace("\t", "");
			
			String[] separatedValues = line.split(";");
			
			//Expecting 5 entries
			if(separatedValues.length < 5) {
				continue;
			}
			
			String materialName = separatedValues[0];
			String materialIdName = separatedValues[1];
			int materialIdNumber;
			byte materialMetaNumber;
			WsColor materialColor;
			
			try {
				materialIdNumber = Integer.parseInt(separatedValues[2]);
			} catch (NumberFormatException e) {
				materialIdNumber = -1;
			}
			
			try {
				materialMetaNumber = Byte.parseByte(separatedValues[3]);
			} catch (NumberFormatException e) {
				materialMetaNumber = 0;
			}
			
			//Read color
			if(separatedValues[4].matches("#[0123456789ABCDEF]{6}") || separatedValues[4].matches("\\d{1,3},\\d{1,3},\\d{1,3}")) {
				materialColor = new WsColor(separatedValues[4]);
			}
			else {
				//No valid color value
				continue;
			}
			
			Material newMaterial = new Material(materialName, materialIdName, materialIdNumber, materialMetaNumber, materialColor);
			REGISTER.add(newMaterial);
		}
		
		reader.close();
	}
	
	public static Material getDefaultMaterial() {
		return REGISTER.get((REGISTER.size() > 1) ? 1 : 0);
	}
	
	public static Material getMaterial(int id, byte meta) {
		for(Material b: REGISTER) {
			if(id == b.getId()) {
				if(meta == b.getMeta() || b.getMeta() == -1) {
					return b;
				}
			}
		}
		return new Material("Unknown " + id + ":" + meta, "unknown", id, meta, WsColor.MAGENTA);
	}
	
	public static Material getMaterial(String idName) {
		if(idName != null) {
			for(Material b: REGISTER) {
				if(idName.equals(b.getIdName())) {
					return b;
				}
			}
		}
		
		return new Material("Unknown", idName, -1, WsColor.MAGENTA);
	}
	
	public static Material getMaterialByInternalId(int internalId) {
		if(internalId < 0 || internalId >= REGISTER.size()) {
			return new Material("Unknown i" + internalId, "unknown", internalId, WsColor.MAGENTA);
		}
		return REGISTER.get(internalId);
	}
	
	public static int getInternalId(Material material) {
		return REGISTER.indexOf(material);
	}
	
	public static Material[] getMaterialsAlphabetically() {
		//Sort materials alphabetically by name
		Material[] materialSort = new Material[REGISTER.size()];
		REGISTER.toArray(materialSort);
		
		Arrays.sort(materialSort);
		
		return materialSort;
	}
}
