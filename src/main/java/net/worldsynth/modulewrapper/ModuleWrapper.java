/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.modulewrapper;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import net.worldsynth.common.WorldSynthCore;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.ModuleIO;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleMacro;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.module.ModuleUnknown;
import net.worldsynth.module.AbstractModuleRegister.ModuleEntry;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.io.Element;

/**
 * The {@link ModuleWrapper} is a wrapper for the {@link AbstractModule}.
 */

public class ModuleWrapper {
	
	private Synth memberSynth = null;
	
	public static int nextWrapperID = 1;
	public int wrapperID;
	
	private String customName = "";
	
	public double posX;
	public double posY;
	
	public float wrapperWidth = 100;
	public float wrapperHeight = 35;
	
	public AbstractModule module = null;
	
	public LinkedHashMap<String, ModuleWrapperIO> wrapperInputs;
	public LinkedHashMap<String, ModuleWrapperIO> wrapperOutputs;
	
	public ModuleWrapper(ModuleEntry moduleEntry, Synth memberSynth, double posX, double posY) throws Exception {
		try {
			this.memberSynth = memberSynth;
			this.posX = posX;
			this.posY = posY;
			module = WorldSynthCore.constructModule(moduleEntry, this);
			buildWrapperForModule(module);
			wrapperID = nextWrapperID++;
		} catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | InstantiationException e) {
			throw new Exception("Could not create instance of ModuleWrapper", e);
		}
	}
	
	public ModuleWrapper(Class<? extends AbstractModule> moduleClass, Synth memberSynth, double posX, double posY) throws Exception {
		try {
			this.memberSynth = memberSynth;
			this.posX = posX;
			this.posY = posY;
			module = WorldSynthCore.constructModule(moduleClass, this);
			buildWrapperForModule(module);
			wrapperID = nextWrapperID++;
		} catch(IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | InstantiationException e) {
			throw new Exception("Could not create instance of ModuleWrapper", e);
		}
	}
	
	/** Initialize a macro from a synth file */
	public ModuleWrapper(File macroFile, Synth memberSynth, double posX, double posY) {
		this.memberSynth = memberSynth;
		this.posX = posX;
		this.posY = posY;
		module = new ModuleMacro(macroFile, this);
		buildWrapperForModule(module);
		wrapperID = nextWrapperID++;
	}
	
	public ModuleWrapper(Element element, Synth memberSynth) {
		this.memberSynth = memberSynth;
		fromElement(element);
		buildWrapperForModule(module);
	}
	
	public void reidentify() {
		wrapperID = nextWrapperID++;
	}
	
	public Synth getMembersynth() {
		return memberSynth;
	}
	
	private void buildWrapperForModule(AbstractModule module) {
		wrapperInputs = new LinkedHashMap<String, ModuleWrapperIO>();
		if(module.getInputs() != null) {
			for(int i = 0; i < module.getInputs().length; i++) {
				ModuleIO io = module.getInputs()[i];
				ModuleWrapperIO wrapperInput = new ModuleWrapperIO(io, 0, 10 + 15*i, true);
				wrapperInputs.put(io.getName(), wrapperInput);
			}
		}
		
		wrapperOutputs = new LinkedHashMap<String, ModuleWrapperIO>();
		if(module.getOutputs() != null) {
			for(int i = 0; i < module.getOutputs().length; i++) {
				ModuleIO io = module.getOutputs()[i];
				ModuleWrapperIO wrapperOutput = new ModuleWrapperIO(io, wrapperWidth-10, 10 + 15*i, false);
				wrapperOutputs.put(io.getName(), wrapperOutput);
			}
		}
		
		//calculate the graphical dimensions of the device
		wrapperHeight = (float) Math.max(wrapperInputs.size(), wrapperOutputs.size()) * 15 + 20;
		
		module.setOnModuleIoChange(e -> {
			reBuildWrapperForModule(module);
		});
	}
	
	private void reBuildWrapperForModule(AbstractModule module) {
		//Make a list of the current device connectors that are connected to the device so
		//connectors connected to later removed IOs can be removed.
		ModuleConnector[] currentConnectors = memberSynth.getConnectorsByWrapper(this);
		
		//Only create new ModuleWrapperIO objects for ModuleIO that is new, the rest can be reused.
		if(module.getInputs() != null) {
			LinkedHashMap<String, ModuleWrapperIO> newWrapperInputs = new LinkedHashMap<String, ModuleWrapperIO>();
			
			int i = 0;
			for(ModuleIO moduleInput: module.getInputs()) {
				String inputName = moduleInput.getName();
				
				//Check if there is an existing ModuleWrapperIO for this ModuleIO and overwrite
				//the new ModuleWrapperIO with the existing.
				//This check is done using the name of the ModuleIO.
				if(wrapperInputs.containsKey(inputName)) {
					ModuleWrapperIO wrapperInput = wrapperInputs.get(inputName);
					wrapperInput.posY = 10 + 15*i;
					newWrapperInputs.put(inputName, wrapperInput);
				}
				else {
					newWrapperInputs.put(inputName, new ModuleWrapperIO(moduleInput, 0, 10 + 15*i, true));
				}
				i++;
			}
			wrapperInputs = newWrapperInputs;
		}
		//Only create new ModuleWrapperIO objects for ModuleIO that is new, the rest can be reused.
		if(module.getOutputs() != null) {
			LinkedHashMap<String, ModuleWrapperIO> newWrapperOutputs = new LinkedHashMap<String, ModuleWrapperIO>();
			
			int i = 0;
			for(ModuleIO moduleOutput: module.getOutputs()) {
				String outputName = moduleOutput.getName();
				
				//Check if there is an existing ModuleWrapperIO for this ModuleIO and overwrite
				//the new ModuleWrapperIO with the existing.
				//This check is done using the name of the ModuleIO.
				if(wrapperOutputs.containsKey(outputName)) {
					ModuleWrapperIO wrapperOutput = wrapperOutputs.get(outputName);
					wrapperOutput.posY = 10 + 15*i;
					newWrapperOutputs.put(outputName, wrapperOutput);
				}
				else {
					newWrapperOutputs.put(outputName, new ModuleWrapperIO(moduleOutput, wrapperWidth-10, 10 + 15*i, false));
				}
				i++;
			}
			
			wrapperOutputs = newWrapperOutputs;
		}
		
		//calculate the graphical dimensions of the device
		wrapperHeight = (float) Math.max(wrapperInputs.size(), wrapperOutputs.size()) * 15 + 20;
		
		//TODO Consider if this should be done in the synth itself by a iochange listener
		//Cleanup the synth this module is a member of for the new unconnected connectors
		for(ModuleConnector mc: currentConnectors) {
			boolean connected = false;
			for(ModuleWrapperIO mio: wrapperInputs.values()) {
				if(mc.inputWrapperIo == mio) {
					connected = true;
					break;
				}
			}
			for(ModuleWrapperIO mio: wrapperOutputs.values()) {
				if(mc.outputWrapperIo == mio) {
					connected = true;
					break;
				}
			}
			
			if(!connected) {
				memberSynth.removeModuleConnector(mc);
			}
		}
	}
	
	public boolean isBypassable() {
		return module.isBypassable();
	}
	
	public void setBypassed(boolean bypass) {
		module.setBypassed(bypass);
	}
	
	public boolean isBypassed() {
		return module.isBypassed();
	}
	
	public void setCustomName(String customName) {
		this.customName = customName;
		if(module instanceof ModuleMacro) {
			((ModuleMacro) module).getMacroSynth().setName(customName);
		}
	}
	
	public String getCustomName() {
		return customName;
	}
	
	public ModuleWrapperIO isOverWrapperIO(float posX, float posY) {
		if(wrapperInputs != null) {
			for(ModuleWrapperIO io: wrapperInputs.values()) {
				if(posX > io.posX && posX < io.posX+ModuleWrapperIO.IO_RENDERSIZE && posY > io.posY && posY < io.posY+ModuleWrapperIO.IO_RENDERSIZE) {
					return io;
				}
			}
		}
		if(wrapperOutputs != null) {
			for(ModuleWrapperIO io: wrapperOutputs.values()) {
				if(posX > io.posX && posX < io.posX+ModuleWrapperIO.IO_RENDERSIZE && posY > io.posY && posY < io.posY+ModuleWrapperIO.IO_RENDERSIZE) {
					return io;
				}
			}
		}
		return null;
	}
	
	public ModuleWrapperIO getWrapperIoByModuleIo(ModuleIO moduleIo) {
		if(wrapperInputs.containsKey(moduleIo.getName())) {
			return wrapperInputs.get(moduleIo.getName());
		}
		else if(wrapperOutputs.containsKey(moduleIo.getName())) {
			return wrapperInputs.get(moduleIo.getName());
		}
		return null;
	}

	public String toString() {
		if(!customName.equals("")) {
			if(module.getModuleMetaTag() != null) {
				return customName + " (" + module.getModuleName() + " [" + module.getModuleMetaTag()+ "])";
			}
			return customName + " (" + module.getModuleName()+ ")";
		}
		if(module.getModuleMetaTag() != null) {
			return module.getModuleName() + " [" + module.getModuleMetaTag()+ "]";
		}
		return module.getModuleName();
	}
	
	public Element toElement() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("id", String.valueOf(wrapperID)));
		paramenterElements.add(new Element("name", customName));
		paramenterElements.add(new Element("x", String.valueOf(posX)));
		paramenterElements.add(new Element("y", String.valueOf(posY)));
		paramenterElements.add(new Element("bypass", String.valueOf(isBypassed())));
		paramenterElements.add(new Element("moduleclass", module.getModuleClassString()));
		paramenterElements.add(new Element("module", module.toElement()));
		
		Element moduleElement = new Element("modulewrapper", paramenterElements);
		return moduleElement;
	}
	
	public void fromElement(Element element) {
		
		boolean bypass = false;
		
		String moduleclass = null;
		Element moduleElement = null;
		
		for(Element e: element.elements) {
			if(e.tag.equals("name")) {
				if(e.content != null) {
					customName = e.content;
				}
			}
			else if(e.tag.equals("id")) {
				wrapperID = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("x")) {
				posX = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("y")) {
				posY = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("bypass")) {
				bypass = Boolean.parseBoolean(e.content);
			}
			else if(e.tag.equals("moduleclass")) {
				moduleclass = e.content;
				//TODO A1.3.0 Remove this module package name port
				if(moduleclass.startsWith("com")) {
					moduleclass = moduleclass.replaceFirst("com.booleanbyte", "net");
				}
			}
			else if(e.tag.equals("module")) {
				moduleElement = e.elements.get(0);
			}
		}
		if(moduleclass != null && moduleElement != null) {
			//Special case for macro module
			if(ModuleMacro.class.getName().equals(moduleclass)) {
				module = new ModuleMacro(this);
				module.fromElement(moduleElement);
			}
			else {
				try {
					for(ModuleEntry moduleEntry: WorldSynthCore.moduleRegister.getRegisteredModuleEntries()) {
						Class<? extends AbstractModule> c = moduleEntry.getModuleClass();
						if(c.getName().equals(moduleclass)) {
							AbstractModule module = WorldSynthCore.constructModule(c, moduleElement, this);
							this.module = module;
							break;
						}
					}
				} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
					e1.printStackTrace();
				}
			}
			
			//If the module is not in the library
			if(module == null) {
				module = new ModuleUnknown(moduleclass, moduleElement);
			}
		}
		
		setBypassed(bypass);
	}
	
	public AbstractDatatype buildInputData(ModuleInputRequest request) {
		//Transform inputrequests into output requests if possible
		ModuleWrapperIO input = getWrapperIoByModuleIo(request.getInput());
		ModuleConnector[] connectors = memberSynth.getConnectorsByWrapperIo(input);
		ModuleConnector connector = null;
		if(connectors.length > 0) {
			connector = connectors[0];
		}
		if(connector == null) {
			return null;
		}
		ModuleWrapper requestedWrapper = connector.outputWrapper;
		
		AbstractModule requestedModule = requestedWrapper.module;
		ModuleWrapperIO requestedWrapperIO = connector.outputWrapperIo;
		ModuleOutput requestedModuleOutput = (ModuleOutput) requestedWrapperIO.getIO();
		
		ModuleOutputRequest outputRequest = new ModuleOutputRequest(requestedModuleOutput, request.getData());
		
		//TODO Propagate buildlistener
		return WorldSynthCore.getModuleOutput(memberSynth, requestedWrapper, outputRequest, null);
	}
}
