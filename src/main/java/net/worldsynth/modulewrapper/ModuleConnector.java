/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.modulewrapper;

import java.util.ArrayList;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeNull;
import net.worldsynth.module.ModuleUnknown;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.io.Element;

public class ModuleConnector {
	
	/**
	 * The {@link ModuleWrapper} that the connector takes inputs from (is connected into the output of)
	 */
	public ModuleWrapper outputWrapper;
	/**
	 * The output {@link ModuleWrapperIO} of the {@link ModuleWrapper} that the connector is connected into
	 */
	public ModuleWrapperIO outputWrapperIo;
	
	/**
	 * The {@link ModuleWrapper} that the connector outputs to (is connected into the input of)
	 */
	public ModuleWrapper inputWrapper;
	/**
	 * The input {@link ModuleWrapperIO} of the {@link ModuleWrapper} that the connector is connected into
	 */
	public ModuleWrapperIO inputWrapperIo;
	
	public ModuleConnector(ModuleWrapper outputWrapper, ModuleWrapperIO outputWrapperIo, ModuleWrapper inputWrapper, ModuleWrapperIO inputWrapperIo) {
		this.outputWrapper = outputWrapper;
		this.outputWrapperIo = outputWrapperIo;
		this.inputWrapper = inputWrapper;
		this.inputWrapperIo = inputWrapperIo;
	}
	
	public ModuleConnector(Element element, Synth synth) {
		fromElement(element, synth);
	}

	/**
	 * Sets the {@link ModuleWrapper} and {@link ModuleWrapperIO} that the connector is connected into the output of
	 * @param outputWrapper
	 * @param outputWrapperIo
	 */
	public void setOutputWrapper(ModuleWrapper outputWrapper, ModuleWrapperIO outputWrapperIo) {
		this.outputWrapper = outputWrapper;
		this.outputWrapperIo = outputWrapperIo;
	}
	
	/**
	 * Sets the {@link ModuleWrapper} and {@link ModuleWrapperIO} that the connector is connected into the input of
	 * @param inputWrapper
	 * @param inputWrapperIo
	 */
	public void setInputWrapper(ModuleWrapper inputWrapper, ModuleWrapperIO inputWrapperIo) {
		this.inputWrapper = inputWrapper;
		this.inputWrapperIo = inputWrapperIo;
	}
	
	public AbstractDatatype getOutputDatatype() {
		if(outputWrapper != null) {
			return outputWrapperIo.getDatatype();
		}
		return null;
	}
	
	public AbstractDatatype getInputDatatype() {
		if(inputWrapper != null) {
			return inputWrapperIo.getDatatype();
		}
		return null;
	}
	
	public boolean verifyInputOutputType() {
		if(getInputDatatype() instanceof DatatypeNull || getOutputDatatype() instanceof DatatypeNull) {
			//Cannot verify that the datatypes are of same type if one or both of them are of null(unknown) type
			return false;
		}
		else if(getInputDatatype() instanceof DatatypeMultitype) {
			DatatypeMultitype multitype = (DatatypeMultitype) getInputDatatype();
			for(AbstractDatatype type: multitype.getDatatypes()) {
				if(type.getClass().equals(getOutputDatatype().getClass())) {
					return true;
				}
			}
		}
		else if(getInputDatatype().getDatatypeName().equals(getOutputDatatype().getDatatypeName())) {
			return true;
		}
		return false;
	}
	
	public boolean verifyConnection() {
		if(inputWrapperIo == null) {
			return false;
		}
		else if(outputWrapperIo == null) {
			return false;
		}
		return true;
	}
	
	public Element toElement() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("outputmoduleid", String.valueOf(outputWrapper.wrapperID)));
		paramenterElements.add(new Element("outputmoduleio", String.valueOf(outputWrapperIo.getName())));
		paramenterElements.add(new Element("inputmoduleid", String.valueOf(inputWrapper.wrapperID)));
		paramenterElements.add(new Element("inputmoduleio", String.valueOf(inputWrapperIo.getName())));
		
		Element moduleElement = new Element("moduleconnector", paramenterElements);
		return moduleElement;
	}
	
	//TODO A1.3.0 remove device flavour of tags (originally planned for A1.2.0 but pushed back to A1.3.0 because of the short life of A1.1.X)
	public void fromElement(Element element, Synth synth) {
		
		int inputModuleID = -1;
		String inputModuleIoName = "";
		int outputModuleID = -1;
		String outputModuleIoName = "";
		
		for(Element e: element.elements) {
			if(e.tag.equals("outputdeviceid") || e.tag.equals("outputmoduleid")) {
				outputModuleID = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("outputdeviceio") || e.tag.equals("outputmoduleio")) {
				outputModuleIoName = e.content;
			}
			else if(e.tag.equals("inputdeviceid") || e.tag.equals("inputmoduleid")) {
				inputModuleID = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("inputdeviceio") || e.tag.equals("inputmoduleio")) {
				inputModuleIoName = e.content;
			}
		}
		
		outputWrapper = synth.getWrapperByID(outputModuleID);
		if(outputWrapper.module instanceof ModuleUnknown) {
			ModuleUnknown m = (ModuleUnknown) outputWrapper.module;
			m.addOutput(outputModuleIoName);
		}
		outputWrapperIo = outputWrapper.wrapperOutputs.get(outputModuleIoName);
		
		inputWrapper = synth.getWrapperByID(inputModuleID);
		if(inputWrapper.module instanceof ModuleUnknown) {
			ModuleUnknown m = (ModuleUnknown) inputWrapper.module;
			m.addInput(inputModuleIoName);
		}
		inputWrapperIo = inputWrapper.wrapperInputs.get(inputModuleIoName);
	}
}
