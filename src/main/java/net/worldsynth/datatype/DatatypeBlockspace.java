/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeBlockspace extends AbstractDatatype {
	
	/**
	 * Internal material id
	 */
	public int[][][] blockspaceMaterialId;
	
	/**
	 * Corner coordinate
	 */
	public double x, y, z;
	
	/**
	 * Unit size of the blockspace
	 */
	public double width, height, length;
	
	/**
	 * The resolutions of units per blockspace point
	 */
	public double resolution;
	
	/**
	 * The number of points in the blockspace
	 */
	public int spacePointsWidth, spacePointsHeight, spacePointsLenght;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeBlockspace() {
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param lenght
	 * @param resolution The unit-distance between the points in the blockspace. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeBlockspace(double x, double y, double z, double width, double height, double lenght, double resolution) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.length = lenght;
		this.resolution = resolution;
		
		spacePointsWidth = (int) Math.ceil(width / resolution);
		spacePointsHeight = (int) Math.ceil(height / resolution);
		spacePointsLenght = (int) Math.ceil(lenght / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param lenght
	 */
	public DatatypeBlockspace(double x, double y, double z, double width, double height, double lenght) {
		this(x, y, z, width, height, lenght, 1);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 85, 85);
	}

	@Override
	public String getDatatypeName() {
		return "Blockspace";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeBlockspace dbs = new DatatypeBlockspace(x, y, z, width, height, length, resolution);
		if(blockspaceMaterialId != null) {
			int[][][] bsid= new int[spacePointsWidth][spacePointsHeight][spacePointsLenght];
			for(int u = 0; u < spacePointsWidth; u++) {
				for(int v = 0; v < spacePointsHeight; v++) {
					for(int w = 0; w < spacePointsLenght; w++) {
						bsid[u][v][w] = blockspaceMaterialId[u][v][w];
					}
				}
			}
			dbs.blockspaceMaterialId = bsid;
		}
		return dbs;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeBlockspace(x, y, z, width, height, length, resolution);
	}
}
