/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeHeightmap extends AbstractDatatype {
	
	public float[][] heightMap;
	
	/**
	 * Corner coordinate
	 */
	public final double x, z;
	
	/**
	 * Unit size of the heightmap
	 */
	public final double width, length;
	
	/**
	 * The resolutions of units per heightmap point
	 */
	public final double resolution;
	
	/**
	 * The number of points in the heightmap
	 */
	public final int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeHeightmap() {
		this(0.0, 0.0, 0.0, 0.0);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param z
	 * @param width
	 * @param length
	 */
	public DatatypeHeightmap(double x, double z, double width, double length) {
		this(x, z, width, length, 1.0);
	}
	
	/**
	 * @param x
	 * @param z
	 * @param width 
	 * @param length
	 * @param resolution The unit-distance between the points in the heightmap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeHeightmap(double x, double z, double width, double length, double resolution) {
		this.x = x;
		this.z = z;
		this.width = width;
		this.length = length;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(width / resolution);
		mapPointsLength = (int) Math.ceil(length / resolution);
	}
	
	public float[][] getHeightmap() {
		return heightMap;
	}
	
	public void setHeightmap(float[][] heightmap) {
		int paramWidth = heightmap.length;
		int paramLength = heightmap[0].length;
		if(paramWidth != mapPointsWidth || paramLength != mapPointsLength) {
			throw new IllegalArgumentException("Heightmap data has wrong size. Expected a " + mapPointsWidth + "x" + mapPointsLength + " map, got a " + paramWidth + "x" + paramLength  + " map.");
		}
		this.heightMap = heightmap;
	}
	
	public float getLocalHeight(int x, int z) {
		return heightMap[x][z];
	}
	
	public float getLocalLerpHeight(double x, double z) {
		int x1 = (int) x;
		int x2 = x1+1;
		x2 = Math.min(x2, mapPointsWidth-1);
		int z1 = (int) z;
		int z2 = z1+1;
		z2 = Math.min(z2, mapPointsLength-1);
		
		double u = x - Math.floor(x);
		double v = z - Math.floor(z);
		
		double val = (1.0-v)*(1.0-u)*heightMap[x1][z1] + (1.0-v)*u*heightMap[x2][z1] + v*(1.0-u)*heightMap[x1][z2] + v*u*heightMap[x2][z2];
		
		return (float) val;
	}
	
	public float getGlobalHeight(double x, double z) {
		x = (x - this.x) / resolution;
		z = (z - this.z) / resolution;
		
		return heightMap[(int) x][(int) z];
	}
	
	public boolean isLocalContained(int x, int z) {
		if(x < 0.0 || x >= mapPointsWidth || z < 0.0 || z >= mapPointsLength) {
			return false;
		}
		return true;
	}
	
	public boolean isGlobalContained(double x, double z) {
		if(x < this.x || x >= this.x + width || z < this.z || z >= this.z + length) {
			return false;
		}
		return true;
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 255, 85);
	}

	@Override
	public String getDatatypeName() {
		return "Heightmap";
	}

	@Override
	public DatatypeHeightmap clone() {
		DatatypeHeightmap dhm = new DatatypeHeightmap(x, z, width, length, resolution);
		if(heightMap != null) {
			float[][] hm = new float[mapPointsWidth][mapPointsLength];
			for(int u = 0; u < mapPointsWidth; u++) {
				for(int v = 0; v < mapPointsWidth; v++) {
					hm[u][v] = heightMap[u][v];
				}
			}
			dhm.heightMap = hm;
		}
		return dhm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeHeightmap(x, z, width, length, resolution);
	}
}
