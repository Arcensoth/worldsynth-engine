/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import java.util.ArrayList;

import javafx.scene.paint.Color;

public class DatatypeBiomemap extends AbstractDatatype {
	
	/**
	 * Internal biome id
	 */
	public int[][] biomeMap;
	
	/**
	 * Corner coordinate
	 */
	public double x, z;
	
	/**
	 * Unit size of the biomemap
	 */
	public double width, length;
	
	/**
	 * The resolutions of units per biomemap point
	 */
	public double resolution;
	
	/**
	 * The number of points in the biomemap
	 */
	public int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeBiomemap() {
	}
	
	/**
	 * @param x
	 * @param z
	 * @param width 
	 * @param length
	 * @param resolution The unit-distance between the points in the biomemap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeBiomemap(double x, double z, double width, double length, double resolution) {
		this.x = x;
		this.z = z;
		this.width = width;
		this.length = length;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(width / resolution);
		mapPointsLength = (int) Math.ceil(length / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param z
	 * @param width
	 * @param length
	 */
	public DatatypeBiomemap(double x, double z, double width, double length) {
		this(x, z, width, length, 1);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 85, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Biomemap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeBiomemap dbm = new DatatypeBiomemap(x, z, width, length, resolution);
		if(biomeMap != null) {
			int[][] bm = new int[mapPointsWidth][mapPointsLength];
			for(int u = 0; u < mapPointsWidth; u++) {
				for(int v = 0; v < mapPointsWidth; v++) {
					bm[u][v] = biomeMap[u][v];
				}
			}
			dbm.biomeMap = bm;
		}
		return dbm;
	}
	
	public int[] getBiomelist() {
		ArrayList<Integer> biomelist = new ArrayList<Integer>();

		for(int u = 0; u < biomeMap.length; u++) {
			for(int v = 0; v < biomeMap[0].length; v++) {
				int currentBiomeId = biomeMap[u][v];
				boolean biomeIsInList = false;
				for(int b: biomelist) {
					if(currentBiomeId == b) {
						biomeIsInList = true;
					}
				}
				if(!biomeIsInList) {
					biomelist.add(currentBiomeId);
				}
			}
		}
		
		int[] returnlist = new int[biomelist.size()];
		
		for(int i = 0; i < returnlist.length; i++) {
			returnlist[i] = biomelist.get(i);
		}
		
		return returnlist;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeBiomemap(x, z, width, length, resolution);
	}
}
