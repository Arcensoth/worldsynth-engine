/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeVectormap extends AbstractDatatype {
	
	public float[][][] vectorField;
	
	/**
	 * Corner coordinate
	 */
	public double x, z;
	
	/**
	 * Unit size of the vectorfield
	 */
	public double width, length;
	
	/**
	 * The resolutions of units per vectorfield point
	 */
	public double resolution;
	
	/**
	 * The number of points in the vectorfield
	 */
	public int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeVectormap() {
	}
	
	/**
	 * @param x
	 * @param z
	 * @param width 
	 * @param length
	 * @param resolution The unit-distance between the points in the vectorfield. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeVectormap(double x, double z, double width, double length, double resolution) {
		this.x = x;
		this.z = z;
		this.width = width;
		this.length = length;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(width / resolution);
		mapPointsLength = (int) Math.ceil(length / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param z
	 * @param width
	 * @param length
	 */
	public DatatypeVectormap(double x, double z, double width, double length) {
		this(x, z, width, length, 1);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(146, 0, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Vectormap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeVectormap dhm = new DatatypeVectormap(x, z, width, length, resolution);
		if(vectorField != null) {
			float[][][] hm = vectorField.clone();
			dhm.vectorField = hm;
		}
		return dhm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeVectormap(x, z, width, length, resolution);
	}
}
