/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeValuespace extends AbstractDatatype {
	
	public float[][][] valuespace;
	
	/**
	 * Corner coordinate
	 */
	public double x, y, z;
	
	/**
	 * Unit size of the valuespace
	 */
	public double width, height, lenght;
	
	/**
	 * The resolutions of units per valuespace point
	 */
	public double resolution;
	
	/**
	 * The number of points in the valuespace
	 */
	public int spacePointsWidth, spacePointsHeight, spacePointsLenght;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeValuespace() {
	}
	
	/**
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param lenght
	 * @param resolution The unit-distance between the points in the valuespace. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeValuespace(double x, double y, double z, double width, double height, double lenght, double resolution) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.width = width;
		this.height = height;
		this.lenght = lenght;
		this.resolution = resolution;
		
		spacePointsWidth = (int) Math.ceil(width / resolution);
		spacePointsHeight = (int) Math.ceil(height / resolution);
		spacePointsLenght = (int) Math.ceil(lenght / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param y
	 * @param z
	 * @param width
	 * @param height
	 * @param lenght
	 */
	public DatatypeValuespace(double x, double y, double z, double width, double height, double lenght) {
		this(x, y, z, width, height, lenght, 1);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(85, 255, 255);
	}

	@Override
	public String getDatatypeName() {
		return "Valuespace";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeValuespace dvs = new DatatypeValuespace(x, y, z, width, height, lenght, resolution);
		if(valuespace != null) {
			float[][][] vs = valuespace.clone();
			dvs.valuespace = vs;
		}
		return dvs;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeValuespace(x, y, z, width, height, length, resolution);
	}
}
