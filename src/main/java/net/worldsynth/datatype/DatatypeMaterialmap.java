/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.datatype;

import javafx.scene.paint.Color;

public class DatatypeMaterialmap extends AbstractDatatype {
	
	public int[][] materialMap;
	
	/**
	 * Corner coordinate
	 */
	public double x, z;
	
	/**
	 * Unit size of the materialmap
	 */
	public double width, length;
	
	/**
	 * The resolutions of units per materialmap point
	 */
	public double resolution;
	
	/**
	 * The number of points in the materialmap
	 */
	public int mapPointsWidth, mapPointsLength;
	
	/**
	 * Only intended for use defining device IO
	 */
	public DatatypeMaterialmap() {
	}
	
	/**
	 * @param x
	 * @param z
	 * @param width 
	 * @param length
	 * @param resolution The unit-distance between the points in the materialmap. Mainly used for preview in the
	 * standalone editor, but also usable in other applications where the resolution of the terrain is lower than 1 unit.
	 */
	public DatatypeMaterialmap(double x, double z, double width, double length, double resolution) {
		this.x = x;
		this.z = z;
		this.width = width;
		this.length = length;
		this.resolution = resolution;
		
		mapPointsWidth = (int) Math.ceil(width / resolution);
		mapPointsLength = (int) Math.ceil(length / resolution);
	}
	
	/**
	 * The resolution of the terrain is always 1 unit using this constructor, mainly intended for use in the Minecraft implementation
	 * of worldsynt, but also usable for other applications where the resolution of the terrain is always 1 unit.
	 * @param x
	 * @param z
	 * @param width
	 * @param length
	 */
	public DatatypeMaterialmap(double x, double z, double width, double length) {
		this(x, z, width, length, 1);
	}
	
	@Override
	public Color getDatatypeColor() {
		return Color.rgb(255, 170, 0);
	}

	@Override
	public String getDatatypeName() {
		return "Materialmap";
	}

	@Override
	public AbstractDatatype clone() {
		DatatypeMaterialmap dmm = new DatatypeMaterialmap(x, z, width, length, resolution);
		if(materialMap != null) {
			int[][] mm = materialMap.clone();
			dmm.materialMap = mm;
		}
		return dmm;
	}
	
	@Override
	public AbstractDatatype getPreviewDatatype(double x, double y, double z, double width, double height, double length, double resolution) {
		return new DatatypeMaterialmap(x, z, width, length, resolution);
	}
}
