/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.synth;

import java.util.ArrayList;

import net.worldsynth.extent.WorldExtentManager;
import net.worldsynth.modulewrapper.ModuleConnector;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.modulewrapper.ModuleWrapperIO;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.synth.SynthEvent;
import net.worldsynth.util.event.synth.SynthListener;
import net.worldsynth.util.event.synth.SynthEvent.SynthEventType;

public class Synth {
	
	private ArrayList<SynthListener> listeners = new ArrayList<SynthListener>();
	
	private String name = "";

	private WorldExtentManager extentManager = new WorldExtentManager();
	
	protected ArrayList<ModuleWrapper> wrapperList;
	protected ArrayList<ModuleConnector> moduleConnectorList;
	
	public Synth(String name) {
		this.name = name;
		wrapperList = new ArrayList<ModuleWrapper>();
		moduleConnectorList = new ArrayList<ModuleConnector>();
	}
	
	public Synth(Element element) {
		wrapperList = new ArrayList<ModuleWrapper>();
		moduleConnectorList = new ArrayList<ModuleConnector>();
		fromElement(element, this);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void addDevice(ModuleWrapper wrapper) {
		wrapperList.add(wrapper);
		notifyListenersWrapperAdded(wrapper);
	}
	
	public ModuleConnector[] removeDevice(ModuleWrapper wrapper) {
		//Get the device connectors currently connected to the device and remove them
		ModuleConnector[] removableModuleConnectors = getConnectorsByWrapper(wrapper);
		for(ModuleConnector c: removableModuleConnectors) {
			moduleConnectorList.remove(c);
		}
		
		//Remove device and notify listeners of it's removal
		wrapperList.remove(wrapper);
		notifyListenersWrapperRemoved(wrapper);
		
		//Return a list of the device connectors that was removed
		return removableModuleConnectors;
	}
	
	public ArrayList<ModuleWrapper> getWrapperList() {
		return wrapperList;
	}
	
	public ModuleWrapper getWrapperByID(int wrapperID) {
		for(ModuleWrapper d: wrapperList) {
			if(d.wrapperID == wrapperID) {
				return d;
			}
		}
		return null;
	}
	
	public void addModuleConnector(ModuleConnector moduleConnector) {
		moduleConnectorList.add(moduleConnector);
	}
	
	public ArrayList<ModuleConnector> getModuleConnectorList() {
		return moduleConnectorList;
	}

	public void removeModuleConnector(ModuleConnector moduleConnector) {
		moduleConnectorList.remove(moduleConnector);
	}
	
	public ModuleConnector[] getConnectorsByWrapperIo(ModuleWrapperIO io) {
		ArrayList<ModuleConnector> moduleConnectors = new ArrayList<ModuleConnector>();
		
		for(ModuleConnector c: moduleConnectorList) {
			if(c.inputWrapperIo == io) moduleConnectors.add(c);
			else if(c.outputWrapperIo == io) moduleConnectors.add(c);
		}
		
		ModuleConnector[] connectors = new ModuleConnector[moduleConnectors.size()];
		connectors = moduleConnectors.toArray(connectors);
		return connectors;
	}
	
	public ModuleConnector[] getConnectorsByWrapper(ModuleWrapper wrapper) {
		ArrayList<ModuleConnector> moduleConnectors = new ArrayList<ModuleConnector>();
		
		for(ModuleConnector c: moduleConnectorList) {
			if(c.inputWrapper == wrapper) moduleConnectors.add(c);
			else if(c.outputWrapper == wrapper) moduleConnectors.add(c);
		}
		
		ModuleConnector[] connectors = new ModuleConnector[moduleConnectors.size()];
		connectors = moduleConnectors.toArray(connectors);
		return connectors;
	}
	
	public void addSynthListerner(SynthListener listener) {
		listeners.add(listener);
	}
	
	public void removeSynthListener(SynthListener listener) {
		listeners.remove(listener);
	}
	
	public boolean containsWrapper(ModuleWrapper wrapper) {
		for(ModuleWrapper d: wrapperList) {
			if(d == wrapper) {
				return true;
			}
		}
		return false;
	}
	
	private void notifyListenersWrapperAdded(ModuleWrapper addedWrapper) {
		for(SynthListener listener: listeners) {
			listener.deviceAdded(new SynthEvent(SynthEventType.MODULE_ADDED, addedWrapper, this));
		}
	}
	
	private void notifyListenersWrapperRemoved(ModuleWrapper removedWrapper) {
		for(SynthListener listener: listeners) {
			listener.deviceRemoved(new SynthEvent(SynthEventType.MODULE_REMOVES, removedWrapper, this));
		}
	}
	
	private void notifyListenersWrapperModified(ModuleWrapper modifiedWrapper) {
		for(SynthListener listener: listeners) {
			listener.deviceModified(new SynthEvent(SynthEventType.MODULE_MODIFIED, modifiedWrapper, this));
		}
	}
	
	protected void setModuleConnectorList(ArrayList<ModuleConnector> moduleConnectorList) {
		this.moduleConnectorList = moduleConnectorList;
	}
	
	public Element toElement() {
		ArrayList<Element> wrapperElements = new ArrayList<Element>();
		for(ModuleWrapper mw: wrapperList) {
			wrapperElements.add(mw.toElement());
		}
		
		ArrayList<Element> moduleConnectorElements = new ArrayList<Element>();
		for(ModuleConnector mc: moduleConnectorList) {
			moduleConnectorElements.add(mc.toElement());
		}
		
		ArrayList<Element> synthElements = new ArrayList<Element>();
		synthElements.add(new Element("name", name));
		synthElements.add(new Element("extents", getExtentManager().toElementList()));
		synthElements.add(new Element("nextmoduleid", String.valueOf(ModuleWrapper.nextWrapperID)));
		synthElements.add(new Element("modules", wrapperElements));
		synthElements.add(new Element("moduleconnetctors", moduleConnectorElements));
		
		Element synthElement = new Element("synth", synthElements);
		
		return synthElement;
	}
	
	protected void fromElement(Element synthElement, Synth parentSynth) {
		//TODO A1.3.0 remove root tag (originally planned for A1.2.0 but pushed back to A1.3.0 because of the short life of A1.1.X)
		if(synthElement.tag.equals("root") || synthElement.tag.equals("synth")) {
			
			Element devicesElement = null;
			Element deviceConnectorsElement = null;
			
			for(Element e: synthElement.elements) {
				if(e.tag.equals("name") && parentSynth == this) {
					if(e.content != null) {
						name = e.content;
					}
				}
				else if(e.tag.equals("extents") && parentSynth == this) {
					extentManager = new WorldExtentManager(e);
				}
				//TODO A1.3.0 Remove "nextdeviceid", "devices" and "deviceconnectors" tag compatability
				else if(e.tag.equals("nextdeviceid") || e.tag.equals("nextmoduleid")) {
					if(ModuleWrapper.nextWrapperID < Integer.parseInt(e.content)) {
						ModuleWrapper.nextWrapperID = Integer.parseInt(e.content);
					}
				}
				else if(e.tag.equals("devices") || e.tag.equals("modules")) {
					devicesElement = e;
				}
				else if(e.tag.equals("deviceconnectors") || e.tag.contentEquals("moduleconnetctors")) {
					deviceConnectorsElement = e;
				}
			}
			
			//Extract devices and connectors
			if(devicesElement != null) {
				wrapperList = extractModuleWrappers(devicesElement, parentSynth);
				if(deviceConnectorsElement != null) {
					setModuleConnectorList(extractModuleConnectors(deviceConnectorsElement));
					cleanupModuleConnectors();
				}
			}
		}
	}
	
	protected ArrayList<ModuleWrapper> extractModuleWrappers(Element wrapperElement, Synth parentSynth) {
		ArrayList<ModuleWrapper> devices = new ArrayList<ModuleWrapper>();
		
		if(wrapperElement.elements.size() > 0) {
			for(Element de: wrapperElement.elements) {
				devices.add(new ModuleWrapper(de, parentSynth));
			}
		}
		
		return devices;
	}
	
	protected ArrayList<ModuleConnector> extractModuleConnectors(Element moduleConnectorsElement) {
		ArrayList<ModuleConnector> modulesConnectors = new ArrayList<ModuleConnector>();
		
		if(moduleConnectorsElement.elements.size() > 0) {
			for(Element dce: moduleConnectorsElement.elements) {
				modulesConnectors.add(new ModuleConnector(dce, this));
			}
		}
		
		return modulesConnectors;
	}
	
	protected void cleanupModuleConnectors() {
		ArrayList<ModuleConnector> invalidConnectors = new ArrayList<ModuleConnector>();
		
		for(ModuleConnector dc: moduleConnectorList) {
			if(!dc.verifyConnection()) {
				invalidConnectors.add(dc);
			}
		}
		
		for(ModuleConnector dc: invalidConnectors) {
			moduleConnectorList.remove(dc);
		}
	}
	
	public WorldExtentManager getExtentManager() {
		return extentManager;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Improve equals check
		return super.equals(obj);
	}
}
