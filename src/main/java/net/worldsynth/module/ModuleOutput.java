/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import net.worldsynth.datatype.AbstractDatatype;

/**
 * This is an extension of {@link ModuleIO} for module outputs 
 */
public class ModuleOutput extends ModuleIO {
	
	public ModuleOutput(AbstractDatatype datat, String name) {
		super(datat, name);
	}
	
	public ModuleOutput(AbstractDatatype datat, String name, boolean visible) {
		super(datat, name, visible);
	}
}
