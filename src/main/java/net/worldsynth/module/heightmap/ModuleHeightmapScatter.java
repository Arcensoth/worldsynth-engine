/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.common.math.MathHelperScalar;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentParameter;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.BooleanParameterCheckbox;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.LongParameterField;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapScatter extends AbstractModule {
	
	private long seed;
	private WorldExtentParameter featureExtent;
	private boolean enableScaling = true;
	private boolean enableRotation = true;
	private double smallestScale = 0.0;
	private double largestScale = 1.0;
	private BlendOperation blendOperation = BlendOperation.ADDITION;
	private ScatterBlendOperation scatterBlendOperation = ScatterBlendOperation.ADDITION;
	private SampleMethod sampleMethod = SampleMethod.BILINEAR;
	
	private final int permutationSize = 256;
	
	/**
	 * This contains a double duplicated permutation table
	 */
	private int[][] dp;
	
	public ModuleHeightmapScatter() {
		seed = new Random().nextLong();
		dp = createPermutatationTable(permutationSize, 2, seed);
	}
	
	@Override
	protected void postInit() {
		featureExtent = getNewExtentParameter();
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap heightmapRequestData = (DatatypeHeightmap) outputRequest.data;
		
		WorldExtent extent = featureExtent.getExtent();
		DatatypeHeightmap featureHeightmapRequestData = new DatatypeHeightmap(extent.getX(), extent.getZ(), extent.getWidth(), extent.getLength(), heightmapRequestData.resolution);
		
		double x = heightmapRequestData.x - extent.getWidth();
		double z = heightmapRequestData.z - extent.getLength();
		double width = heightmapRequestData.width + extent.getWidth()*2D;
		double length = heightmapRequestData.length + extent.getLength()*2D;
		DatatypeFeaturemap scatterFeaturmapRequestData = new DatatypeFeaturemap(x, z, width, length);
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), heightmapRequestData));
		inputRequests.put("feature", new ModuleInputRequest(getInput(1), featureHeightmapRequestData));
		inputRequests.put("placement", new ModuleInputRequest(getInput(2), scatterFeaturmapRequestData));
		inputRequests.put("mask", new ModuleInputRequest(getInput(3), heightmapRequestData));
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		
		//----------READ INPUTS----------//
		
		DatatypeHeightmap heightmap = (DatatypeHeightmap) inputs.get("input");
		DatatypeHeightmap fetaureHeightmap = (DatatypeHeightmap) inputs.get("feature");
		DatatypeFeaturemap scatterFeaturemap = (DatatypeFeaturemap) inputs.get("placement");
		DatatypeHeightmap maskHeightmap = (DatatypeHeightmap) inputs.get("mask");
		
		if(heightmap == null || fetaureHeightmap == null || scatterFeaturemap == null) {
			return null;
		}
		
		//----------BUILD----------//
		
		//Make a clean scattermap
		DatatypeHeightmap scaterHeightmap = new DatatypeHeightmap(heightmap.x, heightmap.z, heightmap.width, heightmap.length, heightmap.resolution);
		scaterHeightmap.setHeightmap(new float[scaterHeightmap.mapPointsWidth][scaterHeightmap.mapPointsLength]);
		
		for(int i = 0; i < scatterFeaturemap.points.length; i++) {
			double x = scatterFeaturemap.points[i][0];
			double z = scatterFeaturemap.points[i][1];
			
			double rotate = 0.0;
			double scale = 1.0;
			if(enableScaling) {
				scale = smallestScale + hash((int) x, (int) z, 0) * (largestScale - smallestScale);
			}
			if(enableRotation) {
				rotate = hash((int) x, (int) z, 1)*Math.PI*2.0;
			}
			placeHeightmap(scaterHeightmap, fetaureHeightmap, x, z, scale, rotate);
		}
		
		//Merge scatterHeightmap and heightmap with mask
		if(maskHeightmap == null) {
			for(int u = 0; u < heightmap.mapPointsWidth; u++) {
				for(int v = 0; v < heightmap.mapPointsLength; v++) {
					heightmap.heightMap[u][v] = MathHelperScalar.clamp(mix(heightmap.heightMap[u][v], scaterHeightmap.heightMap[u][v], blendOperation, 1.0f), 0.0f, 1.0f);
				}
			}
		}
		else {
			for(int u = 0; u < heightmap.mapPointsWidth; u++) {
				for(int v = 0; v < heightmap.mapPointsLength; v++) {
					heightmap.heightMap[u][v] = MathHelperScalar.clamp(mix(heightmap.heightMap[u][v], scaterHeightmap.heightMap[u][v], blendOperation, maskHeightmap.heightMap[u][v]), 0.0f, 1.0f);
				}
			}
		}
		
		
		return heightmap;
	}
	
	private void placeHeightmap(DatatypeHeightmap scatterHeightmap, DatatypeHeightmap featureHeightmap, double x, double z, double scale, double rotation) {
		x = (x - scatterHeightmap.x) / scatterHeightmap.resolution;
		z = (z - scatterHeightmap.z) / scatterHeightmap.resolution;
		
		//Create the transform matrix for mapping points in the scatter heightmap to points in the feature heightmap
		double[][] inverse_transform = {
				{Math.cos(-rotation)/scale, -Math.sin(-rotation)/scale, (-x*Math.cos(-rotation)+z*Math.sin(-rotation))/scale + (double)featureHeightmap.mapPointsWidth/2},
				{Math.sin(-rotation)/scale,  Math.cos(-rotation)/scale, (-x*Math.sin(-rotation)-z*Math.cos(-rotation))/scale + (double)featureHeightmap.mapPointsLength/2},
				{0                        , 0                         , 1          }};
		
		int[] bounds = bounds(x, z, featureHeightmap.mapPointsWidth, featureHeightmap.mapPointsLength, scale, rotation);
		
		int minX = Math.max(bounds[0], 0);
		int maxX = Math.min(bounds[1], scatterHeightmap.mapPointsWidth);
		int minZ = Math.max(bounds[2], 0);
		int maxZ = Math.min(bounds[3], scatterHeightmap.mapPointsLength);
		
		for(int u = minX; u < maxX; u++) {
			for(int v = minZ; v < maxZ; v++) {
				double[] sample = applyTransform(u, v, inverse_transform);
				if(!featureHeightmap.isLocalContained((int) sample[0], (int) sample[1])) continue;
				switch(sampleMethod) {
				case CLOSEST:
					scatterHeightmap.heightMap[u][v] = mix(scatterHeightmap.getLocalHeight(u, v), featureHeightmap.getLocalHeight((int) sample[0], (int) sample[1]) * (float)scale, scatterBlendOperation, 1.0f);
					break;
				case BILINEAR:
					scatterHeightmap.heightMap[u][v] = mix(scatterHeightmap.getLocalHeight(u, v), featureHeightmap.getLocalLerpHeight(sample[0], sample[1]) * (float)scale, scatterBlendOperation, 1.0f);
					break;
				}
			}
		}
	}
	
	//Returns {minX, maxX, minZ, maxZ}
	private int[] bounds(double x, double z, double w, double l, double scale, double rotate) {
		//Create the transform matrix for mapping points in the feature heightmap to points in the scatter heightmap
		double[][] transform = {
				{scale*Math.cos(rotate), -scale*Math.sin(rotate), (-w*scale*Math.cos(rotate)+l*scale*Math.sin(rotate))/2+x},
				{scale*Math.sin(rotate),  scale*Math.cos(rotate), (-w*scale*Math.sin(rotate)-l*scale*Math.cos(rotate))/2+z},
				{0                     , 0                      , 1                                                       }};
		
		double[] c0 = applyTransform(0, 0, transform);
		double[] c1 = applyTransform(w, 0, transform);
		double[] c2 = applyTransform(w, l, transform);
		double[] c3 = applyTransform(0, l, transform);
		
		int minX = (int) Math.floor(min(c0[0], c1[0], c2[0], c3[0]));
		int maxX = (int) Math.floor(max(c0[0], c1[0], c2[0], c3[0]));
		int minZ = (int) Math.floor(min(c0[1], c1[1], c2[1], c3[1]));
		int maxZ = (int) Math.floor(max(c0[1], c1[1], c2[1], c3[1]));
		
		return new int[] {minX, maxX, minZ, maxZ};
	}
	
	private double min(double...values) {
		double min = values[0];
		
		for(double val: values) {
			min = Math.min(min, val);
		}
		
		return min;
	}
	
	private double max(double...values) {
		double max = values[0];
		
		for(double val: values) {
			max = Math.max(max, val);
		}
		
		return max;
	}
	
	private double[] applyTransform(double x, double z, double[][] transform) {
		return new double[] {
			x*transform[0][0] + z*transform[0][1] + transform[0][2],
			x*transform[1][0] + z*transform[1][1] + transform[1][2]
		};
	}
	
	private float mix(float i0, float i1, BlendOperation operation, float fac) {
		float o = 0;
		switch (operation) {
		case ADDITION:
			o = i0 + i1 * fac;
			break;
		case SUBTRACTION:
			o = i0 - i1 * fac;
			break;
		case MULTIPLICATION:
			o = i0 * i1 * fac;
			break;
		case DIVISION:
			o = i0 / i1 * fac;
			break;
		case MODULO:
			o = i0 % i1 * fac;
			break;
		case AVERAGE:
			o = (i0 + i1 * fac) / (1 + fac);
			break;
		case MAX:
			o = Math.max(i0, i1 * fac);
			break;
		case MIN:
			o = Math.min(i0, i1 * fac);
			break;
		case OWERFLOW:
			o = (i0 + i1 * fac);
			o = o % 1.0f;
			break;
		case UNDERFLOW:
			o = (i0 - i1 * fac);
			o = o < 0.0f ? o + 1.0f : o;
			break;
		}
		
		return o;
	}
	
	private float mix(float i0, float i1, ScatterBlendOperation operation, float fac) {
		float o = 0;
		switch (operation) {
		case ADDITION:
			o = i0 + i1 * fac;
			break;
		case MAX:
			o = Math.max(i0, i1 * fac);
			break;
		case OWERFLOW:
			o = (i0 + i1 * fac);
			o = o % 1.0f;
			break;
		}
		
		return o;
	}
	
	private int[][] createPermutatationTable(int size, int dim, long seed) {
		int[][] dp = new int[size*2][dim];
		
		for(int i = 0; i < dim; i++) {
			//Create a random generator with supplied seed
			Random r = new Random(seed + i);
			
			//Generate a list containing every integer from 0 inclusive to size exlusive
			ArrayList<Integer> valueTabel = new ArrayList<Integer>();
			for(int j = 0; j < size; j++) {
				valueTabel.add(j);
			}
			
			//create the permutation table
			int[] permutationTable = new int[size];
			
			//Insert the values from the valueTable into the permutation table in a random order
			int pi = 0;
			while(valueTabel.size() > 0) {
				int index = r.nextInt(valueTabel.size());
				permutationTable[pi] = valueTabel.get(index);
				valueTabel.remove(index);
				pi++;
			}
			
			for(int k = 0; k < dp.length; k++) {
				dp[k][i] = permutationTable[k%permutationSize];
			}
		}
		
		return dp;
	}
	
	private double hash(int x, int z, int table) {
		if(x >= 0) x = x % permutationSize;
		else x = (permutationSize-1)+((x+1)%permutationSize);
		if(z >= 0) z = z % permutationSize;
		else z = (permutationSize-1)+((z+1)%permutationSize);
		
		return (double) dp[dp[x][table]+z][table] / (double) permutationSize;
	}

	@Override
	public String getModuleName() {
		return "Heightmap scatter";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Input"),
				new ModuleInput(new DatatypeHeightmap(), "Feature"),
				new ModuleInput(new DatatypeFeaturemap(), "Placement"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		//////////Parameters //////////
		ExtentParameterDropdownSelector parameterExtent = featureExtent.getDropdownSelector("Feature extent");
		BooleanParameterCheckbox parameterEnableRatate = new BooleanParameterCheckbox("Rotate", enableRotation);
		BooleanParameterCheckbox parameterEnableScale = new BooleanParameterCheckbox("Scale", enableScaling);
		DoubleParameterSlider parameterSmallestScale = new DoubleParameterSlider("Smallest scale", 0.0, 2.0, smallestScale);
		DoubleParameterSlider parameterLargestScale = new DoubleParameterSlider("Largest scale", 0.0, 2.0, largestScale);
		EnumParameterDropdownSelector<BlendOperation> parameterBlendOperation = new EnumParameterDropdownSelector<BlendOperation>("Blend", BlendOperation.class, blendOperation);
		EnumParameterDropdownSelector<ScatterBlendOperation> parameterScatterBlendOperation = new EnumParameterDropdownSelector<ScatterBlendOperation>("Scatter blend", ScatterBlendOperation.class, scatterBlendOperation);
		EnumParameterDropdownSelector<SampleMethod> parameterSampleMehtod = new EnumParameterDropdownSelector<SampleMethod>("Sample method", SampleMethod.class, sampleMethod);
		LongParameterField parameterSeed = new LongParameterField("Seed", seed);
		
		parameterExtent.addToGrid(pane, 0);
		parameterEnableRatate.addToGrid(pane, 1);
		parameterEnableScale.addToGrid(pane, 2);
		parameterSmallestScale.addToGrid(pane, 3);
		parameterLargestScale.addToGrid(pane, 4);
		parameterBlendOperation.addToGrid(pane, 5);
		parameterScatterBlendOperation.addToGrid(pane, 6);
		parameterSampleMehtod.addToGrid(pane, 7);
		parameterSeed.addToGrid(pane, 8);
			
		//////////
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			featureExtent.setExtent(parameterExtent.getValue());
			
			enableRotation = parameterEnableRatate.getValue();
			enableScaling = parameterEnableScale.getValue();
			smallestScale = parameterSmallestScale.getValue();
			largestScale = parameterLargestScale.getValue();
			
			blendOperation = parameterBlendOperation.getValue();
			scatterBlendOperation = parameterScatterBlendOperation.getValue();
			sampleMethod = parameterSampleMehtod.getValue();
			
			//Sets the seed and regenerates the permutationtable
			seed = parameterSeed.getValue();
			dp = createPermutatationTable(permutationSize, 2, seed);
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("seed", String.valueOf(seed)));
		paramenterElements.add(new Element("rotate", String.valueOf(enableRotation)));
		paramenterElements.add(new Element("scale", String.valueOf(enableScaling)));
		paramenterElements.add(new Element("smallestscale", String.valueOf(smallestScale)));
		paramenterElements.add(new Element("largestscale", String.valueOf(largestScale)));
		paramenterElements.add(new Element("extent", featureExtent.getExtentAsString()));
		paramenterElements.add(new Element("blend", blendOperation.name()));
		paramenterElements.add(new Element("scatterblend", scatterBlendOperation.name()));
		paramenterElements.add(new Element("samplemethod", sampleMethod.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("seed")) {
				seed = Long.parseLong(e.content);
				dp = createPermutatationTable(permutationSize, 2, seed);
			}
			else if(e.tag.equals("rotate")) {
				enableRotation = Boolean.parseBoolean(e.content);
			}
			else if(e.tag.equals("scale")) {
				enableScaling = Boolean.parseBoolean(e.content);
			}
			else if(e.tag.equals("smallestscale")) {
				smallestScale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("largestscale")) {
				largestScale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("extent")) {
				featureExtent.setExtentAsString(e.content);
			}
			else if(e.tag.equals("blend")) {
				for(BlendOperation type: BlendOperation.values()) {
					if(e.content.equals(type.name())) {
						blendOperation = type;
						break;
					}
				}
			}
			else if(e.tag.equals("scatterblend")) {
				for(ScatterBlendOperation type: ScatterBlendOperation.values()) {
					if(e.content.equals(type.name())) {
						scatterBlendOperation = type;
						break;
					}
				}
			}
			else if(e.tag.equals("samplemethod")) {
				for(SampleMethod type: SampleMethod.values()) {
					if(e.content.equals(type.name())) {
						sampleMethod = type;
						break;
					}
				}
			}
		}
	}
	
	private enum BlendOperation {
		ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, MODULO, AVERAGE, MAX, MIN, OWERFLOW, UNDERFLOW;
	}
	
	private enum ScatterBlendOperation {
		ADDITION, MAX, OWERFLOW;
	}
	
	private enum SampleMethod {
		CLOSEST, BILINEAR; // BICUBIC;
	}
}
