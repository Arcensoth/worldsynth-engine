/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.IntegerParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapSmothen extends AbstractModule {
	
	private int kernelRadius = 2;

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] outputMap = new float[mpw][mpl];
		int activeKernelRadius = (int) Math.floor((double) kernelRadius/res);
		double[][] kernel = generateKernel(activeKernelRadius);
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float o = 0;
				if(isBypassed()) {
					outputMap[u][v] = inputMap[u+activeKernelRadius][v+activeKernelRadius];
					continue;
				}
				else {
					o = (float)convolution(kernel, activeKernelRadius, inputMap, u+activeKernelRadius, v+activeKernelRadius);
				}
				o = Math.min(o, 1);
				o = Math.max(o, 0);
				outputMap[u][v] = o;
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					outputMap[u][v] = outputMap[u][v] * mask[u][v] + inputMap[u+activeKernelRadius][v+activeKernelRadius] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.setHeightmap(outputMap);
		
		return requestData;
	}
	
	private double[][] generateKernel(int kernelRadius) {
		int size = 1 + (int)kernelRadius * 2;
		double[][] kernel = new double[size][size];
		
		if(kernelRadius == 0) {
			kernel[0][0] = 1.0;
			return kernel;
		}
		
		for(int u = 0; u < size; u++) {
			for(int v = 0; v < size; v++) {
				double dist = Math.sqrt(Math.pow(u-kernelRadius, 2) + Math.pow(v-kernelRadius, 2));
				if(dist > kernelRadius) dist = kernelRadius;
				double a = 1.0-(dist/kernelRadius);
				kernel[u][v] = a;
			}
		}
		
		return kernel;
	}
	
	private double convolution(double[][] kernel, int kernelRadius, float[][] heightmap, int x, int y) {
		
		double n = 0;
		double sum = 0;
		
		for(int kx = -kernelRadius; kx <= kernelRadius; kx++) {
			for(int ky = -kernelRadius; ky <= kernelRadius; ky++) {
				
				double a = (double)heightmap[x+kx][y+ky];
				double k = kernel[kx+kernelRadius][ky+kernelRadius];
				sum += a*k;
				n += k;
			}
		}
		
		return sum/(double)n;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		DatatypeHeightmap d = (DatatypeHeightmap)outputRequest.data;
		
		double expandRadius = Math.floor((double)kernelRadius/d.resolution)*d.resolution;
		
		DatatypeHeightmap inputRequestDatatype = new DatatypeHeightmap(d.x-expandRadius, d.z-expandRadius, d.width+3.0*expandRadius, d.length+3.0*expandRadius, d.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput(0), inputRequestDatatype));
		inputRequests.put("mask", new ModuleInputRequest(getInput(1), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Smothen";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		//////////Scale //////////
		
		IntegerParameterSlider parameterRadius = new IntegerParameterSlider("Radius", 0, 20, kernelRadius);
		
		try {
			parameterRadius.addToGrid(pane, 0);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			kernelRadius = parameterRadius.getValue();
		};
		
		return applyHandler;
		
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("radius", String.valueOf(kernelRadius)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			//TODO A1.3.0 Remove scale tag (originally planned for A1.2.0 but pushed back to A1.3.0 because of the short life of A1.1.X)
			if(e.tag.equals("radius") || e.tag.equals("scale")) {
				kernelRadius = Integer.parseInt(e.content);
			}
		}
	}
}
