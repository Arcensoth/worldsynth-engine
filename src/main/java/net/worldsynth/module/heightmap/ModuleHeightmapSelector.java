/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.IntegerParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapSelector extends AbstractModule {
	
	int inputPorts = 5;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		if(inputs.size() <= 1) {
			return null;
		}
		//First input is always the biomemap
		DatatypeBiomemap biomemapData = (DatatypeBiomemap) inputs.get("inputselector");
		
		int[] inputlist = biomemapData.getBiomelist();
		
		for(int i = 0; i < inputlist.length; i++) {
			int inputToChek = inputlist[i]+1;
			if(inputs.get("input"+inputToChek) == null) {
				return null;
			}
		}
		
		int[][] biomemap = biomemapData.biomeMap;
		
		float[][] map = new float[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float o = 0.0f;
				int b = biomemap[u][v];
				
				o = ((DatatypeHeightmap) inputs.get("input"+(b+1))).getHeightmap()[u][v];
				
				o = Math.min(o, 1);
				o = Math.max(o, 0);
				map[u][v] = o;
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap outputRequestData = (DatatypeHeightmap) outputRequest.data;
		
		DatatypeBiomemap biomemapInputRequestData = new DatatypeBiomemap(outputRequestData.x, outputRequestData.z, outputRequestData.width, outputRequestData.length, outputRequestData.resolution);
		ModuleInputRequest biomemapInputRequest = new ModuleInputRequest(getInput(0), biomemapInputRequestData);
		
		//prebuild the biomemap to optimize what input's will actually be used if there are many inputs
		DatatypeBiomemap selectorMapData = (DatatypeBiomemap) buildInputData(biomemapInputRequest);
		if(selectorMapData == null) {
			return inputRequests;
		}
		int[][] selectorBiomemap = selectorMapData.biomeMap;
		
		ArrayList<Integer> biomeIdsToCheck = new ArrayList<Integer>();
		for(int i = 0; i < inputPorts; i++) {
			biomeIdsToCheck.add(i);
		}
		
		boolean[] requestInput = new boolean[inputPorts];
		
		for(int u = 0; u < selectorBiomemap.length; u++) {
			for(int v = 0; v < selectorBiomemap[0].length; v++) {
				int currentBiomeId = selectorBiomemap[u][v];
				for(int b = 0; b < biomeIdsToCheck.size(); b++) {
					if(currentBiomeId == biomeIdsToCheck.get(b)) {
						requestInput[currentBiomeId] = true;
						biomeIdsToCheck.remove(b);
						break;
					}
				}
			}
		}
		
		inputRequests.put("inputselector", biomemapInputRequest);
		for(int i = 0; i < inputPorts; i++) {
			if(requestInput[i]) {
				inputRequests.put("input"+i, new ModuleInputRequest(getInput(i+1), outputRequest.data));
			}
		}
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Selector";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] in = new ModuleInput[inputPorts+1];
		in[0] = new ModuleInput(new DatatypeBiomemap(), "Selector input");
		for(int i = 0; i < inputPorts; i++) {
			in[i+1] = new ModuleInput(new DatatypeHeightmap(), "Input" + String.valueOf(i));
		}
		return in;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		IntegerParameterSlider parameterInputPorts = new IntegerParameterSlider("Input ports", 1, 10, inputPorts);
		
		try {
			parameterInputPorts.addToGrid(pane, 0);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			inputPorts = parameterInputPorts.getValue();
			reregisterIO();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("inputports", String.valueOf(inputPorts)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("inputports")) {
				inputPorts = Integer.parseInt(e.content);
				reregisterIO();
			}
		}
	}
}
