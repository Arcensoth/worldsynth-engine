/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.IntegerParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapTerrace extends AbstractModule {
	
	private TerraceType terraceType = TerraceType.BASIC;
	private int terraceLevels = 10;
	private double terraceShape = 0;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		else if(isBypassed()) {
			return inputs.get("input");
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in terrace modulation map
		float[][] terraceModulationMap = null;
		if(inputs.get("tmod") != null) {
			terraceModulationMap = ((DatatypeHeightmap) inputs.get("tmod")).getHeightmap();
		}
		
		//Read in offset modulation map
		float[][] offsetModulationMap = null;
		if(inputs.get("omod") != null) {
			offsetModulationMap = ((DatatypeHeightmap) inputs.get("omod")).getHeightmap();
		}
		
		//Read in mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] terracedMap = new float[mpw][mpl];
		
		//Has both modulation maps
		if(terraceModulationMap != null && offsetModulationMap != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = (float) terrace(inputMap[u][v], terraceModulationMap[u][v], offsetModulationMap[u][v], terraceLevels, terraceType);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					terracedMap[u][v] = o;
				}
			}
		}
		//Has terrace modulation map
		else if(terraceModulationMap != null && offsetModulationMap == null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = (float) terrace(inputMap[u][v], terraceModulationMap[u][v], 0.0, terraceLevels, terraceType);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					terracedMap[u][v] = o;
				}
			}
		}
		//Has terrace modulation map
		else if(terraceModulationMap == null && offsetModulationMap != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = (float) terrace(inputMap[u][v], 1.0, offsetModulationMap[u][v], terraceLevels, terraceType);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					terracedMap[u][v] = o;
				}
			}
		}
		//No modulation map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = (float) terrace(inputMap[u][v], 1.0, 0.0, terraceLevels, terraceType);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					terracedMap[u][v] = o;
				}
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					terracedMap[u][v] = terracedMap[u][v] * mask[u][v] + inputMap[u][v] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.setHeightmap(terracedMap);
		
		return requestData;
	}
	
	private double terrace(double height, double terraceModulation, double offsetModulation, double terraceLevels, TerraceType method) {
		height -= offsetModulation;
		terraceLevels *= 0.3 + terraceModulation*0.7;
//		terraceLevels = 1 / (terraceLevels * (1 - terraceModulation) / (terraceLevels + 1) + 1 / terraceLevels);
		double levelRes = 1.0 / terraceLevels;
		double level = Math.floor(height/levelRes);
		double levelDivision = (height*terraceLevels - level);
		double shape = terraceShape * 0.5;
		if(method == TerraceType.BASIC) {
			return level*levelRes + offsetModulation;
		}
		else if(method == TerraceType.SHARP) {
			if(levelDivision < shape) {
				return level * levelRes + levelDivision/(2.0*shape) * levelRes + offsetModulation;
			}
			else if(levelDivision < (1.0 - shape)) {
				return level * levelRes + 0.5 * levelRes + offsetModulation;
			}
			else {
				return level * levelRes + (levelDivision + 2.0*shape - 1)/(2.0*shape) * levelRes + offsetModulation;
			}
		}
		return 0.0f;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("tmod", new ModuleInputRequest(getInput(1), outputRequest.data));
		inputRequests.put("omod", new ModuleInputRequest(getInput(2), outputRequest.data));
		inputRequests.put("mask", new ModuleInputRequest(getInput(3), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Terrace";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeHeightmap(), "Terrace modulation"),
				new ModuleInput(new DatatypeHeightmap(), "Offset modulation"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeHeightmap(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		EnumParameterDropdownSelector<TerraceType> parameterTerraceType = new EnumParameterDropdownSelector<TerraceType>("Terrace type", TerraceType.class, terraceType);
		IntegerParameterSlider parameterTerraceLevels = new IntegerParameterSlider("Terrace levels", 1, 100, terraceLevels);
		DoubleParameterSlider parameterTerraceShape = new DoubleParameterSlider("Terrace shape", 0.0, 1.0, terraceShape);
		
		try {
			parameterTerraceType.addToGrid(pane, 0);
			parameterTerraceLevels.addToGrid(pane, 1);
			parameterTerraceShape.addToGrid(pane, 2);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			terraceType = parameterTerraceType.getValue();
			terraceLevels = parameterTerraceLevels.getValue();
			terraceShape = parameterTerraceShape.getValue();
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("type", String.valueOf(terraceType.name())));
		paramenterElements.add(new Element("levels", String.valueOf(terraceLevels)));
		paramenterElements.add(new Element("shape", String.valueOf(terraceShape)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("type")) {
				for(TerraceType type: TerraceType.values()) {
					if(e.content.equals(type.name())) {
						terraceType = type;
						break;
					}
				}
			}
			else if(e.tag.equals("levels")) {
				terraceLevels = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("shape")) {
				terraceShape = Double.parseDouble(e.content);
			}
		}
	}
	
	private enum TerraceType {
		BASIC, SHARP;
	}
}
