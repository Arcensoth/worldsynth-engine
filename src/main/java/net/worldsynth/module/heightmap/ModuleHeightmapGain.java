/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.BooleanParameterCheckbox;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapGain extends AbstractModule {

	private double gain = 1.0;
	private double offset = 0.0;
	private boolean gainCentered = false;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		else if(isBypassed()) {
			return inputs.get("input");
		}
		
		//Read in gain
		double gainValue = this.gain;
		float[][] gainMap = null;
		if(inputs.get("gain") != null) {
			if(inputs.get("gain") instanceof DatatypeScalar) {
				gainValue = ((DatatypeScalar) inputs.get("gain")).data;
			}
			else {
				gainMap = ((DatatypeHeightmap) inputs.get("gain")).getHeightmap();
			}
		}
		
		//Read in offset
		double offsetValue = this.offset;
		float[][] offsetMap = null;
		if(inputs.get("offset") != null) {
			if(inputs.get("offset") instanceof DatatypeScalar) {
				offsetValue = ((DatatypeScalar) inputs.get("offset")).data / 256.0;
			}
			else {
				offsetMap = ((DatatypeHeightmap) inputs.get("offset")).getHeightmap();
			}
		}
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		float[][] gainedMap = new float[mpw][mpl];
		
		float preOffset = 0.0f;
		if(gainCentered) {
			preOffset = -0.5f;
		}
		
		//Has both gain and offset map
		if(gainMap != null && offsetMap != null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					gainedMap[u][v] = gain(inputMap[u][v], preOffset, gainMap[u][v]*gainValue, (offsetMap[u][v]-0.5) * offsetValue);
				}
			}
		}
		//Has gain map
		else if(gainMap != null && offsetMap == null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					gainedMap[u][v] = gain(inputMap[u][v], preOffset, gainMap[u][v]*gainValue, offsetValue);
				}
			}
		}
		//Has offset map
		else if(gainMap == null && offsetMap != null) {
			//Use amplitude map
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					gainedMap[u][v] = gain(inputMap[u][v], preOffset, gainValue, (offsetMap[u][v]-0.5) * offsetValue);
				}
			}
		}
		//Has only values and no map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					gainedMap[u][v] = gain(inputMap[u][v], preOffset, gainValue, offsetValue);
				}
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					gainedMap[u][v] = gainedMap[u][v] * mask[u][v] + inputMap[u][v] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.setHeightmap(gainedMap);
		
		return requestData;
	}
	
	private float gain(float height, float preOffset, double gain, double offset) {
		height += preOffset;
		height *= gain;
		height -= preOffset;
		height += offset;
		height = Math.min(height, 1);
		height = Math.max(height, 0);
		return height;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("gain", new ModuleInputRequest(getInput(1), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("offset", new ModuleInputRequest(getInput(2), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("mask", new ModuleInputRequest(getInput(3), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Gain";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Gain"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Offset"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterGain = new DoubleParameterSlider("Gain", 0.0, 10.0, gain);
		DoubleParameterSlider parameterOffset = new DoubleParameterSlider("Offset", -1.0, 1.0, offset, 256.0);
		BooleanParameterCheckbox parameterGainCenter = new BooleanParameterCheckbox("Gain centered", gainCentered);
		
		try {
			parameterGain.addToGrid(pane, 0);
			parameterOffset.addToGrid(pane, 1);
			parameterGainCenter.addToGrid(pane, 2);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			gain = parameterGain.getValue();
			offset = parameterOffset.getValue();
			gainCentered = parameterGainCenter.getValue();
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("gain", String.valueOf(gain)));
		paramenterElements.add(new Element("offset", String.valueOf(offset)));
		paramenterElements.add(new Element("gaincentered", String.valueOf(gainCentered)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("gain")) {
				gain = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("offset")) {
				offset = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("gaincentered")) {
				gainCentered = Boolean.parseBoolean(e.content);
			}
		}
	}
}
