/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.IntegerParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapExpander extends AbstractModule {
	
	private int radius = 1;
	private ExpandState expandState = ExpandState.HIGHEST;

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		else if(isBypassed()) {
			return inputs.get("input");
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		//Read in radius
		int radiusValue = this.radius;
		float[][] radiusMap = null;
		if(inputs.get("radius") != null) {
			radiusMap = ((DatatypeHeightmap) inputs.get("radius")).getHeightmap();
		}
		
		//Read mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] outputMap = new float[mpw][mpl];
		
		//Has radius map
		if(radiusMap != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = expand(radiusValue, radiusMap[u][v], inputMap, res, u, v);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					outputMap[u][v] = o;
				}
			}
		}
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = expand(radiusValue, 1.0, inputMap, res, u, v);
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					outputMap[u][v] = o;
				}
			}
		}
		
		//Apply mask
		if(mask != null) {
			int activeRadius = (int) Math.floor((double) radiusValue/res);
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					outputMap[u][v] = outputMap[u][v] * mask[u][v] + inputMap[u+activeRadius][v+activeRadius] * (1-mask[u][v]);
				}
			}
		}
		
		requestData.setHeightmap(outputMap);
		
		return requestData;
	}
	
	private float expand(int radius, double radiusModifier, float[][] heightmap, double res, int x, int y) {
		boolean firstSelected = false;
		float selectedValue = 0;
		
		int appliedRadius = (int) Math.floor((double) radius*radiusModifier/res);
		radius = (int) Math.floor((double) radius/res);
		
		for(int kx = -appliedRadius; kx <= appliedRadius; kx++) {
			for(int ky = -appliedRadius; ky <= appliedRadius; ky++) {
				
				float a = heightmap[x+kx+radius][y+ky+radius];
				
				if(!firstSelected) {
					selectedValue = a;
					firstSelected = true;
				}
				else if(expandState == ExpandState.HIGHEST && a > selectedValue) {
					selectedValue = a;
				}
				else if(expandState == ExpandState.LOWEST && a < selectedValue) {
					selectedValue = a;
				}
			}
		}
		
		return selectedValue;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap d = (DatatypeHeightmap) outputRequest.data;
		double expandRadius = Math.floor((double) radius/d.resolution)*d.resolution;
		DatatypeHeightmap inputRequestDatatype = new DatatypeHeightmap(d.x-expandRadius, d.z-expandRadius, d.width+3.0*expandRadius, d.length+3.0*expandRadius, d.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput(0), inputRequestDatatype));
		
		inputRequests.put("radius", new ModuleInputRequest(getInput(1), (DatatypeHeightmap) outputRequest.data));
		inputRequests.put("mask", new ModuleInputRequest(getInput(2), (DatatypeHeightmap) outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Expander";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				new ModuleInput(new DatatypeHeightmap(), "Radius"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		//FIXME Fix bypass
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		//////////Scale //////////
		
		IntegerParameterSlider parameterScale = new IntegerParameterSlider("Kernel size", 0, 20, radius);
		EnumParameterDropdownSelector<ExpandState> parameterExpandState = new EnumParameterDropdownSelector<ExpandState>("State", ExpandState.class, expandState);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterExpandState.addToGrid(pane, 1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			radius = parameterScale.getValue();
			expandState = parameterExpandState.getValue();
		};
		
		return applyHandler;
		
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("kernelradius", String.valueOf(radius)));
		paramenterElements.add(new Element("expandstate", expandState.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("kernelradius")) {
				radius = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("expandstate")) {
				for(ExpandState type: ExpandState.values()) {
					if(e.content.equals(type.name())) {
						expandState = type;
						break;
					}
				}
			}
		}
	}
	
	private enum ExpandState {
		HIGHEST, LOWEST;
	}
}
