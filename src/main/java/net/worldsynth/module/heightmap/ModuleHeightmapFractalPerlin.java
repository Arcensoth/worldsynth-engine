/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.common.math.Permutation;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.IntegerParameterSlider;
import net.worldsynth.standalone.ui.parameters.LongParameterField;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapFractalPerlin extends AbstractModule {
	
	private long seed;
	private double scale = 100.0;
	private double amplitude = 1.0;
	private double offset = 0.0;
	private double distortion = 1.0;
	
	private Shape shape = Shape.STANDARD;
	private int octaves = 4;
	private double persistence = 0.5;
	
	private final int permutationSize = 256;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	public ModuleHeightmapFractalPerlin() {
		
		seed = new Random().nextLong();
		
		permutation = new Permutation(seed, permutationSize, 1);
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		double scaleValue = this.scale;
		double amplitudeValue = this.amplitude;
		double persistenceValue = this.persistence;
		double offsetValue = this.offset;
		float[][] amplitudeMap = null;
		float[][] persistenceMap = null;
		float[][] offsetMap = null;
		float[][][] distortionMap = null;
		float[][] maskMap = null;
		
		//Read in scale
		if(inputs.get("scale") != null) {
			scaleValue = ((DatatypeScalar)inputs.get("scale")).data;
		}
		
		//Read in amplitude
		if(inputs.get("amplitude") != null) {
			if(inputs.get("amplitude") instanceof DatatypeScalar) {
				amplitudeValue = ((DatatypeScalar) inputs.get("amplitude")).data;
			}
			else {
				amplitudeMap = ((DatatypeHeightmap) inputs.get("amplitude")).getHeightmap();
			}
		}
		
		//Read in persistence
		if(inputs.get("persistence") != null) {
			if(inputs.get("persistence") instanceof DatatypeScalar) {
				persistenceValue = ((DatatypeScalar) inputs.get("persistence")).data;
			}
			else {
				persistenceMap = ((DatatypeHeightmap) inputs.get("persistence")).getHeightmap();
			}
		}
		
		//Read in offset
		if(inputs.get("offset") != null) {
			if(inputs.get("offset") instanceof DatatypeScalar) {
				offsetValue = ((DatatypeScalar) inputs.get("offset")).data / 256.0f;
			}
			else {
				offsetMap = ((DatatypeHeightmap) inputs.get("offset")).getHeightmap();
			}
		}
		
		//Read in distortion
		if(inputs.get("distortion") != null) {
			if(inputs.get("distortion") instanceof DatatypeVectormap) {
				distortionMap = ((DatatypeVectormap) inputs.get("distortion")).vectorField;
			}
			else {
				float[][] distortionHeightmap = ((DatatypeHeightmap) inputs.get("distortion")).getHeightmap();
				distortionMap = new float[mpw][mpl][2];
				for(int u = 0; u < mpw; u++) {
					for(int v = 0; v < mpl; v++) {
						distortionMap[u][v][0] = (float) Math.cos(distortionHeightmap[u][v] * Math.PI * 2.0);
						distortionMap[u][v][1] = (float) Math.sin(distortionHeightmap[u][v] * Math.PI * 2.0);
					}
				}
			}
		}
		
		//Read mask
		if(inputs.get("mask") != null) {
			maskMap = ((DatatypeHeightmap)inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		//Has some input maps
		if(amplitudeMap != null || persistenceMap != null || offsetMap != null || distortionMap != null) {
			double xDistortion = 0.0;
			double zDistortion = 0.0;
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					if(amplitudeMap != null) amplitudeValue = amplitudeMap[u][v] * this.amplitude;
					if(persistenceMap != null) persistenceValue = persistenceMap[u][v] * this.persistence;
					if(offsetMap != null) offsetValue = offsetMap[u][v] - 0.5 * this.offset;
					if(distortionMap != null) {
						xDistortion = distortionMap[u][v][0];
						zDistortion = distortionMap[u][v][1];
					}
					
					map[u][v] = (float) getHeightAt(x+u*res+xDistortion*distortion, z+v*res+zDistortion*distortion, scaleValue, amplitudeValue, offsetValue, this.octaves, persistenceValue);
				}
			}
		}
		//Has only values and no map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scaleValue, amplitudeValue, offsetValue, this.octaves, persistenceValue);
				}
			}
		}
		
		//Apply mask
		if(maskMap != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = map[u][v] * maskMap[u][v];
				}
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("scale", new ModuleInputRequest(getInput("Scale"), new DatatypeScalar()));
		inputRequests.put("amplitude", new ModuleInputRequest(getInput("Amplitude"), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("persistence", new ModuleInputRequest(getInput("Persistence"), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("offset", new ModuleInputRequest(getInput("Offset"), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));

		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		DatatypeVectormap requestVectorMap = new DatatypeVectormap(ord.x, ord.z, ord.width, ord.length, ord.resolution);
		inputRequests.put("distortion", new ModuleInputRequest(getInput("Distortion"), new DatatypeMultitype(new AbstractDatatype[] {(DatatypeHeightmap)outputRequest.data, requestVectorMap})));
		
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), (DatatypeHeightmap)outputRequest.data));
		
		return inputRequests;
	}
	
	public double getHeightAt(double x, double y, double scale, double amplitude, double offset, int octaves, double persistence) {
		float height = 0;
		for(int i = 0; i < octaves; i++) {
			double m = Math.pow(persistence, i);
			height += perlin(x/scale*Math.pow(2, i), y/scale*Math.pow(2, i))*m;
		}
		
		if(persistence == 1) {
			persistence -= 0.00001;
		}
		
		switch (shape) {
		case STANDARD:
			height /= 1 + Math.pow(persistence, octaves - 1);
			height *= amplitude;
			height += 0.5f;
			height += offset;
			break;
		case RIDGED:
			double f = 1.0;
			if(octaves > 0) {
				f = (1 - Math.pow(persistence, octaves)) / (1 - persistence);
			}
			height /= f;
			height *= amplitude;
			height += offset;
			break;
		case BOWLY:
			double g = 1.0;
			if(octaves > 0) {
				g = (1 - Math.pow(persistence, octaves)) / (1 - persistence);
			}
			height /= g;
			height *= amplitude;
			height += offset;
			break;
		}
		
		height = Math.min(height, 1);
		height = Math.max(height, 0);
		return height;
	}
	
	private double perlin(double x, double y) {
		if(repeat > 0) {
			if(x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if(y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		
		
		double u = easeCurve(xf);
		double v = easeCurve(yf);
		
		int aa, ab, ba, bb;
		aa = permutation.lHash(0, xi     , yi     );
		ab = permutation.lHash(0, xi     , inc(yi));
		ba = permutation.lHash(0, inc(xi), yi     );
		bb = permutation.lHash(0, inc(xi), inc(yi));
		
		double a1, a2, a3, a4;
		a1 = grad(aa, xf  , yf  , 0);
		a2 = grad(ba, xf-1, yf, 0);
		a3 = grad(ab, xf, yf-1, 0);
		a4 = grad(bb, xf-1, yf-1, 0);
		
		double a12, a34;
		a12 = lerp(a2, a1, u);
		a34 = lerp(a4, a3, u);
		double height = lerp(a34, a12, v);
		
		switch (shape) {
		case STANDARD:
			height /= 2;
			break;
		case RIDGED:
			height = 1.0f - Math.abs(height);
			break;
		case BOWLY:
			height = Math.abs(height);
			break;
		}
		
		return height;
	}
	
	private int inc(int num) {
		num++;
		int ret;
		if(num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}
	
	private double easeCurve(double t) {
		return t * t * t * (t * (t * 6 - 15) + 10);
	}
	
	private double grad(int hash, double x, double y, double z) {
		switch(hash & 0xF)
	    {
	        case 0x0: return  x + y;
	        case 0x1: return -x + y;
	        case 0x2: return  x - y;
	        case 0x3: return -x - y;
	        case 0x4: return  x + z;
	        case 0x5: return -x + z;
	        case 0x6: return  x - z;
	        case 0x7: return -x - z;
	        case 0x8: return  y + z;
	        case 0x9: return -y + z;
	        case 0xA: return  y - z;
	        case 0xB: return -y - z;
	        case 0xC: return  y + x;
	        case 0xD: return -y + z;
	        case 0xE: return  y - x;
	        case 0xF: return -y - z;
	        default: return 0; // never happens
	    }
	}
	
	private double lerp(double a, double b, double x) {
	    return a*x + b*(1-x);
	}

	@Override
	public String getModuleName() {
		return "Fractal perlin";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Amplitude"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Persistence"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Offset"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeHeightmap(), new DatatypeVectormap()}), "Distortion"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterAmplitude = new DoubleParameterSlider("Amplitude", 0.0, 5.0, amplitude);
		DoubleParameterSlider parameterOffset = new DoubleParameterSlider("Offset", -1.0, 1.0, offset, 256.0);
		IntegerParameterSlider parameterOctaves = new IntegerParameterSlider("Octaves", 1, 10, octaves);
		DoubleParameterSlider parameterPersistence = new DoubleParameterSlider("Persistence", 0, 1, persistence);
		EnumParameterDropdownSelector<Shape> parameterShape = new EnumParameterDropdownSelector<Shape>("Shape", Shape.class, shape);
		LongParameterField parameterSeed = new LongParameterField("Seed", seed);
		DoubleParameterSlider parameterDistortion = new DoubleParameterSlider("Distortion", 0.0, 100.0, distortion);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterAmplitude.addToGrid(pane, 1);
			parameterOffset.addToGrid(pane, 2);
			parameterOctaves.addToGrid(pane, 3);
			parameterPersistence.addToGrid(pane, 4);
			parameterShape.addToGrid(pane, 5);
			parameterSeed.addToGrid(pane, 6);
			parameterDistortion.addToGrid(pane, 7);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			amplitude = parameterAmplitude.getValue();
			offset = parameterOffset.getValue();
			distortion = parameterDistortion.getValue();
			
			shape = parameterShape.getValue();
			octaves = parameterOctaves.getValue();
			persistence = parameterPersistence.getValue();
			
			//Sets the seed and regenerates the permutationtable
			seed = parameterSeed.getValue();
			permutation = new Permutation(seed, permutationSize, 1);
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("seed", String.valueOf(seed)));
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("amplitude", String.valueOf(amplitude)));
		paramenterElements.add(new Element("offset", String.valueOf(offset)));
		paramenterElements.add(new Element("shape", shape.name()));
		paramenterElements.add(new Element("octaves", String.valueOf(octaves)));
		paramenterElements.add(new Element("persistence", String.valueOf(persistence)));
		paramenterElements.add(new Element("distortion", String.valueOf(distortion)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("seed")) {
				seed = Long.parseLong(e.content);
				permutation = new Permutation(seed, permutationSize, 1);
			}
			else if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("amplitude")) {
				amplitude = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("offset")) {
				offset = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("distortion")) {
				distortion = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("octaves")) {
				octaves = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("persistence")) {
				persistence = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("shape")) {
				for(Shape type: Shape.values()) {
					if(e.content.equals(type.name())) {
						shape = type;
						break;
					}
				}
			}
		}
	}
	
	private enum Shape {
		STANDARD, RIDGED, BOWLY;
	}
}
