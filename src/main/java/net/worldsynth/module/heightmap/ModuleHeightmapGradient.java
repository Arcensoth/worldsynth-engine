/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.common.math.MathHelperScalar;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapGradient extends AbstractModule {
	
	private double scale = 100;
	private double direction = 0;
	private double distortion = 1.0;
	
	private GradientTiling tiling = GradientTiling.NONE;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		double scaleValue = this.scale;
		double directionValue = this.direction;
		float[][][] distortionMap = null;
		float[][] maskMap = null;
		
		//Read in scale
		if(inputs.get("scale") != null) {
			scaleValue = ((DatatypeScalar) inputs.get("scale")).data;
		}
		
		//Read in direction
		if(inputs.get("direction") != null) {
			directionValue = ((DatatypeScalar) inputs.get("direction")).data;
		}
		
		//Read in distortion
		if(inputs.get("distortion") != null) {
			if(inputs.get("distortion") instanceof DatatypeVectormap) {
				distortionMap = ((DatatypeVectormap) inputs.get("distortion")).vectorField;
			}
			else {
				float[][] distortionHeightmap = ((DatatypeHeightmap) inputs.get("distortion")).getHeightmap();
				distortionMap = new float[mpw][mpl][2];
				for(int u = 0; u < mpw; u++) {
					for(int v = 0; v < mpl; v++) {
						distortionMap[u][v][0] = (float) Math.cos(distortionHeightmap[u][v] * Math.PI * 2.0);
						distortionMap[u][v][1] = (float) Math.sin(distortionHeightmap[u][v] * Math.PI * 2.0);
					}
				}
			}
		}
		
		//Read in mask
		if(inputs.get("mask") != null) {
			maskMap = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		//Has some input maps
		if(distortionMap != null) {
			double xDistortion = 0.0;
			double zDistortion = 0.0;
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					if(distortionMap != null) {
						xDistortion = distortionMap[u][v][0];
						zDistortion = distortionMap[u][v][1];
					}
					
					float o = (float) getHeightAt(x+u*res+xDistortion*distortion, z+v*res+zDistortion*distortion, scaleValue, directionValue);
					map[u][v] = MathHelperScalar.clamp(o, 0.0f, 1.0f);
				}
			}
		}
		//Has only values and no map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					float o = (float) getHeightAt(x+u*res, z+v*res, scaleValue, directionValue);
					map[u][v] = MathHelperScalar.clamp(o, 0.0f, 1.0f);
				}
			}
		}
		
		//Apply mask
		if(maskMap != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = map[u][v] * maskMap[u][v];
				}
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	public double getHeightAt(double x, double y, double scale, double direction) {
		double directionRadians = Math.toRadians(direction);
		if(direction != 0.0) {
			x = x * Math.cos(directionRadians) - y * Math.sin(directionRadians);
		}
		return gradient(x, scale);
	}
	
	private double gradient(double x, double scale) {
		double h = 0;
		h = x/scale;
		if(tiling == GradientTiling.NONE) {
			h = Math.min(Math.max(h, 0), 1);
		}
		else if(tiling == GradientTiling.TILING) {
			h -= Math.floor(h);
		}
		else if(tiling == GradientTiling.CONTINOUS) {
			h -= Math.floor(h);
			h *= 2;
			if(h > 1) {
				h = 2 - h;
			}
		}
		
		return h;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		inputRequests.put("scale", new ModuleInputRequest(getInput("Scale"), new DatatypeScalar()));
		inputRequests.put("direction", new ModuleInputRequest(getInput("Direction"), new DatatypeScalar()));
		
		DatatypeHeightmap ord = (DatatypeHeightmap) outputRequest.data;
		DatatypeVectormap requestVectorMap = new DatatypeVectormap(ord.x, ord.z, ord.width, ord.length, ord.resolution);
		inputRequests.put("distortion", new ModuleInputRequest(getInput("Distortion"), new DatatypeMultitype(new AbstractDatatype[] {(DatatypeHeightmap)outputRequest.data, requestVectorMap})));
		
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), (DatatypeHeightmap) outputRequest.data));
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Gradient";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeScalar(), "Direction"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeHeightmap(), new DatatypeVectormap()}), "Distortion"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterDirection = new DoubleParameterSlider("Direction", 0.0, 360.0, direction);
		EnumParameterDropdownSelector<GradientTiling> parameterTiling = new EnumParameterDropdownSelector<GradientTiling>("Tiling", GradientTiling.class, tiling);
		DoubleParameterSlider parameterDistortion = new DoubleParameterSlider("Distortion", 0.0, 100.0, distortion);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterDirection.addToGrid(pane, 1);
			parameterTiling.addToGrid(pane, 2);
			parameterDistortion.addToGrid(pane, 3);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			direction = parameterDirection.getValue();
			tiling = parameterTiling.getValue();
			distortion = parameterDistortion.getValue();
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("direction", String.valueOf(direction)));
		paramenterElements.add(new Element("tiling", tiling.name()));
		paramenterElements.add(new Element("distortion", String.valueOf(distortion)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			//TODO A1.3.0 remove rotation tag (originally planned for A1.2.0 but pushed back to A1.3.0 because of the short life of A1.1.X)
			else if(e.tag.equals("direction") || e.tag.equals("rotation")) {
				direction = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("tiling")) {
				for(GradientTiling type: GradientTiling.values()) {
					if(e.content.equals(type.name())) {
						tiling = type;
						break;
					}
				}
			}
			else if(e.tag.equals("distortion")) {
				distortion = Double.parseDouble(e.content);
			}
		}
	}
	
	private enum GradientTiling {
		NONE, TILING, CONTINOUS;
	}
}
