/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.common.math.Permutation;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.LongParameterField;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapCells extends AbstractModule {
	
	private long seed;
	private double scale = 100;
	private double amplitude = 1;
	private double offset = 0;
	private double distortion = 1.0;
	
	private CellType cellType = CellType.VORONOI;
	private DistanceFunction distanceFunction = DistanceFunction.EUCLIDEAN;
	private Feature feature = Feature.F1;
	
	private final double S60 = Math.sin(Math.toRadians(60));

	private final int permutationSize = 256;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	public ModuleHeightmapCells() {
		seed = new Random().nextLong();
		permutation = new Permutation(seed, permutationSize, 3);
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//

		double scaleValue = this.scale;
		double amplitudeValue = this.amplitude;
		double offsetValue = this.offset;
		float[][] amplitudeMap = null;
		float[][] offsetMap = null;
		float[][][] distortionMap = null;
		float[][] maskMap = null;
		
		//Read in scale
		if(inputs.get("scale") != null) {
			scaleValue = ((DatatypeScalar) inputs.get("scale")).data;
		}
		
		//Read in amplitude
		if(inputs.get("amplitude") != null) {
			if(inputs.get("amplitude") instanceof DatatypeScalar) {
				amplitudeValue = ((DatatypeScalar) inputs.get("amplitude")).data;
			}
			else {
				amplitudeMap = ((DatatypeHeightmap) inputs.get("amplitude")).getHeightmap();
			}
		}
		
		//Read in offset
		if(inputs.get("offset") != null) {
			if(inputs.get("offset") instanceof DatatypeScalar) {
				offsetValue = ((DatatypeScalar) inputs.get("offset")).data / 256.0;
			}
			else {
				offsetMap = ((DatatypeHeightmap) inputs.get("offset")).getHeightmap();
			}
		}
		
		//Read in distortion
		if(inputs.get("distortion") != null) {
			if(inputs.get("distortion") instanceof DatatypeVectormap) {
				distortionMap = ((DatatypeVectormap) inputs.get("distortion")).vectorField;
			}
			else {
				float[][] distortionHeightmap = ((DatatypeHeightmap) inputs.get("distortion")).getHeightmap();
				distortionMap = new float[mpw][mpl][2];
				for(int u = 0; u < mpw; u++) {
					for(int v = 0; v < mpl; v++) {
						distortionMap[u][v][0] = (float) Math.cos(distortionHeightmap[u][v] * Math.PI * 2.0);
						distortionMap[u][v][1] = (float) Math.sin(distortionHeightmap[u][v] * Math.PI * 2.0);
					}
				}
			}
		}
		
		//Read in mask
		if(inputs.get("mask") != null) {
			maskMap = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		//Has some input maps
		if(amplitudeMap != null || offsetMap != null || distortionMap != null) {
			double xDistortion = 0.0;
			double zDistortion = 0.0;
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					if(amplitudeMap != null) amplitudeValue = amplitudeMap[u][v];
					if(offsetMap != null) offsetValue = offsetMap[u][v];
					if(distortionMap != null) {
						xDistortion = distortionMap[u][v][0];
						zDistortion = distortionMap[u][v][1];
					}
					
					map[u][v] = (float) getHeightAt(x+u*res+xDistortion*distortion, z+v*res+zDistortion*distortion, scaleValue, amplitudeValue, offsetValue);
				}
			}
		}
		//Has only values and no map
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = (float) getHeightAt(x+u*res, z+v*res, scaleValue, amplitudeValue, offsetValue);
				}
			}
		}
		
		//Apply mask
		if(maskMap != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = map[u][v] * maskMap[u][v];
				}
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("scale", new ModuleInputRequest(getInput("Scale"), new DatatypeScalar()));
		inputRequests.put("amplitude", new ModuleInputRequest(getInput("Amplitude"), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		inputRequests.put("offset", new ModuleInputRequest(getInput("Offset"), new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), (DatatypeHeightmap)outputRequest.data})));
		
		DatatypeHeightmap or = (DatatypeHeightmap) outputRequest.data;
		DatatypeVectormap requestVectorMap = new DatatypeVectormap(or.x, or.z, or.width, or.length, or.resolution);
		inputRequests.put("distortion", new ModuleInputRequest(getInput("Distortion"), new DatatypeMultitype(new AbstractDatatype[] {(DatatypeHeightmap)outputRequest.data, requestVectorMap})));
		
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), (DatatypeHeightmap)outputRequest.data));
		
		return inputRequests;
	}
	
	public double getHeightAt(double x, double y, double scale, double amplitude, double offset) {
		return cells(x/scale, y/scale, amplitude, offset);
	}
	
	private double cells(double x, double y, double amplitude, double offset) {
		if(cellType == CellType.HEXAGON) {
			y *= 1.0/S60;
		}
		if(repeat > 0) {
			if(x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if(y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		
		//Calculate the local coordinates of the 9 closest featurepoints
		double[][] fx = new double[3][3];
		double[][] fy = new double[3][3];
		
		double[] dist = new double[9];
		double[] height = new double[9];
		
		for(int ix = 0; ix < 3; ix++) {
			for(int iy = 0; iy < 3; iy++) {
				int cx = inc(xi, ix-1);
				int cy = inc(yi, iy-1);
				
				int xh = permutation.lHash(0, cx, cy);
				int yh = permutation.lHash(1, cx, cy);
				
				double xoffset = (double) xh/(double) repeat;
				double yoffset = (double) yh/(double) repeat;
				
				if(cellType == CellType.SQUARE) {
					xoffset = 0.0;
					yoffset = 0.0;
				}
				
				//Code for hexagon cells
				if(cellType == CellType.HEXAGON) {
					xoffset = 0;
					if(yi%2 == 0) {
						double c = 0.5;
						double m = 0 + Math.abs(iy-1);
						xoffset =  c * m;
					}
					else {
						double c = 0.5;
						double m = 1 - Math.abs(iy-1);
						xoffset =  c * m;
					}
					yoffset = 0;
				}
				
				fx[ix][iy] = (double) (ix-1) + xoffset;
				fy[ix][iy] = (double) (iy-1) + yoffset;
				
				double xdist = fx[ix][iy] - xf;
				double ydist = fy[ix][iy] - yf;
				if(cellType == CellType.HEXAGON) {
					ydist *= S60;
				}
				
				//Distance from point
				switch (distanceFunction) {
				case EUCLIDEAN:
					dist[ix*3 + iy] = Math.sqrt(Math.pow(xdist, 2) + Math.pow(ydist, 2));
					break;
				case MANHATTAN:
					dist[ix*3 + iy] = Math.abs(xdist) + Math.abs(ydist);
					break;
				case CHEBYSHEV:
					dist[ix*3 + iy] = Math.max(Math.abs(xdist), Math.abs(ydist));
					break;
				case MIN:
					dist[ix*3 + iy] = Math.min(Math.abs(xdist), Math.abs(ydist));
					break;
				}
				
				height[ix*3 + iy] = permutation.lHash(2, cx, cy)/(double) permutationSize;
			}
		}
		
		//Sort
		for(int i = 0; i < feature.getMaxFeature(); i++) {
			for(int j = 8; j > i; j--) {
				if(dist[j] < dist[j-1]) {
					//Swap distance
					double temp = dist[j];
					dist[j] = dist[j-1];
					dist[j-1] = temp;
					//Swap height
					temp = height[j];
					height[j] = height[j-1];
					height[j-1] = temp;
					
				}
			}
		}
		
		double h = 0;
		switch (feature) {
		case F1:
			h = height[0];
			break;
		case F2:
			h = height[1];
			break;
		case F3:
			h = height[2];
			break;
		}
		
		h *= amplitude;
		h += offset;
		h = Math.min(h, 1);
		h = Math.max(h, 0);
		return h;
	}
	
	private int inc(int num, int n) {
		num += n;
		int ret;
		if(num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}

	@Override
	public String getModuleName() {
		return "Cells";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Amplitude"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeScalar(), new DatatypeHeightmap()}), "Offset"),
				new ModuleInput(new DatatypeMultitype(new AbstractDatatype[] {new DatatypeHeightmap(), new DatatypeVectormap()}), "Distortion"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		EnumParameterDropdownSelector<CellType> parameterCelltype = new EnumParameterDropdownSelector<ModuleHeightmapCells.CellType>("Celltype", CellType.class, cellType);
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterAmplitude = new DoubleParameterSlider("Amplitude", 0.0, 5.0, amplitude);
		DoubleParameterSlider parameterOffset = new DoubleParameterSlider("Offset", -1.0, 1.0, offset, 256.0);
		EnumParameterDropdownSelector<DistanceFunction> parameterDistanceFunction = new EnumParameterDropdownSelector<ModuleHeightmapCells.DistanceFunction>("Distance function", DistanceFunction.class, distanceFunction);
		EnumParameterDropdownSelector<Feature> parameterFeatureSelection = new EnumParameterDropdownSelector<ModuleHeightmapCells.Feature>("Feature function", Feature.class, feature);
		LongParameterField parameterSeed = new LongParameterField("Seed", seed);
		DoubleParameterSlider parameterDistortion = new DoubleParameterSlider("Distortion", 0.0, 100.0, distortion);
		
		try {
			parameterCelltype.addToGrid(pane, 0);
			parameterScale.addToGrid(pane, 1);
			parameterAmplitude.addToGrid(pane, 2);
			parameterOffset.addToGrid(pane, 3);
			parameterDistanceFunction.addToGrid(pane, 4);
			parameterFeatureSelection.addToGrid(pane, 5);
			parameterSeed.addToGrid(pane, 6);
			parameterDistortion.addToGrid(pane, 7);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			amplitude = parameterAmplitude.getValue();
			offset = parameterOffset.getValue();
			distortion = parameterDistortion.getValue();
			
			distanceFunction = parameterDistanceFunction.getValue();
			feature = parameterFeatureSelection.getValue();
			cellType = parameterCelltype.getValue();
			
			//Sets the seed and regenerates the permutationtable
			seed = parameterSeed.getValue();
			permutation = new Permutation(seed, permutationSize, 3);
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("seed", String.valueOf(seed)));
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("amplitude", String.valueOf(amplitude)));
		paramenterElements.add(new Element("offset", String.valueOf(offset)));
		paramenterElements.add(new Element("type", cellType.name()));
		paramenterElements.add(new Element("distancefunction", distanceFunction.name()));
		paramenterElements.add(new Element("feature", String.valueOf(feature.name())));
		paramenterElements.add(new Element("distortion", String.valueOf(distortion)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("seed")) {
				seed = Long.parseLong(e.content);
				permutation = new Permutation(seed, permutationSize, 3);
			}
			else if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("amplitude")) {
				amplitude = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("offset")) {
				offset = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("distortion")) {
				distortion = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("type")) {
				for(CellType type: CellType.values()) {
					if(e.content.equals(type.name())) {
						cellType = type;
						break;
					}
				}
			}
			else if(e.tag.equals("distancefunction")) {
				for(DistanceFunction type: DistanceFunction.values()) {
					if(e.content.equals(type.name())) {
						distanceFunction = type;
						break;
					}
				}
			}
			else if(e.tag.equals("feature")) {
				for(Feature type: Feature.values()) {
					if(e.content.equals(type.name())) {
						feature = type;
						break;
					}
				}
			}
		}
	}
	
	private enum DistanceFunction {
		EUCLIDEAN,
		MANHATTAN,
		CHEBYSHEV,
		MIN;
	}

	private enum Feature {
		F1(1),
		F2(2),
		F3(3);
		
		private final int maxFeature;
		
		private Feature(int maxFeature) {
			this.maxFeature = maxFeature;
		}
		
		int getMaxFeature() {
			return maxFeature;
		}
	}
	
	private enum CellType {
		VORONOI, HEXAGON, SQUARE;
	}
}
