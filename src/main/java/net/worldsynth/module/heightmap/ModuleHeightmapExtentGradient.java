/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.common.math.MathHelperScalar;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentParameter;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapExtentGradient extends AbstractModule {
	
	private WorldExtentParameter gradientExtent;
	
	private double gradientScale = 100;
	private GradientShape gradientShape = GradientShape.ELLIPTIC;
	private GradientStyle gradientStyle = GradientStyle.SCURVE;
	
	
	@Override
	protected void postInit() {
		gradientExtent = getNewExtentParameter();
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double res = requestData.resolution;
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		WorldExtent extent = gradientExtent.getExtent();
		if(extent == null) {
			return null;
		}
		
		//Read in scale
		double scaleValue = this.gradientScale;
		if(inputs.get("scale") != null) {
			scaleValue = ((DatatypeScalar) inputs.get("scale")).data;
		}
		
		//Read in mask
		float[][] mask = null;
		if(inputs.get("mask") != null) {
			mask = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][] map = new float[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float o = getHeightAt(x+u*res, z+v*res, scaleValue, extent);
				map[u][v] = o;
			}
		}
		
		//Apply mask
		if(mask != null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					map[u][v] = map[u][v] * mask[u][v];
				}
			}
		}
		
		requestData.setHeightmap(map);
		
		return requestData;
	}
	
	private float getHeightAt(double x, double z, double scale, WorldExtent extent) {
		float h = 0.0f;
		
		if(gradientShape == GradientShape.RECTANGULAR) {
			h = getRectangularGradient(x, z, scale, extent);
		}
		else if(gradientShape == GradientShape.ELLIPTIC) {
			h = getElipticalGradient(x, z, scale, extent);
		}
		
		h = Math.min(h, 1.0f);
		h = Math.max(h, 0.0f);
		
		switch(gradientStyle) {
		case LINEAR:
			break;
		case SCURVE:
			h = h * h * h * (h * (h * 6 - 15) + 10);
			break;
		case DOME:
			h = 1.0f - h;
			h = (float) Math.sqrt(1.0f - h * h);
			break;
		case SPIKE:
			h = 1.0f - (float) Math.sqrt(1.0f - h * h);
			break;
		}
		
		return h;
	}
	
	private float getRectangularGradient(double x, double z, double scale, WorldExtent extent) {
		if(extent.containsCoordinate(x, z)) {
			double west = x - extent.getX();
			double east = (extent.getX()+extent.getWidth()) - x;
			double north = z - extent.getZ();
			double south = (extent.getZ()+extent.getLength()) - z;
			
			double min = Math.min(Math.min(west, east), Math.min(north, south));
			return (float) (MathHelperScalar.clamp(min / scale, 0.0, 1.0));
		}
		else {
			return 0.0f;
		}
	}
	
	private float getElipticalGradient(double x, double z, double scale, WorldExtent extent) {
		if(extent.containsCoordinate(x, z)) {
			double cx = extent.getX() + extent.getWidth() / 2.0;
			double cz = extent.getZ() + extent.getLength() / 2.0;
			
			double dx = (x - cx) / (extent.getWidth() / 2.0);
			double dz = (z - cz) / (extent.getLength() / 2.0);
			
			double dist = 1.0 - Math.sqrt(dx * dx + dz * dz);
			dist *= (Math.max(extent.getWidth(), extent.getLength()) / 2.0) / scale;
			
			return (float) dist;
		}
		else {
			return 0.0f;
		}
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		inputRequests.put("scale", new ModuleInputRequest(getInput(0), new DatatypeScalar()));
		inputRequests.put("mask", new ModuleInputRequest(getInput(1), (DatatypeHeightmap) outputRequest.data));
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Extent gradient";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeScalar(), "Scale"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		ExtentParameterDropdownSelector parameterExtent = gradientExtent.getDropdownSelector("Export extent");
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, gradientScale);
		EnumParameterDropdownSelector<GradientShape> parameterShape = new EnumParameterDropdownSelector<GradientShape>("Gradient shape", GradientShape.class, gradientShape);
		EnumParameterDropdownSelector<GradientStyle> parameterStyle = new EnumParameterDropdownSelector<GradientStyle>("Gradient style", GradientStyle.class, gradientStyle);
		
		try {
			parameterExtent.addToGrid(pane, 0);
			parameterScale.addToGrid(pane, 1);
			parameterShape.addToGrid(pane, 2);
			parameterStyle.addToGrid(pane, 3);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			gradientExtent.setExtent(parameterExtent.getValue());
			gradientScale = parameterScale.getValue();
			gradientShape = parameterShape.getValue();
			gradientStyle = parameterStyle.getValue();
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("extent", gradientExtent.getExtentAsString()));
		paramenterElements.add(new Element("scale", String.valueOf(gradientScale)));
		paramenterElements.add(new Element("shape", gradientShape.name()));
		paramenterElements.add(new Element("style", gradientStyle.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("extent")) {
				gradientExtent.setExtentAsString(e.content);
			}
			else if(e.tag.equals("scale")) {
				gradientScale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("shape")) {
				for(GradientShape type: GradientShape.values()) {
					if(e.content.equals(type.name())) {
						gradientShape = type;
						break;
					}
				}
			}
			else if(e.tag.equals("style")) {
				for(GradientShape type: GradientShape.values()) {
					if(e.content.equals(type.name())) {
						gradientShape = type;
						break;
					}
				}
			}
		}
	}
	
	private enum GradientShape {
		RECTANGULAR, ELLIPTIC;
	}
	
	private enum GradientStyle {
		LINEAR, SCURVE, DOME, SPIKE;
	}
}
