/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.colormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleColormapSelector extends AbstractModule {
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		if(inputs.get("selector") == null || inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If any of the inputs are null, there is not enough input and then just return null
			return null;
		}
		
		float[][] inputMap0 = ((DatatypeHeightmap) inputs.get("selector")).getHeightmap();
		float[][][] inputMap1 = ((DatatypeColormap) inputs.get("primary")).colorMap;
		float[][][] inputMap2 = ((DatatypeColormap) inputs.get("secondary")).colorMap;
		
		float[][][] map = new float[mpw][mpl][3];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float r = lerp(inputMap1[u][v][0], inputMap2[u][v][0], inputMap0[u][v]);
				float g = lerp(inputMap1[u][v][1], inputMap2[u][v][1], inputMap0[u][v]);
				float b = lerp(inputMap1[u][v][2], inputMap2[u][v][2], inputMap0[u][v]);
				map[u][v][0] = r;
				map[u][v][1] = g;
				map[u][v][2] = b;
			}
		}
		
		requestData.colorMap = map;
		
		return requestData;
	}
	
	private float lerp(float a, float b, float x) {
	    return a*x + b*(1-x);
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeColormap colormapRequestData = (DatatypeColormap) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(colormapRequestData.x, colormapRequestData.z, colormapRequestData.width, colormapRequestData.length, colormapRequestData.resolution);
		
		inputRequests.put("selector", new ModuleInputRequest(getInput(0), heightmapRequestData));
		inputRequests.put("primary", new ModuleInputRequest(getInput(1), colormapRequestData));
		inputRequests.put("secondary", new ModuleInputRequest(getInput(2), colormapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Selector";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER_COLORMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Selector input"),
				new ModuleInput(new DatatypeColormap(), "Primary color"),
				new ModuleInput(new DatatypeColormap(), "Secondary color")
		};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
	}
}
