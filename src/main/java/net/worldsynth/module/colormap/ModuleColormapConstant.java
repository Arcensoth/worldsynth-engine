/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.colormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.WorldSynthColorPicker;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleColormapConstant extends AbstractModule {
	
	float constantRed = 0;
	float constantGreen = 0.5f;
	float constantBlue = 0;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeColormap requestData = (DatatypeColormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		float[][][] map = new float[mpw][mpl][3];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float r = constantRed;
				float g = constantGreen;
				float b = constantBlue;
				map[u][v][0] = r;
				map[u][v][1] = g;
				map[u][v][2] = b;
			}
		}
		
		requestData.colorMap = map;
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Constant";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_COLORMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeColormap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Constant //////////
		Color c = Color.rgb((int) (constantRed*255.0), (int) (constantGreen*255.0), (int) (constantBlue*255.0));
		WorldSynthColorPicker colorPicker = new WorldSynthColorPicker(c);
		pane.add(colorPicker, 0, 0);
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			Color color = colorPicker.getSelectedColor();
			colorPicker.setReferanceColor(color);
			constantRed = (float) color.getRed();
			constantGreen = (float) color.getGreen();
			constantBlue = (float) color.getBlue();
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("red", String.valueOf(constantRed)));
		paramenterElements.add(new Element("green", String.valueOf(constantGreen)));
		paramenterElements.add(new Element("blue", String.valueOf(constantBlue)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("red")) {
				constantRed = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("green")) {
				constantGreen = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("blue")) {
				constantBlue = Float.parseFloat(e.content);
			}
		}
	}
}
