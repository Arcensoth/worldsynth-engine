/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.heightmapoverlay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeHeightmapOverlay;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleOverlayGenerator extends AbstractModule {
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmapOverlay requestData = (DatatypeHeightmapOverlay) request.data;
		
		if(inputs.get("height") == null || inputs.get("color") == null) {
			//If any of the inputs are null, there is not enough input and then just return null
			return null;
		}
		
		float[][] inputMap0 = ((DatatypeHeightmap) inputs.get("height")).getHeightmap();
		float[][][] inputMap1 = ((DatatypeColormap) inputs.get("color")).colorMap;
		
		requestData.heightMap = inputMap0.clone();
		requestData.colorMap = inputMap1.clone();
		
		return requestData;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmapOverlay overlayRequestData = (DatatypeHeightmapOverlay) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(overlayRequestData.x, overlayRequestData.z, overlayRequestData.width, overlayRequestData.length, overlayRequestData.resolution);
		DatatypeColormap colormapRequestData = new DatatypeColormap(overlayRequestData.x, overlayRequestData.z, overlayRequestData.width, overlayRequestData.length, overlayRequestData.resolution);
		
		inputRequests.put("height", new ModuleInputRequest(getInput(0), heightmapRequestData));
		inputRequests.put("color", new ModuleInputRequest(getInput(1), colormapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Overlay";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_OVERLAY;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Heightmap input"),
				new ModuleInput(new DatatypeColormap(), "Colormap input"),
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmapOverlay(), "Primary output", false)
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
	}
}
