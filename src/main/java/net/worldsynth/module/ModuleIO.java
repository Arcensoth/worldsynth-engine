/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.modulewrapper.ModuleWrapper;

/**
 * {@link ModuleIO}  is used by the parent {@link ModuleWrapper} object that wraps a module
 *  and the rendering core to define its inputs and outputs. It contains a {@link AbstractDatatype}
 *  prototype used to define the input or output type, and the name of the input or output.
 */
public class ModuleIO {
	AbstractDatatype data;
	String name;
	boolean visible;
	
	public ModuleIO(AbstractDatatype data, String name) {
		this(data, name, true);
	}
	
	public ModuleIO(AbstractDatatype data, String name, boolean visible) {
		this.data = data;
		this.name = name;
		this.visible = visible;
	}
	
	public AbstractDatatype getData() {
		return data;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isVisible() {
		return visible;
	}
}
