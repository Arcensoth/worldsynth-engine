/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleBlockspaceCombiner extends AbstractModule {
	
	Operation operation = Operation.OR;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLenght;
		
		int[][][] blockspaceMaterialId = new int[spw][sph][spl];
		
		if(!inputs.containsKey("primary") || !inputs.containsKey("secondary")) {
			//If the main or secondary input is null, there is not enough input and then just return null
			return null;
		}
		
		int[][][] primary = ((DatatypeBlockspace) inputs.get("primary")).blockspaceMaterialId;
		
		int[][][] secondary = ((DatatypeBlockspace) inputs.get("secondary")).blockspaceMaterialId;
		
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					int i0Id = primary[u][v][w];
					int i1Id = secondary[u][v][w];
					
					int oId = 0;
					
					switch (operation) {
					case AND:
						if(i0Id != 0 && i1Id != 0) {
							oId = i0Id;
						}
						break;
					case OR:
						if(i0Id != 0) {
							oId = i0Id;
						}
						else {
							oId = i1Id;
						}
						break;
					case XOR:
						if(i0Id != 0 && i1Id == 0) {
							oId = i0Id;
						}
						else if(i0Id == 0 && i1Id != 0) {
							oId = i1Id;
						}
						break;
					case REPLACE:
						if(i0Id != 0 && i1Id != 0) {
							oId = i0Id;
						}
						else {
							oId = i1Id;
						}
						break;
					case REMOVE:
						if(i1Id == 0) {
							oId = i0Id;
						}
						else {
							oId = 0;
						}
						break;
					}
					
					blockspaceMaterialId[u][v][w] = oId;
				}
			}
		}
		
		requestData.blockspaceMaterialId = blockspaceMaterialId;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput(1), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Blockspace combiner";
	}
	
	@Override
	public String getModuleMetaTag() {
		return operation.name();
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Primary input"),
				new ModuleInput(new DatatypeBlockspace(), "Secondary input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		EnumParameterDropdownSelector<Operation> parameterOperation = new EnumParameterDropdownSelector<Operation>("Arithmetic operation", Operation.class, operation);
		
		try {
			parameterOperation.addToGrid(pane, 0);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			operation = parameterOperation.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("operation", operation.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("operation")) {
				for(Operation type: Operation.values()) {
					if(e.content.equals(type.name())) {
						operation = type;
						break;
					}
				}
			}
		}
	}
	
	private enum Operation {
		AND, OR, XOR, REPLACE, REMOVE;
	}
}
