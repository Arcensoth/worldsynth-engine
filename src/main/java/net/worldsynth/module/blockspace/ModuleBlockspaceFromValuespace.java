/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.FloatParameterSlider;
import net.worldsynth.standalone.ui.parameters.MaterialParameterSelector;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleBlockspaceFromValuespace extends AbstractModule {
	
	float lowSelect = 0.0f;
	float highSelect = 1.0f;
	
	private Material material = MaterialRegistry.getDefaultMaterial();
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLenght;
		
		int[][][] id = new int[spw][sph][spl];
		
		if(inputs.get("valuespace") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}
		if(inputs.get("high") != null) {
			highSelect = (float) ((DatatypeScalar) inputs.get("high")).data;
		}
		if(inputs.get("low") != null) {
			lowSelect = (float) ((DatatypeScalar) inputs.get("low")).data;
		}
		
		float[][][] inputSpace0 = ((DatatypeValuespace) inputs.get("valuespace")).valuespace;
		int materialId = material.getInternalId();
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					float i0 = inputSpace0[u][v][w];
					
					int oid = 0;
					if(i0 <= highSelect && i0 >= lowSelect) {
						oid = materialId;
					}
					
					id[u][v][w] = oid;
				}
			}
		}
		
		requestData.blockspaceMaterialId = id;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace dvb = (DatatypeBlockspace) outputRequest.data;
		
		DatatypeValuespace dvv = new DatatypeValuespace(dvb.x, dvb.y, dvb.z, dvb.width, dvb.height, dvb.length, dvb.resolution);
		
		inputRequests.put("valuespace", new ModuleInputRequest(getInput(0), dvv));
		inputRequests.put("high", new ModuleInputRequest(getInput(1), new DatatypeScalar()));
		inputRequests.put("low", new ModuleInputRequest(getInput(2), new DatatypeScalar()));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Blockspace from valuespace";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "High select"),
				new ModuleInput(new DatatypeScalar(), "Low select")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		FloatParameterSlider parameterHighSelect = new FloatParameterSlider("High select", 0.0f, 1.0f, highSelect);
		FloatParameterSlider parameterLowSelect = new FloatParameterSlider("Low select", 0.0f, 1.0f, lowSelect);
		
		MaterialParameterSelector parameterMaterial = new MaterialParameterSelector("Material", material);
		
		try {
			parameterHighSelect.addToGrid(pane, 0);
			parameterLowSelect.addToGrid(pane, 1);
			parameterMaterial.addToGrid(pane, 2);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			highSelect = parameterHighSelect.getValue();
			lowSelect = parameterLowSelect.getValue();

			material = parameterMaterial.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("lowselect", String.valueOf(lowSelect)));
		paramenterElements.add(new Element("highselect", String.valueOf(highSelect)));
		paramenterElements.add(new Element("material", material.getIdName()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		int blockId = -1;
		byte blockMeta = 0;
		for(Element e: element.elements) {
			if(e.tag.equals("lowselect")) {
				lowSelect = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("highselect")) {
				highSelect = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("material")) {
				material = MaterialRegistry.getMaterial(e.content);
			}
			//FORREMOVAL A1.0.3 Remove reading of material from id and meta values
			else if(e.tag.equals("blockid")) {
				blockId = Integer.parseInt(e.content);
			}
			else if(e.tag.equals("blockmeta")) {
				blockMeta = Byte.parseByte(e.content);
			}
		}
		
		if(blockId != -1) {
			material = MaterialRegistry.getMaterial(blockId, blockMeta);
		}
	}
}
