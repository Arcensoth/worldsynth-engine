/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.FloatParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleBlockspaceHeightClamp extends AbstractModule {
	
	float lowClamp = 0;
	float highClamp = 1;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double y = requestData.y;
		double height = requestData.height;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLenght;
		double res = requestData.resolution;
		
		int[][][] blockspaceMaterialId = new int[spw][sph][spl];
		
		float[][] inputMap1 = null;
		float[][] inputMap2 = null;
		
		if(inputs.get("input") == null) {
			//If the main or secondary input is null, there is not enough input and then just return null
			return null;
		}
		if(inputs.get("high") != null) {
			inputMap1 = ((DatatypeHeightmap) inputs.get("high")).getHeightmap();
		}
		if(inputs.get("low") != null) {
			inputMap2 = ((DatatypeHeightmap) inputs.get("low")).getHeightmap();
		}
		
		int[][][] inputSpace0MaterialId = ((DatatypeBlockspace) inputs.get("input")).blockspaceMaterialId;
		
		float fy = (float)y/255.0f;
		float fheight = (float)height/255.0f;
		
		for(int u = 0; u < spw; u++) {
			for(int w = 0; w < spl; w++) {
				
				float instantMinHeight = Math.max(fy, lowClamp);
				float instantMaxHeight = Math.min(fy+fheight, highClamp);
				if(inputMap1 != null) {
					instantMaxHeight = Math.min(fy+fheight, inputMap1[u][w]-fy);
				}
				if(inputMap2 != null) {
					instantMinHeight = Math.max(fy, inputMap2[u][w] - fy);
				}
				
				int minHeight = (int)(instantMinHeight*255.0f/res);
				int maxHeight = (int)(instantMaxHeight*255.0f/res);
				
				for(int v = minHeight; v < maxHeight; v++) {
					int i0ID = inputSpace0MaterialId[u][v][w];
					
					blockspaceMaterialId[u][v][w] = i0ID;
				}
			}
		}
		
		requestData.blockspaceMaterialId = blockspaceMaterialId;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace vbrd = (DatatypeBlockspace) outputRequest.data;
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(vbrd.x, vbrd.z, vbrd.width, vbrd.length, vbrd.resolution);
		
		inputRequests.put("high", new ModuleInputRequest(getInput(1), heightmapRequestData));
		inputRequests.put("low", new ModuleInputRequest(getInput(2), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Blockspace height clamp";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Primary input"),
				new ModuleInput(new DatatypeHeightmap(), "High clamp"),
				new ModuleInput(new DatatypeHeightmap(), "Low clamp")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		FloatParameterSlider parameterHighClamp = new FloatParameterSlider("Low clamp", 0.0f, 1.0f, highClamp);
		FloatParameterSlider parameterLowClamp = new FloatParameterSlider("Low clamp", 0.0f, 1.0f, lowClamp);
		
		try {
			parameterHighClamp.addToGrid(pane, 0);
			parameterLowClamp.addToGrid(pane, 1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			highClamp = parameterHighClamp.getValue();
			lowClamp = parameterLowClamp.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("lowclamp", String.valueOf(lowClamp)));
		paramenterElements.add(new Element("highclamp", String.valueOf(highClamp)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("lowclamp")) {
				lowClamp = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("highclamp")) {
				highClamp = Float.parseFloat(e.content);
			}
		}
	}
}
