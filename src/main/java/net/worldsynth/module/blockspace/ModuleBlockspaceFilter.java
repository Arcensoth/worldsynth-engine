/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleBlockspaceFilter extends AbstractModule {
	
	private boolean inclusive = true;
	
	private final Material BLANK = new Material("", "", -1, null);
	Material[] materialFilterPalette = {MaterialRegistry.getDefaultMaterial()};
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLenght;
		
		if(inputs.get("input") == null) {
			return null;
		}
		
		int[][][] blockspaceMaterialId = ((DatatypeBlockspace) inputs.get("input")).blockspaceMaterialId;
		
		//Apply cover on top of existing blocks
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					boolean inPalette = inFilterPalette(blockspaceMaterialId[u][v][w]);
					if(inPalette && !inclusive) {
						blockspaceMaterialId[u][v][w] = 0;
					}
					else if(!inPalette && inclusive) {
						blockspaceMaterialId[u][v][w] = 0;
					}
				}
			}
		}
		
		requestData.blockspaceMaterialId = blockspaceMaterialId;
		
		return requestData;
	}
	
	private boolean inFilterPalette(int materialId) {
		for(Material m: materialFilterPalette) {
			if(materialId == m.getInternalId()) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace outputRequestData = (DatatypeBlockspace) outputRequest.data;
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Filter";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Blockspace")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		//////////Filter mode//////////
		ToggleButton modeButton = new ToggleButton(inclusive ? "Inclusive" : "Exlusive");
		GridPane.setFillWidth(modeButton, true);
		modeButton.setSelected(!inclusive);
		modeButton.fire();
		
		modeButton.setOnAction(e -> {
			modeButton.setText(modeButton.isSelected() ? "Inclusive" : "Exlusive");
		});
		
		//////////Material palette//////////
		TableView<MaterialEntry> paletteTable = new TableView<MaterialEntry>();
		paletteTable.setPrefHeight(400);
		paletteTable.setEditable(true);
		TableColumn<MaterialEntry, Material> materialColumn = new TableColumn<MaterialEntry, Material>("Material palette");
		paletteTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		
		materialColumn.setCellValueFactory(new PropertyValueFactory<MaterialEntry, Material>("material"));
		materialColumn.setCellFactory(ComboBoxTableCell.forTableColumn(MaterialRegistry.getMaterialsAlphabetically()));
		materialColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<MaterialEntry, Material>>() {
			@Override
			public void handle(CellEditEvent<MaterialEntry, Material> t) {
				((MaterialEntry) t.getTableView().getItems().get(t.getTablePosition().getRow())).setMaterial(t.getNewValue());
				if(t.getTableView().getItems().get(t.getTableView().getItems().size()-1).getMaterial() != BLANK) {
					paletteTable.getItems().add(MaterialEntry.convertToEntry(BLANK));
				}
			}
		});
		
		paletteTable.getItems().addAll(MaterialEntry.convertToEntries(materialFilterPalette));
		paletteTable.getItems().add(MaterialEntry.convertToEntry(BLANK));
		
		paletteTable.getColumns().add(materialColumn);
		
		pane.add(paletteTable, 0, 0);
		pane.add(modeButton, 0, 1);
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			ObservableList<MaterialEntry> list = paletteTable.getItems();
			ArrayList<Material> materialList = new ArrayList<Material>();
			for(MaterialEntry entry: list) {
				if(entry.getMaterial() != BLANK) {
					materialList.add(entry.getMaterial());
				}
			}
			Material[] m = new Material[materialList.size()];
			materialList.toArray(m);
			materialFilterPalette = m;
			
			inclusive = modeButton.isSelected();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> parameterElements = new ArrayList<Element>();
		
		String paletteString = "";
		for(Material m: materialFilterPalette) {
			paletteString += m.getIdName() + ";";
		}
		
		parameterElements.add(new Element("palette", paletteString));
		parameterElements.add(new Element("inclusive", String.valueOf(inclusive)));
		
		return parameterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("palette")) {
				ArrayList<Material> recoveredPalette = new ArrayList<Material>();
				String[] paletteString = e.content.split(";");
				for(String s: paletteString) {
					Material material = MaterialRegistry.getMaterial(s);
					recoveredPalette.add(material);
				}
				materialFilterPalette = new Material[recoveredPalette.size()];
				recoveredPalette.toArray(materialFilterPalette);
			}
			else if(e.tag.equals("inclusive")) {
				inclusive = Boolean.parseBoolean(e.content);
			}
		}
	}
	
	public static class MaterialEntry {
		private final SimpleObjectProperty<Material> material;
		
		private MaterialEntry(Material m) {
			this.material = new SimpleObjectProperty<Material>(m);
		}
		
		public Material getMaterial() {
			return material.get();
		}
		
		public void setMaterial(Material m) {
			material.set(m);
		}
		
		public static MaterialEntry[] getMaterialEntriesAlphabetically() {
			return convertToEntries(MaterialRegistry.getMaterialsAlphabetically());
		}
		
		public static MaterialEntry[] convertToEntries(Material[] m) {
			MaterialEntry[] entries = new MaterialEntry[m.length];
			for(int i = 0; i < m.length; i++) {
				entries[i] = new MaterialEntry(m[i]);
			}
			
			return entries;
		}
		
		public static MaterialEntry convertToEntry(Material m) {
			return new MaterialEntry(m);
		}
	}
}
