/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.blockspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.BooleanParameterCheckbox;
import net.worldsynth.standalone.ui.parameters.FloatParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleBlockspaceSurfaceCover extends AbstractModule {
	
	private boolean coverOnTop = false;
	private float depth = 1.0f;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeBlockspace requestData = (DatatypeBlockspace) request.data;
		
		double res = requestData.resolution;
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLenght;
		
		int[][][] blockspaceMaterialId = null;
		
		int[][] inputMaterialmap1 = null;
		float[][] inputHeightmap2 = null;
		
		if(inputs.get("input") == null || inputs.get("material") == null) {
			return null;
		}
		
		blockspaceMaterialId = ((DatatypeBlockspace) inputs.get("input")).blockspaceMaterialId;
		inputMaterialmap1 = ((DatatypeMaterialmap) inputs.get("material")).materialMap;
		
		if(inputs.get("depth") != null) {
			inputHeightmap2 = ((DatatypeHeightmap) inputs.get("depth")).getHeightmap();
		}
		
		//Apply cover on top of existing blocks
		if(coverOnTop) {
			for(int u = 0; u < spw; u++) {
				for(int w = 0; w < spl; w++) {
					
					double remainingDepth = depth;
					if(inputHeightmap2 != null) {
						remainingDepth *= inputHeightmap2[u][w];
					}
					
					int v = sph;
					while(--v >= 0) {
						if(blockspaceMaterialId[u][v][w] != 0) {
							break;
						}
					}
					if(v < 0) {
						continue;
					}
					while(++v < sph && remainingDepth > 0) {
						blockspaceMaterialId[u][v][w] = inputMaterialmap1[u][w];
						remainingDepth -= res;
					}
				}
			}
		}
		//Replace existing blocks from the top down
		else {
			for(int u = 0; u < spw; u++) {
				for(int w = 0; w < spl; w++) {
					
					double remainingDepth = depth;
					if(inputHeightmap2 != null) {
						remainingDepth *= inputHeightmap2[u][w];
					}
					
					int v = sph;
					while(--v >= 0) {
						if(blockspaceMaterialId[u][v][w] != 0) {
							break;
						}
					}
					if(v < 0) {
						continue;
					}
					v++;
					while(--v >= 0 && remainingDepth > 0) {
						blockspaceMaterialId[u][v][w] = inputMaterialmap1[u][w];
						remainingDepth -= res;
					}
				}
			}
		}
		
		
		requestData.blockspaceMaterialId = blockspaceMaterialId;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeBlockspace outputRequestData = (DatatypeBlockspace) outputRequest.data;
		
		DatatypeMaterialmap materialmapRequeatData = new DatatypeMaterialmap(outputRequestData.x, outputRequestData.z, outputRequestData.width, outputRequestData.length, outputRequestData.resolution);
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(outputRequestData.x, outputRequestData.z, outputRequestData.width, outputRequestData.length, outputRequestData.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequestData));
		inputRequests.put("material", new ModuleInputRequest(getInput(1), materialmapRequeatData));
		inputRequests.put("depth", new ModuleInputRequest(getInput(2), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Surface cover";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBlockspace(), "Blockspace"),
				new ModuleInput(new DatatypeMaterialmap(), "Material"),
				new ModuleInput(new DatatypeHeightmap(), "Depth")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		FloatParameterSlider parameterDepth = new FloatParameterSlider("Cover depth", 0.0f, 100.0f, depth);
		BooleanParameterCheckbox parameterDirection = new BooleanParameterCheckbox("Cover on top", coverOnTop);
		
		try {
			parameterDepth.addToGrid(pane, 0);
			parameterDirection.addToGrid(pane, 1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			depth = parameterDepth.getValue();
			coverOnTop = parameterDirection.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> parameterElements = new ArrayList<Element>();
		
		parameterElements.add(new Element("depth", String.valueOf(depth)));
		parameterElements.add(new Element("ontop", String.valueOf(coverOnTop)));
		
		return parameterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("depth")) {
				depth = Float.parseFloat(e.content);
			}
			else if(e.tag.equals("ontop")) {
				coverOnTop = Boolean.parseBoolean(e.content);
			}
		}
	}
}
