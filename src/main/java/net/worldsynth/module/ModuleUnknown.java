/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeNull;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleUnknown extends AbstractModule {
	
	private ArrayList<String> inputConnections = new ArrayList<String>();
	private ArrayList<String> outputConnections = new ArrayList<String>();
	
	String moduleclass;
	private String moduleclassName;
	private Element moduleElement;
	
	public ModuleUnknown(String moduleclass, Element moduleElement) {
		this.moduleclass = moduleclass;
		this.moduleElement = moduleElement;
		
		String[] s = moduleclass.split("\\.");
		moduleclassName = s[s.length-1];
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return null;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Unknown - " + moduleclassName;
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.UNKNOWN;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] in = new ModuleInput[inputConnections.size()];
		for(int i = 0; i < inputConnections.size(); i++) {
			in[i] = new ModuleInput(new DatatypeNull(), inputConnections.get(i));
		}
		return in;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] out = new ModuleOutput[outputConnections.size()];
		for(int i = 0; i < outputConnections.size(); i++) {
			out[i] = new ModuleOutput(new DatatypeNull(), outputConnections.get(i));
		}
		return out;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		pane.add(new Label("This is an unknown module.\n"
				+ "The module might be unavailable because it is part of a missing library,\n"
				+ "or the module might be removed from newer versions of a third party libray\n"
				+ "or the WorldSynth standard library."
				+ "\n\nModule:\n" + moduleclass + ""),
				0, 0);
		return null;
	}

	@Override
	public ArrayList<Element> toElementList() {
		return moduleElement.elements;
	}

	@Override
	public void fromElement(Element element) {
		moduleElement = element;
	}
	
	public void addInput(String inputName) {
		inputConnections.add(inputName);
		reregisterIO();
	}
	
	public void addOutput(String outputName) {
		outputConnections.add(outputName);
		reregisterIO();
	}
}
