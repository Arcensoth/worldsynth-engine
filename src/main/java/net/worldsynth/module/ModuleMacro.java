/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.common.WorldSynthCore;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.macro.AbstractModuleMacroEntry;
import net.worldsynth.modulewrapper.ModuleWrapper;
import net.worldsynth.synth.SubSynth;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.io.Element;
import net.worldsynth.synth.io.ProjectReader;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;
import net.worldsynth.util.event.synth.SynthEvent;
import net.worldsynth.util.event.synth.SynthListener;

public class ModuleMacro extends AbstractModule {
	
	private ModuleMacro instance = this;
	
	private SubSynth macroSynth;
	
	private SynthChangeListener listener;
	
	ModuleWrapper[] entryDevices;
	ModuleWrapper[] exitDevices;
	
	/** Only for use by the module register to properly register the module */
	public ModuleMacro() {}
	
	public ModuleMacro(ModuleWrapper wrapper) {
		this.wrapper = wrapper;
		setMacroSynth(new SubSynth("New macro", wrapper.getMembersynth()));
	}
	
	public ModuleMacro(File macroSynthFile, ModuleWrapper wrapper) {
		this.wrapper = wrapper;
		setMacroSynth(ProjectReader.readSubSynthFromFile(macroSynthFile, wrapper.getMembersynth()));
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		ModuleOutput exitOutput = request.output;
		ModuleWrapper exitDevice = null;
		for(int i = 0; i < exitDevices.length; i++) {
			if(exitDevices[i].module.getOutput(0) == exitOutput) {
				exitDevice = exitDevices[i];
				break;
			}
		}
		if(exitDevice == null) {
			return null;
		}
		
		//TODO Propagate buildlistener
		return WorldSynthCore.getModuleOutput(macroSynth, exitDevice, request, null);
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Macro";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MACRO;
	}

	@Override
	public ModuleInput[] registerInputs() {
		if(entryDevices == null) {
			entryDevices = new ModuleWrapper[0];
			return null;
		}
		if(entryDevices.length == 0) {
			return null;
		}
		ModuleInput[] in = new ModuleInput[entryDevices.length];
		for(int i = 0; i < entryDevices.length; i++) {
			in[i] = entryDevices[i].module.getInput(0);
		}
		return in;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		if(exitDevices == null) {
			exitDevices = new ModuleWrapper[0];
		}
		if(exitDevices.length == 0) {
			return null;
		}
		ModuleOutput[] out = new ModuleOutput[exitDevices.length];
		for(int i = 0; i < exitDevices.length; i++) {
			out[i] = exitDevices[i].module.getOutput(0);
		}
		return out;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		return null;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(macroSynth.toElement());
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("synth")) {
				setMacroSynth(new SubSynth(e, wrapper.getMembersynth()));
			}
		}
	}
	
	private void setMacroSynth(SubSynth synth) {
		if(macroSynth != null) {
			macroSynth.removeSynthListener(listener);
		}
		
		macroSynth = synth;
		
		ArrayList<ModuleWrapper> tempEntryDevices = new ArrayList<ModuleWrapper>();
		ArrayList<ModuleWrapper> tempExitDevices = new ArrayList<ModuleWrapper>();
		
		for(ModuleWrapper d: synth.getWrapperList()) {
			if(d.module.getModuleCategory() == ModuleCategory.MACRO_ENTRY) {
				tempEntryDevices.add(d);
				((AbstractModuleMacroEntry)d.module).setParentMacroModule(this);
			}
			else if(d.module.getModuleCategory() == ModuleCategory.MACRO_EXIT) {
				tempExitDevices.add(d);
			}
		}
		
		entryDevices = new ModuleWrapper[tempEntryDevices.size()];
		if(tempEntryDevices.size() > 0) {
			entryDevices = tempEntryDevices.toArray(entryDevices);
		}
		
		exitDevices = new ModuleWrapper[tempExitDevices.size()];
		if(tempExitDevices.size() > 0) {
			exitDevices = tempExitDevices.toArray(exitDevices);
		}
		
		reregisterIO();
		
		listener = new SynthChangeListener();
		macroSynth.addSynthListerner(listener);
	}
	
	public Synth getMacroSynth() {
		return macroSynth;
	}
	
	private class SynthChangeListener implements SynthListener {
		@Override
		public void deviceRemoved(SynthEvent event) {
			if(event.getDevice().module.getModuleCategory() == ModuleCategory.MACRO_ENTRY) {
				ModuleWrapper[] newEntryDevices = new ModuleWrapper[entryDevices.length-1];
				
				int j = 0;
				for(int i = 0; i < entryDevices.length; i++) {
					ModuleWrapper d = entryDevices[i];
					if(d == event.getDevice()) {
						continue;
					}
					newEntryDevices[j] = d;
					j++;
				}
				
				entryDevices = newEntryDevices;
				reregisterIO();
			}
			else if(event.getDevice().module.getModuleCategory() == ModuleCategory.MACRO_EXIT) {
				ModuleWrapper[] newExitDevices = new ModuleWrapper[exitDevices.length-1];
				
				int j = 0;
				for(int i = 0; i < exitDevices.length; i++) {
					ModuleWrapper d = exitDevices[i];
					if(d == event.getDevice()) {
						continue;
					}
					newExitDevices[j] = d;
					j++;
				}
				
				exitDevices = newExitDevices;
				reregisterIO();
			}
		}
		
		@Override
		public void deviceModified(SynthEvent event) {
		}
		
		@Override
		public void deviceAdded(SynthEvent event) {
			if(event.getDevice().module.getModuleCategory() == ModuleCategory.MACRO_ENTRY) {
				int currentEntryDeviceCount = 0;
				if(entryDevices != null) {
					currentEntryDeviceCount = entryDevices.length;
				}
				ModuleWrapper[] newEntryDevices = new ModuleWrapper[currentEntryDeviceCount+1];
				
				for(int i = 0; i < currentEntryDeviceCount; i++) {
					newEntryDevices[i] = entryDevices[i];
				}
				((AbstractModuleMacroEntry) event.getDevice().module).setParentMacroModule(instance);
				newEntryDevices[currentEntryDeviceCount] = event.getDevice();
				
				entryDevices = newEntryDevices;
				reregisterIO();
			}
			else if(event.getDevice().module.getModuleCategory() == ModuleCategory.MACRO_EXIT) {
				int currentExitDeviceCount = 0;
				if(exitDevices != null) {
					currentExitDeviceCount = exitDevices.length;
				}
				ModuleWrapper[] newExitDevices = new ModuleWrapper[currentExitDeviceCount+1];
				
				for(int i = 0; i < currentExitDeviceCount; i++) {
					newExitDevices[i] = exitDevices[i];
				}
				newExitDevices[currentExitDeviceCount] = event.getDevice();
				
				exitDevices = newExitDevices;
				reregisterIO();
			}
		}
	}
	
	public AbstractDatatype buildMacroInput(ModuleInputRequest request) {
		return buildInputData(request);
	}
}
