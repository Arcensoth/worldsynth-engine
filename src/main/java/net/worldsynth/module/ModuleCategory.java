/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import javafx.scene.paint.Color;

public enum ModuleCategory implements IModuleCategory {
	UNKNOWN {
		@Override
		public Color classColor() {
			return Color.rgb(50, 50, 50);
		}
	},

	////////////////
	// GENERATORS //
	////////////////

	GENERATOR_SCALAR {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_HEIGHTMAP {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_VALUESPACE {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_COLORMAP {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_BIOMEMAP {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_MATERIALMAP {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_BLOCKSPACE {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_VECTORFIELD {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_VECTORSPACE {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_FEATUREMAP {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_FEATURESPACE {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_OBJECT {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_OBJECTS {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},
	GENERATOR_OVERLAY {
		@Override
		public Color classColor() {
			return Color.rgb(200, 255, 200);
		}
	},

	///////////////
	// MODIFIERS //
	///////////////

	MODIFIER_SCALAR {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_HEIGHTMAP {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_VALUESPACE {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_COLORMAP {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_BIOMEMAP {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_MATERIALMAP {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_BLOCKSPACE {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_VECTORFIELD {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_VECTORSPACE {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_FEATUREMAP {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_FEATURESPACE {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_OBJECT {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},
	MODIFIER_OBJECTS {
		@Override
		public Color classColor() {
			return Color.rgb(255, 255, 100);
		}
	},

	///////////////
	// COMBINERS //
	///////////////

	COMBINER_SCALAR {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_HEIGHTMAP {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_VALUESPACE {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_COLORMAP {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_BIOMEMAP {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_MATERIALMAP {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_BLOCKSPACE {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_VECTORFIELD {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_VECTORSPACE {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_FEATUREMAP {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_FEATURESPACE {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_OBJECT {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	COMBINER_OBJECTS {
		@Override
		public Color classColor() {
			return Color.rgb(100, 100, 255);
		}
	},
	
	///////////////
	// SELECTORS //
	///////////////
	
	SELECTOR_SCALAR {
		@Override
		public Color classColor() {
			return Color.rgb(255, 100, 255);
		}
	},
	SELECTOR_HEIGHTMAP {
		@Override
		public Color classColor() {
			return Color.rgb(255, 100, 255);
		}
	},
	SELECTOR_BIOMEMAP {
		@Override
		public Color classColor() {
			return Color.rgb(255, 100, 255);
		}
	},
	SELECTOR_VALUESPACE {
		@Override
		public Color classColor() {
			return Color.rgb(255, 100, 255);
		}
	},
	SELECTOR_BLOCKSPACE {
		@Override
		public Color classColor() {
			return Color.rgb(255, 100, 255);
		}
	},
	SELECTOR_COLORMAP {
		@Override
		public Color classColor() {
			return Color.rgb(255, 100, 255);
		}
	},
	SELECTOR_VECTORFIELD {
		@Override
		public Color classColor() {
			return Color.rgb(255, 100, 255);
		}
	},
	SELECTOR_MATERIALMAP {
		@Override
		public Color classColor() {
			return Color.rgb(255, 100, 255);
		}
	},

	MACRO {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}

	},
	MACRO_ENTRY {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}

	},
	MACRO_EXIT {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}

	},

	EXPORTER_HEIGHTMAP {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}
	},
	EXPORTER_COLORMAP {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}
	},

	IMPORTER_HEIGHTMAP {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}
	},
	IMPORTER_COLORMAP {
		@Override
		public Color classColor() {
			return Color.rgb(250, 250, 250);
		}
	};
}
