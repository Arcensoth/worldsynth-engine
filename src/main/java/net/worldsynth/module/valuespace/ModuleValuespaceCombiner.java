/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.valuespace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeValuespace;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleValuespaceCombiner extends AbstractModule {
	
	Operation operation = Operation.ADDITION;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeValuespace requestData = (DatatypeValuespace) request.data;
		
		int spw = requestData.spacePointsWidth;
		int sph = requestData.spacePointsHeight;
		int spl = requestData.spacePointsLenght;
		
		float[][][] valuespace = new float[spw][sph][spl];
		
		if(inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If the primary or secondary input is null, there is not enough input and then just return null
			return null;
		}
		
		float[][][] inputMap0 = ((DatatypeValuespace) inputs.get("primary")).valuespace;
		float[][][] inputMap1 = ((DatatypeValuespace) inputs.get("secondary")).valuespace;
		for(int u = 0; u < spw; u++) {
			for(int v = 0; v < sph; v++) {
				for(int w = 0; w < spl; w++) {
					float i0 = inputMap0[u][v][w];
					float i1 = inputMap1[u][v][w];
					float o = 0.0f;
					
					switch (operation) {
					case ADDITION:
						o = i0 + i1;
						break;
					case SUBTRACTION:
						o = i0 - i1;
						break;
					case MULTIPLICATION:
						o = i0 * i1;
						break;
					case DIVISION:
						o = i0 / i1;
						break;
					case MODULO:
						o = i0 % i1;
						break;
					case AVERAGE:
						o = (i0 + i1) / 2;
						break;
					case MAX:
						o = Math.max(i0, i1);
						break;
					case MIN:
						o = Math.min(i0, i1);
						break;
					}
					
					o = Math.min(o, 1);
					o = Math.max(o, 0);
					valuespace[u][v][w] = o;
				}
			}
		}
		
		requestData.valuespace = valuespace;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput(1), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Valuespace combiner";
	}
	
	@Override
	public String getModuleMetaTag() {
		return operation.name().substring(0, 3);
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER_VALUESPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeValuespace(), "Primary input"),
				new ModuleInput(new DatatypeValuespace(), "Secondary input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeValuespace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		EnumParameterDropdownSelector<Operation> parameterOperation = new EnumParameterDropdownSelector<Operation>("Arithmetic operation", Operation.class, operation);
		
		try {
			parameterOperation.addToGrid(pane, 0);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			operation = parameterOperation.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("operation", operation.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("operation")) {
				for(Operation type: Operation.values()) {
					if(e.content.equals(type.name())) {
						operation = type;
						break;
					}
				}
			}
		}
	}
	
	private enum Operation {
		ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, MODULO, AVERAGE, MAX, MIN;
	}
}
