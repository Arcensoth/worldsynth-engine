/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import net.worldsynth.module.biomemap.ModuleBiomemapConstant;
import net.worldsynth.module.biomemap.ModuleBiomemapParameterVoronoi;
import net.worldsynth.module.biomemap.ModuleBiomemapSelector;
import net.worldsynth.module.biomemap.ModuleBiomemapTranslate;
import net.worldsynth.module.blockspace.ModuleBlockspaceCombiner;
import net.worldsynth.module.blockspace.ModuleBlockspaceFilter;
import net.worldsynth.module.blockspace.ModuleBlockspaceFromHeightmap;
import net.worldsynth.module.blockspace.ModuleBlockspaceFromValuespace;
import net.worldsynth.module.blockspace.ModuleBlockspaceHeightClamp;
import net.worldsynth.module.blockspace.ModuleBlockspacePerlinWorm;
import net.worldsynth.module.blockspace.ModuleBlockspaceSurfaceCover;
import net.worldsynth.module.blockspace.ModuleBlockspaceTranslate;
import net.worldsynth.module.colormap.ModuleColormapConstant;
import net.worldsynth.module.colormap.ModuleColormapFromMaterialmap;
import net.worldsynth.module.colormap.ModuleColormapGradient;
import net.worldsynth.module.colormap.ModuleColormapSelector;
import net.worldsynth.module.colormap.ModuleColormapTranslate;
import net.worldsynth.module.featuremap.ModuleFeaturemapSimpleDistribution;
import net.worldsynth.module.featurespace.ModuleFeaturespaceSimpleDistribution;
import net.worldsynth.module.fileio.ModuleColormapExporter;
import net.worldsynth.module.fileio.ModuleColormapImporter;
import net.worldsynth.module.fileio.ModuleHeightmapExporter;
import net.worldsynth.module.fileio.ModuleHeightmapImporter;
import net.worldsynth.module.heightmap.ModuleHeightmapCells;
import net.worldsynth.module.heightmap.ModuleHeightmapClamp;
import net.worldsynth.module.heightmap.ModuleHeightmapCombiner;
import net.worldsynth.module.heightmap.ModuleHeightmapConstant;
import net.worldsynth.module.heightmap.ModuleHeightmapExpander;
import net.worldsynth.module.heightmap.ModuleHeightmapExtentGradient;
import net.worldsynth.module.heightmap.ModuleHeightmapFractalPerlin;
import net.worldsynth.module.heightmap.ModuleHeightmapFromBlockspace;
import net.worldsynth.module.heightmap.ModuleHeightmapGain;
import net.worldsynth.module.heightmap.ModuleHeightmapGradient;
import net.worldsynth.module.heightmap.ModuleHeightmapHeightSelector;
import net.worldsynth.module.heightmap.ModuleHeightmapInverter;
import net.worldsynth.module.heightmap.ModuleHeightmapMacSearlasPseudoErosion;
import net.worldsynth.module.heightmap.ModuleHeightmapRamp;
import net.worldsynth.module.heightmap.ModuleHeightmapScatter;
import net.worldsynth.module.heightmap.ModuleHeightmapSimplePerlin;
import net.worldsynth.module.heightmap.ModuleHeightmapSlope;
import net.worldsynth.module.heightmap.ModuleHeightmapSmothen;
import net.worldsynth.module.heightmap.ModuleHeightmapTerrace;
import net.worldsynth.module.heightmap.ModuleHeightmapTranslate;
import net.worldsynth.module.heightmap.ModuleHeightmapValueNoise;
import net.worldsynth.module.heightmap.ModuleHeightmapWorley;
import net.worldsynth.module.heightmapoverlay.ModuleOverlayGenerator;
import net.worldsynth.module.macro.ModuleHeightmapMacroEntry;
import net.worldsynth.module.macro.ModuleHeightmapMacroExit;
import net.worldsynth.module.materialmap.ModuleMaterialmapConstant;
import net.worldsynth.module.materialmap.ModuleMaterialmapFromBiomemap;
import net.worldsynth.module.materialmap.ModuleMaterialmapFromColormap;
import net.worldsynth.module.objects.ModuleObjectPlacer;
import net.worldsynth.module.objects.ModuleObjectsImporter;
import net.worldsynth.module.scalar.ModuleScalarClamp;
import net.worldsynth.module.scalar.ModuleScalarCombiner;
import net.worldsynth.module.scalar.ModuleScalarConstat;
import net.worldsynth.module.valuespace.ModuleValuespaceCombiner;
import net.worldsynth.module.valuespace.ModuleValuespaceGradient;
import net.worldsynth.module.valuespace.ModuleValuespacePointDistance;
import net.worldsynth.module.valuespace.ModuleValuespaceSimplePerlin;
import net.worldsynth.module.valuespace.ModuleValuespaceTranslate;
import net.worldsynth.module.valuespace.ModuleValuespaceWorley;
import net.worldsynth.module.vectormap.ModuleVectorfieldAbs;
import net.worldsynth.module.vectormap.ModuleVectorfieldCombiner;
import net.worldsynth.module.vectormap.ModuleVectorfieldConstant;
import net.worldsynth.module.vectormap.ModuleVectorfieldDotProduct;
import net.worldsynth.module.vectormap.ModuleVectorfieldHeightmapComponents;
import net.worldsynth.module.vectormap.ModuleVectorfieldHeightmapGradient;
import net.worldsynth.module.vectormap.ModuleVectorfieldNormalize;
import net.worldsynth.module.vectormap.ModuleVectorfieldRotate;
import net.worldsynth.module.vectormap.ModuleVectorfieldTranslate;

public class NativeModuleRegister extends AbstractModuleRegister {
	
	public NativeModuleRegister(File worldSynthConfigDirectory, AbstractModuleRegister[] injectedModuleRegisters) {
		super();
		
		try {
			//Scalars
			registerModule(ModuleScalarConstat.class, "\\Scalar");
			
			registerModule(ModuleScalarClamp.class, "\\Scalar");
			
			registerModule(ModuleScalarCombiner.class, "\\Scalar");
			
			
			//Heightmap
			registerModule(ModuleHeightmapConstant.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapGradient.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapSimplePerlin.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapFractalPerlin.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapValueNoise.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapWorley.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapCells.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapFromBlockspace.class, "\\Heightmap\\Generator");
			registerModule(ModuleHeightmapExtentGradient.class, "\\Heightmap\\Generator");
			
			registerModule(ModuleHeightmapGain.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapTerrace.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapInverter.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapRamp.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapSmothen.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapClamp.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapMacSearlasPseudoErosion.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapExpander.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapSlope.class, "\\Heightmap\\Modifier");
			registerModule(ModuleHeightmapTranslate.class, "\\Heightmap\\Modifier");
			
			registerModule(ModuleHeightmapCombiner.class, "\\Heightmap");
//			registerModule(ModuleHeightmapSelector.class, "\\Heightmap");
			
			registerModule(ModuleHeightmapScatter.class, "\\Heightmap");
			registerModule(ModuleHeightmapHeightSelector.class, "\\Heightmap");
			
			registerModule(ModuleHeightmapExporter.class, "\\Heightmap");
			registerModule(ModuleHeightmapImporter.class, "\\Heightmap");
			
			
			//Biomemap
			registerModule(ModuleBiomemapConstant.class, "\\Biomemap\\Generator");
			registerModule(ModuleBiomemapParameterVoronoi.class, "\\Biomemap\\Generator");
			
			registerModule(ModuleBiomemapTranslate.class, "\\Biomemap\\Modifier");
			
			registerModule(ModuleBiomemapSelector.class, "\\Biomemap");
			
			
			//Valuespace
			registerModule(ModuleValuespaceSimplePerlin.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespaceWorley.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespacePointDistance.class, "\\Valuespace\\Generator");
			registerModule(ModuleValuespaceGradient.class, "\\Valuespace\\Generator");
//			registerModule(ModuleValuespaceProjectedDensity.class, "\\Valuespace\\Generator");
			
			registerModule(ModuleValuespaceTranslate.class, "\\Valuespace\\Modifier");
			
			registerModule(ModuleValuespaceCombiner.class, "\\Valuespace");
			
			
			//Blockspace
			registerModule(ModuleBlockspaceFromValuespace.class, "\\Blockspace\\Generator");
			registerModule(ModuleBlockspaceFromHeightmap.class, "\\Blockspace\\Generator");
			registerModule(ModuleBlockspacePerlinWorm.class, "\\Blockspace\\Generator");
			
			registerModule(ModuleBlockspaceHeightClamp.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceTranslate.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceSurfaceCover.class, "\\Blockspace\\Modifier");
			registerModule(ModuleBlockspaceFilter.class, "\\Blockspace\\Modifier");
			
			registerModule(ModuleBlockspaceCombiner.class, "\\Blockspace");
			
			
			//Colormap
			registerModule(ModuleColormapConstant.class, "\\Colormap\\Generator");
			registerModule(ModuleColormapGradient.class, "\\Colormap\\Generator");
			registerModule(ModuleColormapFromMaterialmap.class, "\\Colormap\\Generator");
			
			registerModule(ModuleColormapTranslate.class, "\\Colormap\\Modifier");
			
			registerModule(ModuleColormapSelector.class, "\\Colormap");
			
			registerModule(ModuleColormapExporter.class, "\\Colormap");
			registerModule(ModuleColormapImporter.class, "\\Colormap");
			
			
			//Vectorfield
			registerModule(ModuleVectorfieldConstant.class, "\\Vectormap\\Generator");
			registerModule(ModuleVectorfieldHeightmapComponents.class, "\\Vectormap\\Generator");
			registerModule(ModuleVectorfieldHeightmapGradient.class, "\\Vectormap\\Generator");
			
			registerModule(ModuleVectorfieldNormalize.class, "\\Vectormap\\Modifier");
			registerModule(ModuleVectorfieldRotate.class, "\\Vectormap\\Modifier");
			registerModule(ModuleVectorfieldTranslate.class, "\\Vectormap\\Modifier");
			
			registerModule(ModuleVectorfieldDotProduct.class, "\\Vectormap\\Other");
			registerModule(ModuleVectorfieldAbs.class, "\\Vectormap\\Other");
			
			registerModule(ModuleVectorfieldCombiner.class, "\\Vectormap");
			
			
			//Heightmap overlay
			registerModule(ModuleOverlayGenerator.class, "\\Overlay");
			
			
			//Materialmap
			registerModule(ModuleMaterialmapConstant.class, "\\Materialmap\\Generator");
			registerModule(ModuleMaterialmapFromColormap.class, "\\Materialmap\\Generator");
			registerModule(ModuleMaterialmapFromBiomemap.class, "\\Materialmap\\Generator");
			
			//Featuremap
			registerModule(ModuleFeaturemapSimpleDistribution.class, "\\Featuremap\\Generator");
			
			//Featurespace
			registerModule(ModuleFeaturespaceSimpleDistribution.class, "\\Featurespace\\Generator");
			
			//Objects
			registerModule(ModuleObjectPlacer.class, "\\Objects");
			registerModule(ModuleObjectsImporter.class, "\\Objects");
			
			//Macro
			registerModule(ModuleMacro.class, "\\Macro");
			registerModule(ModuleHeightmapMacroEntry.class, "\\Macro\\IO");
			registerModule(ModuleHeightmapMacroExit.class, "\\Macro\\IO");
			
			
			//Minecraft
//			registerModule(ModuleMinecraftWorldExport.class, "\\Minecraft save");
//			registerModule(ModuleMinecraftWorldImport.class, "\\Minecraft save");
			

			//Expperimental
//			registerModule(ModuleHeightmapSimpleErosion.class, "\\Erosion");
			
			
			//Load injected libs
			if(injectedModuleRegisters != null) {
				for(AbstractModuleRegister moduleRegister: injectedModuleRegisters) {
					loadModuleRegister(moduleRegister);
				}
			}
			
			//Load external libs
			File libFolder = new File(worldSynthConfigDirectory, "addons");
			if(!libFolder.exists()) {
				libFolder.mkdir();
			}
			loadeExternalLib(libFolder);
			
		} catch (ClassNotModuleExeption e) {
			throw new RuntimeException(e);
		}
	}
	
	private void loadeExternalLib(File libDirectory) {
		
		if(!libDirectory.isDirectory()) {
			System.err.println("Lib directory \"" + libDirectory.getAbsolutePath() + "\" does not exist");
			return;
		}
		
		for(File sub: libDirectory.listFiles()) {
			if(sub.isDirectory()) {
				loadeExternalLib(sub);
			}
			else if(sub.getAbsolutePath().endsWith(".jar")) {
				try {
					ModuleRegisterLoader loader = new ModuleRegisterLoader(sub);
					for(AbstractModuleRegister moduleRegister: loader.getModuleRegisters()) {
						loadModuleRegister(moduleRegister);
					}
				} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException
						| IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void loadModuleRegister(AbstractModuleRegister moduleRegister) {
		for(ModuleEntry moduleEntry: moduleRegister.getRegisteredModuleEntries()) {
			try {
				registerModule(moduleEntry);
			} catch (ClassNotModuleExeption e) {
				e.printStackTrace();
			}
		}
	}
}
