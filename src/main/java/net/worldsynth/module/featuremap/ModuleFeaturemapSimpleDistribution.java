/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.featuremap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.common.math.Permutation;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.standalone.ui.parameters.LongParameterField;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleFeaturemapSimpleDistribution extends AbstractModule {
	private long seed;
	private double scale = 100.0;
	private double probability = 0.2;
	private double displacement = 0.0;
	
	private final int permutationSize = 1024;
	private final int repeat = permutationSize;
	private Permutation permutation;
	
	public ModuleFeaturemapSimpleDistribution() {
		
		seed = new Random().nextLong();
		permutation = new Permutation(seed, permutationSize, 4);
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeFeaturemap requestData = (DatatypeFeaturemap) request.data;
		
		double x = requestData.x;
		double z = requestData.z;
		double width = requestData.width;
		double length = requestData.length;
		double resolution = requestData.resolution;
		
		float[][] inputMap0_probabilityMap = null;
		
		if(inputs.get("probabilitymap") != null) {
			inputMap0_probabilityMap = ((DatatypeHeightmap) inputs.get("probabilitymap")).getHeightmap();
		}
		
		int minXPoint = (int)Math.floor(x/scale);
		int maxXPoint = (int)Math.ceil((x+width)/scale);
		int minZPoint = (int)Math.floor(z/scale);
		int maxZPoint = (int)Math.ceil((z+length)/scale);
		
		ArrayList<double[]> generatedPoints = new ArrayList<double[]>();
		ArrayList<Long> generatedPointSeeds = new ArrayList<Long>();
		
		for(int u = minXPoint; u < maxXPoint; u++) {
			for(int v = minZPoint; v < maxZPoint; v++) {
				int[] hashCoordinates = hashCoordianteAt(u, v);
				
				double xPos = u * scale;
				double zPos = v * scale;
				
				double xDisplace = permutation.lUnitHash(0, hashCoordinates[0], hashCoordinates[1]);
				double zDisplace = permutation.lUnitHash(1, hashCoordinates[0], hashCoordinates[1]);
				
				xPos += -scale*displacement/2.0f + xDisplace*scale*displacement;
				zPos += -scale*displacement/2.0f + zDisplace*scale*displacement;
				
				if(insideBoundary(xPos, zPos, requestData)) {
					double curentProbability = this.probability;
					if(inputMap0_probabilityMap != null) {
						curentProbability = inputMap0_probabilityMap[(int) ((xPos-x)/resolution)][(int) ((zPos-z)/resolution)];
					}
					if(permutation.lUnitHash(2, hashCoordinates[0], hashCoordinates[1]) < curentProbability) {
						generatedPoints.add(new double[]{xPos, zPos});
						generatedPointSeeds.add((long) permutation.lHash(3, hashCoordinates[0], hashCoordinates[1]));
					}
				}
			}
		}
		
		double[][] points = new double[generatedPoints.size()][2];
		generatedPoints.toArray(points);
		
		long[] pointsSeeds = new long[generatedPointSeeds.size()];
		for(int i = 0; i < pointsSeeds.length; i++) {
			pointsSeeds[i] = generatedPointSeeds.get(i);
		}
		
		requestData.points = points;
		requestData.pointSeeds = pointsSeeds;
		return requestData;
	}
	
	private boolean insideBoundary(double x, double z, DatatypeFeaturemap requestData) {
		if(x < requestData.x || x >= requestData.x + requestData.width) {
			return false;
		}
		else if(z < requestData.z || z >= requestData.z + requestData.length) {
			return false;
		}
		return true;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeFeaturemap requestData = (DatatypeFeaturemap) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(requestData.x, requestData.z, requestData.width, requestData.length, requestData.resolution);
		inputRequests.put("probabilitymap", new ModuleInputRequest(getInput(0), heightmapRequestData));
		
		return inputRequests;
	}
	
	private int[] hashCoordianteAt(int x, int z) {
		
		if(repeat > 0) {
			x = x < 0 ? repeat+(x%repeat) : x%repeat;
			z = z < 0 ? repeat+(z%repeat) : z%repeat;
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int zi = (int)z & 255;
		
		return new int[] {xi, zi};
	}

	@Override
	public String getModuleName() {
		return "Simple distribution";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_FEATUREMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Probability")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeFeaturemap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterScale = new DoubleParameterSlider("Scale", 0.0, 1000.0, scale);
		DoubleParameterSlider parameterProbability = new DoubleParameterSlider("Probability", 0.0, 1.0, probability);
		DoubleParameterSlider parameterDisplacement = new DoubleParameterSlider("Displacement", 0.0, 1.0, displacement);
		LongParameterField parameterSeed = new LongParameterField("Seed", seed);
		
		try {
			parameterScale.addToGrid(pane, 0);
			parameterProbability.addToGrid(pane, 1);
			parameterDisplacement.addToGrid(pane, 2);
			parameterSeed.addToGrid(pane, 3);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			scale = parameterScale.getValue();
			probability = parameterProbability.getValue();
			displacement = parameterDisplacement.getValue();
			
			//Sets the seed and regenerates the permutationtable
			seed = parameterSeed.getValue();
			permutation = new Permutation(seed, permutationSize, 4);
		};
		
		return applyHandler;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("seed", String.valueOf(seed)));
		paramenterElements.add(new Element("scale", String.valueOf(scale)));
		paramenterElements.add(new Element("probability", String.valueOf(probability)));
		paramenterElements.add(new Element("displacement", String.valueOf(displacement)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("seed")) {
				seed = Long.parseLong(e.content);
				permutation = new Permutation(seed, permutationSize, 4);
			}
			else if(e.tag.equals("scale")) {
				scale = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("probability")) {
				probability = Double.parseDouble(e.content);
			}
			else if(e.tag.equals("displacement")) {
				displacement = Double.parseDouble(e.content);
			}
		}
	}
}
