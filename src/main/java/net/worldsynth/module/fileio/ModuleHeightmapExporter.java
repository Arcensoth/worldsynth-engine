/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.fileio;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentParameter;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.FileParameterField;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleHeightmapExporter extends AbstractModule {
	
	private WorldExtentParameter exportExtent;
	private File exportFile = new File(System.getProperty("user.home") + "/heightmap_export.png");
	
	@Override
	protected void postInit() {
		exportExtent = getNewExtentParameter();
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("heightmap");
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap heightmapRequestData = (DatatypeHeightmap) outputRequest.data;
		
		inputRequests.put("heightmap", new ModuleInputRequest(getInput(0), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Heightmap exporter";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.EXPORTER_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Heightmap")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Troughput", false)
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ExtentParameterDropdownSelector parameterExtent = exportExtent.getDropdownSelector("Export extent");
		FileParameterField parameterFile = new FileParameterField("Save file", exportFile, true, false, new ExtensionFilter("PNG", "*.png"));
		
		Button exportButton = new Button("Export heightmap");
		exportButton.setOnAction(e -> {
			if(parameterExtent.getValue() == null) {
				return;
			}
			
			float[][] heightmap = getHeightmap(parameterExtent.getValue());
			writeImageToFile(heightmap, parameterFile.getValue());
		});
		
		parameterExtent.addToGrid(pane, 0);
		parameterFile.addToGrid(pane, 1);
		pane.add(exportButton, 1, 2);
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			exportExtent.setExtent(parameterExtent.getValue());
			exportFile = parameterFile.getValue();
		};
		
		return applyHandler;
	}
	
	private float[][] getHeightmap(WorldExtent extent) {
		ModuleInputRequest request = new ModuleInputRequest(getInput(0), new DatatypeHeightmap(extent.getX(), extent.getZ(), extent.getWidth(), extent.getLength()));
		DatatypeHeightmap hmd = (DatatypeHeightmap) buildInputData(request);
		return hmd.getHeightmap();
	}
	
	private void writeImageToFile(float[][] heightmap, File f) {
		try {
			BufferedImage image = new BufferedImage(heightmap.length, heightmap[0].length, BufferedImage.TYPE_USHORT_GRAY);
			for(int x = 0; x < heightmap.length; x++) {
				System.out.println("Converting to buffer: " + (int)(100.0f*(float)x/(float)heightmap.length) + "%");
				for(int y = 0; y < heightmap[0].length; y++) {
					int pixelValue = (int) (heightmap[x][y] * 65535.0);
					image.getRaster().setPixel(x, y, new int[] {pixelValue});
				}
			}
			ImageIO.write(image, "png", f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("extent", exportExtent.getExtentAsString()));
		paramenterElements.add(new Element("file", exportFile.getPath()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("extent")) {
				exportExtent.setExtentAsString(e.content);
			}
			else if(e.tag.equals("file")) {
				exportFile = new File(e.content);
			}
		}
	}
}
