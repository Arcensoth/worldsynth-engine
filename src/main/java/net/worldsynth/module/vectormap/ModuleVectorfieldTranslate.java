/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.vectormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleVectorfieldTranslate extends AbstractModule {
	
	private double xTranslate = 0;
	private double zTranslate = 0;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("input");
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		double xt = xTranslate;
		double zt = zTranslate;
		
		DatatypeScalar scalarDataX = (DatatypeScalar) buildInputData(new ModuleInputRequest(getInput("xTranslate"), new DatatypeScalar()));
		DatatypeScalar scalarDataZ = (DatatypeScalar) buildInputData(new ModuleInputRequest(getInput("zTranslate"), new DatatypeScalar()));
		
		if(scalarDataX != null) {
			xt = scalarDataX.data;
		}
		if(scalarDataZ != null) {
			zt = scalarDataZ.data;
		}
		
		DatatypeVectormap requestData = (DatatypeVectormap) outputRequest.data;
		DatatypeVectormap translatedRequestData = new DatatypeVectormap(requestData.x-xt, requestData.z-zt, requestData.width, requestData.length, requestData.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Primary input"), translatedRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Translate";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_VECTORFIELD;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeVectormap(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "xTranslate"),
				new ModuleInput(new DatatypeScalar(), "zTranslate")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeVectormap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		DoubleParameterSlider parameterXTranslate = new DoubleParameterSlider("Translate x", -100, 100, xTranslate);
		DoubleParameterSlider parameterZTranslate = new DoubleParameterSlider("Translate z", -100, 100, zTranslate);
		
		try {
			parameterXTranslate.addToGrid(pane, 0);
			parameterZTranslate.addToGrid(pane, 1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			xTranslate = parameterXTranslate.getValue();
			zTranslate = parameterZTranslate.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("xtranslate", String.valueOf(xTranslate)));
		paramenterElements.add(new Element("ztranslate", String.valueOf(zTranslate)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			switch(e.tag) {
			case "xtranslate":
				xTranslate = Double.parseDouble(e.content);
				break;
			case "ztranslate":
				zTranslate = Double.parseDouble(e.content);
				break;
			default:
				break;
			}
		}
	}
}
