/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.vectormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.FloatParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleVectorfieldConstant extends AbstractModule {
	
	private float xComponent = 1.0f;
	private float zComponent = 1.0f;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeVectormap requestData = (DatatypeVectormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//----------BUILD----------//
		
		float[][][] field = new float[mpw][mpl][2];
		
		float[] vec = {xComponent, zComponent};
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				field[u][v] = vec;
			}
		}
		
		requestData.vectorField = field;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Constant vectorfield";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_VECTORFIELD;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeVectormap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		FloatParameterSlider parameterXComponent = new FloatParameterSlider("X component", -1.0f, 1.0f, xComponent);
		FloatParameterSlider parameterZComponent = new FloatParameterSlider("Z component", -1.0f, 1.0f, zComponent);
		
		try {
			parameterXComponent.addToGrid(pane, 0);
			parameterZComponent.addToGrid(pane, 1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			xComponent = parameterXComponent.getValue();
			zComponent = parameterZComponent.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("xcomponent", String.valueOf(xComponent)));
		paramenterElements.add(new Element("zcomponent", String.valueOf(zComponent)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			switch (e.tag) {
			case "xcomponent":
				xComponent = Float.parseFloat(e.content);
				break;
			case "zcomponent":
				zComponent = Float.parseFloat(e.content);
				break;
			default:
				break;
			}
		}
	}
}
