/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.vectormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleVectorfieldCombiner extends AbstractModule {

	private Operation operation = Operation.ADDITION;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeVectormap requestData = (DatatypeVectormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		if(inputs.get("input1") == null || inputs.get("input2") == null) {
			//If either of the inputs are null, there is not enough input and then just return null
			return null;
		}
		float[][][] inputField1 = ((DatatypeVectormap) inputs.get("input1")).vectorField;
		float[][][] inputField2 = ((DatatypeVectormap) inputs.get("input2")).vectorField;
		
		//Read mask
		float[][] maskMap = null;
		if(inputs.get("mask") != null) {
			maskMap = ((DatatypeHeightmap) inputs.get("mask")).getHeightmap();
		}
		
		//----------BUILD----------//
		
		float[][][] field = new float[mpw][mpl][2];
		
		if(maskMap == null) {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					switch (operation) {
					case ADDITION:
						field[u][v][0] = inputField1[u][v][0] + inputField2[u][v][0];
						field[u][v][1] = inputField1[u][v][1] + inputField2[u][v][1];
						break;
					case SUBTRACTION:
						field[u][v][0] = inputField1[u][v][0] - inputField2[u][v][0];
						field[u][v][1] = inputField1[u][v][1] - inputField2[u][v][1];
						break;
					} 
				}
			}
		}
		else {
			for(int u = 0; u < mpw; u++) {
				for(int v = 0; v < mpl; v++) {
					switch (operation) {
					case ADDITION:
						field[u][v][0] = inputField1[u][v][0] + inputField2[u][v][0] * maskMap[u][v];
						field[u][v][1] = inputField1[u][v][1] + inputField2[u][v][1] * maskMap[u][v];
						break;
					case SUBTRACTION:
						field[u][v][0] = inputField1[u][v][0] - inputField2[u][v][0] * maskMap[u][v];
						field[u][v][1] = inputField1[u][v][1] - inputField2[u][v][1] * maskMap[u][v];
						break;
					} 
				}
			}
		}
		
		requestData.vectorField = field;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeVectormap vectormapRequestData = (DatatypeVectormap) outputRequest.data;
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(vectormapRequestData.x, vectormapRequestData.z, vectormapRequestData.width, vectormapRequestData.length, vectormapRequestData.resolution);
		
		inputRequests.put("input1", new ModuleInputRequest(getInput("Primary input"), vectormapRequestData));
		inputRequests.put("input2", new ModuleInputRequest(getInput("Secondary input"), vectormapRequestData));
		inputRequests.put("mask", new ModuleInputRequest(getInput("Mask"), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Vectormap combiner";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeVectormap(), "Primary input"),
				new ModuleInput(new DatatypeVectormap(), "Secondary input"),
				new ModuleInput(new DatatypeHeightmap(), "Mask")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeVectormap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		EnumParameterDropdownSelector<Operation> parameterOperation = new EnumParameterDropdownSelector<Operation>("Arithmetic operation", Operation.class, operation);
		
		parameterOperation.addToGrid(pane, 0);
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			operation = parameterOperation.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();

		paramenterElements.add(new Element("operation", operation.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			switch (e.tag) {
			case "operation":
				for(Operation type: Operation.values()) {
					if(e.content.equals(type.name())) {
						operation = type;
						break;
					}
				}
				break;
			default:
				break;
			}
		}
	}
	
	private enum Operation {
		ADDITION, SUBTRACTION;
	}
}
