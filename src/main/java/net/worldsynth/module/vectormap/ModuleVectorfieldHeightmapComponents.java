/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.vectormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleVectorfieldHeightmapComponents extends AbstractModule {
	
	private double gain = 1.0f;
	private CoordinateSystem coordinateSystem = CoordinateSystem.CARTESIAN;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeVectormap requestData = (DatatypeVectormap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		if(inputs.get("pcomponent") == null || inputs.get("scomponent") == null) {
			//If at least one of the component inputs are null, there is not enough input and then just return null
			return null;
		}
		
		float[][] inputMapPComponent = ((DatatypeHeightmap) inputs.get("pcomponent")).getHeightmap();
		float[][] inputMapSComponent = ((DatatypeHeightmap) inputs.get("scomponent")).getHeightmap();
		
		//----------BUILD----------//
		
		float[][][] field = new float[mpw][mpl][2];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				if(coordinateSystem == CoordinateSystem.CARTESIAN) {
					field[u][v] = new float[] {
							(inputMapPComponent[u][v] - 0.5f) * (float) gain,
							(inputMapSComponent[u][v] - 0.5f) * (float) gain
							};
				}
				else if(coordinateSystem == CoordinateSystem.POLAR) {
					field[u][v] = new float[] {
							inputMapPComponent[u][v] * (float) Math.cos(inputMapSComponent[u][v] * Math.PI * 2.0) * (float) gain,
							inputMapPComponent[u][v] * (float) Math.sin(inputMapSComponent[u][v] * Math.PI * 2.0) * (float) gain
							};
				}
			}
		}
		
		requestData.vectorField = field;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeVectormap vectorfieldRequestData = (DatatypeVectormap) outputRequest.data;
		
		DatatypeHeightmap heightmapRequestData = new DatatypeHeightmap(vectorfieldRequestData.x, vectorfieldRequestData.z, vectorfieldRequestData.width, vectorfieldRequestData.length, vectorfieldRequestData.resolution);
		
		inputRequests.put("pcomponent", new ModuleInputRequest(getInput("Primary component"), heightmapRequestData));
		inputRequests.put("scomponent", new ModuleInputRequest(getInput("Secondary component"), heightmapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Component vectorfield";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_VECTORFIELD;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary component"),
				new ModuleInput(new DatatypeHeightmap(), "Secondary component")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeVectormap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterGain = new DoubleParameterSlider("Gain", 0.0, 10.0, gain);
		EnumParameterDropdownSelector<CoordinateSystem> parameterCoordinateSystem = new EnumParameterDropdownSelector<CoordinateSystem>("Component type", CoordinateSystem.class, coordinateSystem);
		
		try {
			parameterGain.addToGrid(pane, 0);
			parameterCoordinateSystem.addToGrid(pane, 1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			gain = parameterGain.getValue();
			coordinateSystem = parameterCoordinateSystem.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("gain", String.valueOf(gain)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			switch(e.tag) {
			case "gain":
				gain = Double.parseDouble(e.content);
			default:
				break;
			}
		}
	}
	
	private enum CoordinateSystem {
		CARTESIAN,
		POLAR;
	}
}
