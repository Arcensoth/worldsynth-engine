/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.vectormap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.common.math.MathHelperScalar;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.datatype.DatatypeVectormap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.DoubleParameterSlider;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleVectorfieldDotProduct extends AbstractModule {
	
	private double gain = 1.0;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		if(inputs.get("input1") == null || inputs.get("input2") == null) {
			//If either of the inputs are null, there is not enough input and then just return null
			return null;
		}
		float[][][] inputField1 = ((DatatypeVectormap) inputs.get("input1")).vectorField;
		float[][][] inputField2 = ((DatatypeVectormap) inputs.get("input2")).vectorField;
		
		//----------BUILD----------//
		
		float[][] dotMap = new float[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				dotMap[u][v] = MathHelperScalar.clamp(dotProduct(inputField1[u][v], inputField2[u][v]) * (float) gain * 0.5f + 0.5f, 0.0f, 1.0f);
			}
		}
		
		requestData.setHeightmap(dotMap);
		
		return requestData;
	}
	
	private float dotProduct(float[] v1, float[] v2) {
		float dot = v1[0] * v2[0] + v1[1] * v2[1];
		return dot;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeHeightmap heightmapRequestData = (DatatypeHeightmap) outputRequest.data;
		
		DatatypeVectormap vectorfieldRequestData = new DatatypeVectormap(heightmapRequestData.x, heightmapRequestData.z, heightmapRequestData.width, heightmapRequestData.length, heightmapRequestData.resolution);
		
		inputRequests.put("input1", new ModuleInputRequest(getInput("Primary input"), vectorfieldRequestData));
		inputRequests.put("input2", new ModuleInputRequest(getInput("Secondary input"), vectorfieldRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Dot product";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_HEIGHTMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeVectormap(), "Primary input"),
				new ModuleInput(new DatatypeVectormap(), "Secondary input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		
		DoubleParameterSlider parameterGain = new DoubleParameterSlider("Gain", 0.0, 1.0, gain);
		
		parameterGain.addToGrid(pane, 0);
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			gain = parameterGain.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("gain", String.valueOf(gain)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			switch (e.tag) {
			case "gain":
				gain = Double.parseDouble(e.content);
				break;
			default:
				break;
			}
		}
	}

}
