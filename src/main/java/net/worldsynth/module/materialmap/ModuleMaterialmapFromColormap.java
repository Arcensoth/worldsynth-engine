/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.materialmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeColormap;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleMaterialmapFromColormap extends AbstractModule {
	
	MaterialEntry[] materialPalette = {new MaterialEntry(MaterialRegistry.getDefaultMaterial())};
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeMaterialmap requestData = (DatatypeMaterialmap) request.data;
		
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}

		float[][][] inputColorMap = ((DatatypeColormap) inputs.get("input")).colorMap;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		int[][] materialmap = new int[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				float r = inputColorMap[u][v][0];
				float g = inputColorMap[u][v][1];
				float b = inputColorMap[u][v][2];
				materialmap[u][v] = selectMaterialFromPalette(r, g, b, materialPalette).getInternalId();
			}
		}
		
		requestData.materialMap = materialmap;
		
		return requestData;
	}
	
	private Material selectMaterialFromPalette(float r, float g, float b, MaterialEntry[] palette) {
		double closestDist = -1;
		Material closestMaterial = palette[0].getMaterial();
		
		for(int i = 0; i < palette.length; i++) {
			Material m = palette[i].getMaterial();
			float pr = (float) m.getWsColor().getRed();
			float pg = (float) m.getWsColor().getGreen();
			float pb = (float) m.getWsColor().getBlue();
			//Difference from color ideal
			double d = Math.sqrt(Math.pow(r - pr, 2) + Math.pow(g - pg, 2) + Math.pow(b - pb, 2));
			
			if(closestDist > d || closestDist < 0) {
				closestMaterial = palette[i].getMaterial();
				closestDist = d;
			}
		}
		
		return closestMaterial;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeMaterialmap materialmapRequestData = (DatatypeMaterialmap) outputRequest.data;
		
		DatatypeColormap colormapRequestData = new DatatypeColormap(materialmapRequestData.x, materialmapRequestData.z, materialmapRequestData.width, materialmapRequestData.length, materialmapRequestData.resolution);
		
		inputRequests.put("input", new ModuleInputRequest(getInput(0), colormapRequestData));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Materialmap from colormap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_MATERIALMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeColormap(), "Primary input")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeMaterialmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		MaterialEntry[] uiPalette = new MaterialEntry[materialPalette.length];
		for(int i = 0; i < uiPalette.length; i++) {
			uiPalette[i] = materialPalette[i].clone();
		}
		
		//////////Material palette//////////
		TableView<MaterialEntry> paletteTable = new TableView<MaterialEntry>();
		paletteTable.setPrefHeight(400);
		paletteTable.setEditable(true);
		TableColumn<MaterialEntry, Material> materialColumn = new TableColumn<MaterialEntry, Material>("Material palette");
		paletteTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		
		materialColumn.setCellValueFactory(new PropertyValueFactory<MaterialEntry, Material>("material"));
		materialColumn.setCellFactory(ComboBoxTableCell.forTableColumn(getSelectableMaterials()));
		materialColumn.setOnEditCommit(t -> {
			t.getRowValue().setMaterial(t.getNewValue());
			if(paletteTable.getItems().get(paletteTable.getItems().size()-1).getMaterial() != Material.NULL) {
				paletteTable.getItems().add(new MaterialEntry(Material.NULL));
			}
		});
		
		paletteTable.getItems().addAll(uiPalette);
		paletteTable.getItems().add(new MaterialEntry(Material.NULL));
		
		paletteTable.getColumns().add(materialColumn);
		pane.add(paletteTable, 0, 0);
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			int materialsCount = 0;
			for(MaterialEntry material: paletteTable.getItems()) {
				if(material.getMaterial() != Material.NULL) {
					materialsCount++;
				}
			}
			MaterialEntry[] entries = new MaterialEntry[materialsCount];
			int i = 0;
			for(MaterialEntry material: paletteTable.getItems()) {
				if(material.getMaterial() != Material.NULL) {
					entries[i] = paletteTable.getItems().get(i).clone();
					i++;
				}
			}
			materialPalette = entries;
		};
		
		return applyHandler;
	}
	
	private Material[] getSelectableMaterials() {
		Material[] alphabeticalCollection = MaterialRegistry.getMaterialsAlphabetically();
		Material[] selectables = new Material[alphabeticalCollection.length+1];
		selectables[0] = Material.NULL;
		for(int i = 0; i < alphabeticalCollection.length; i++) {
			selectables[i+1] = alphabeticalCollection[i];
		}
		return selectables;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		String paletteString = "";
		for(MaterialEntry m: materialPalette) {
			if(m.getMaterial() == Material.NULL) {
				continue;
			}
			paletteString += m.getMaterial().getIdName() + ";";
		}
		
		paramenterElements.add(new Element("palette", paletteString));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("palette")) {
				ArrayList<MaterialEntry> recoveredPalette = new ArrayList<MaterialEntry>();
				String[] paletteString = e.content.split(";");
				for(String s: paletteString) {
					Material material = MaterialRegistry.getMaterial(s);
					recoveredPalette.add(new MaterialEntry(material));
				}
				materialPalette = new MaterialEntry[recoveredPalette.size()];
				recoveredPalette.toArray(materialPalette);
			}
		}
	}
	
	public static class MaterialEntry {
		private final SimpleObjectProperty<Material> material;
		
		private MaterialEntry(Material m) {
			this.material = new SimpleObjectProperty<Material>(m);
		}
		
		public final Material getMaterial() {
			return material.get();
		}
		
		public final void setMaterial(Material m) {
			material.set(m);
		}
		
		@Override
		protected MaterialEntry clone() {
			return new MaterialEntry(getMaterial());
		}
		
		@Override
		public String toString() {
			return getMaterial().getName();
		}
	}
}
