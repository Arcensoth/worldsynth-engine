/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.materialmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import net.worldsynth.biome.Biome;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.datatype.DatatypeMaterialmap;
import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.MaterialParameterSelector;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleMaterialmapFromBiomemap extends AbstractModule {
	
	private Material defaultMaterial = MaterialRegistry.getDefaultMaterial();
	private Entry[] materialPalette = {};
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeMaterialmap requestData = (DatatypeMaterialmap) request.data;
		
		if(inputs.get("input") == null) {
			//If the main input is null, there is not enough input and then just return null
			return null;
		}

		int[][] inputBiomemap0 = ((DatatypeBiomemap) inputs.get("input")).biomeMap;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		int[][] materialmap = new int[mpw][mpl];
		
		for(int u = 0; u < mpw; u++) {
			for(int v = 0; v < mpl; v++) {
				int b = inputBiomemap0[u][v];
				materialmap[u][v] = selectMaterialFromPalette(b, materialPalette, defaultMaterial).getInternalId();
			}
		}
		
		requestData.materialMap = materialmap;
		
		return requestData;
	}
	
	private Material selectMaterialFromPalette(int internalBiomeId, Entry[] palette, Material defaultMaterial) {
		Material returnMaterial = defaultMaterial;
		
		for(int i = 0; i < palette.length; i++) {
			if(palette[i].getBiome().getInternalId() == internalBiomeId) {
				returnMaterial = palette[i].getMaterial();
				break;
			}
		}
		
		return returnMaterial;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		DatatypeMaterialmap mmap = (DatatypeMaterialmap) outputRequest.data;
		
		DatatypeBiomemap hmap = new DatatypeBiomemap(mmap.x, mmap.z, mmap.width, mmap.length, mmap.resolution);
		inputRequests.put("input", new ModuleInputRequest(getInput(0), hmap));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Materialmap from biomemap";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.GENERATOR_MATERIALMAP;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeBiomemap(), "Biomemap")
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeMaterialmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		Entry[] uiPalette = new Entry[materialPalette.length];
		for(int i = 0; i < uiPalette.length; i++) {
			uiPalette[i] = materialPalette[i].clone();
		}
		
		////////// Palettetable //////////
		TableView<Entry> paletteTable = new TableView<Entry>();
		paletteTable.setPrefHeight(400);
		paletteTable.setEditable(true);
		TableColumn<Entry, Biome> biomeColumn = new TableColumn<Entry, Biome>("Biome");
		TableColumn<Entry, Material> materialColumn = new TableColumn<Entry, Material>("Material");
		biomeColumn.setResizable(false);
		biomeColumn.prefWidthProperty().bind(paletteTable.widthProperty().multiply(0.5));
		materialColumn.setResizable(false);
		materialColumn.prefWidthProperty().bind(paletteTable.widthProperty().multiply(0.5));
		paletteTable.getColumns().addAll(biomeColumn, materialColumn);
		
		biomeColumn.setCellValueFactory(new PropertyValueFactory<Entry, Biome>("biome"));
		biomeColumn.setCellFactory(ComboBoxTableCell.forTableColumn(getSelectableBiomes()));
		biomeColumn.setOnEditCommit(t -> {
			t.getRowValue().setBiome(t.getNewValue());
			if(paletteTable.getItems().get(paletteTable.getItems().size()-1).getBiome() != Biome.NULL || paletteTable.getItems().get(paletteTable.getItems().size()-1).getMaterial() != Material.NULL) {
				paletteTable.getItems().add(new Entry(Biome.NULL, Material.NULL));
			}
		});
		
		materialColumn.setCellValueFactory(new PropertyValueFactory<Entry, Material>("material"));
		materialColumn.setCellFactory(ComboBoxTableCell.forTableColumn(getSelectableMaterials()));
		materialColumn.setOnEditCommit(t -> {
			t.getRowValue().setMaterial(t.getNewValue());
			if(paletteTable.getItems().get(paletteTable.getItems().size()-1).getBiome() != Biome.NULL || paletteTable.getItems().get(paletteTable.getItems().size()-1).getMaterial() != Material.NULL) {
				paletteTable.getItems().add(new Entry(Biome.NULL, Material.NULL));
			}
		});
		
		paletteTable.getItems().addAll(uiPalette);
		paletteTable.getItems().add(new Entry(Biome.NULL, Material.NULL));
		GridPane.setColumnSpan(paletteTable, 2);
		pane.add(paletteTable, 0, 1);
		
		//Default material
		MaterialParameterSelector parameterDefaultMaterial = new MaterialParameterSelector("Default material", defaultMaterial);
		parameterDefaultMaterial.addToGrid(pane, 0);
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			Entry[] entries = new Entry[paletteTable.getItems().size()];
			for(int i = 0; i < paletteTable.getItems().size(); i++) {
				entries[i] = paletteTable.getItems().get(i).clone();
			}
			materialPalette = entries;
			
			defaultMaterial = parameterDefaultMaterial.getValue();
		};
		
		return applyHandler;
	}
	
	private Biome[] getSelectableBiomes() {
		Biome[] alphabeticalCollection = BiomeRegistry.getBiomesAlphabetically();
		Biome[] selectables = new Biome[alphabeticalCollection.length+1];
		selectables[0] = Biome.NULL;
		for(int i = 0; i < alphabeticalCollection.length; i++) {
			selectables[i+1] = alphabeticalCollection[i];
		}
		return selectables;
	}
	
	private Material[] getSelectableMaterials() {
		Material[] alphabeticalCollection = MaterialRegistry.getMaterialsAlphabetically();
		Material[] selectables = new Material[alphabeticalCollection.length+1];
		selectables[0] = Material.NULL;
		for(int i = 0; i < alphabeticalCollection.length; i++) {
			selectables[i+1] = alphabeticalCollection[i];
		}
		return selectables;
	}
	
	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> parameterElements = new ArrayList<Element>();
		
		String paletteString = "";
		for(Entry paletteEntry: materialPalette) {
			if(paletteEntry.getBiome() == Biome.NULL || paletteEntry.getMaterial() == Material.NULL) {
				continue;
			}
			paletteString += paletteEntry.getMaterial().getIdName() + ",";
			paletteString += paletteEntry.getBiome().getIdName() + ";";
		}
		
		parameterElements.add(new Element("defaultmaterial", defaultMaterial.getIdName()));
		parameterElements.add(new Element("materialpalette", paletteString));
		
		return parameterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("defaultmaterial")) {
				defaultMaterial = MaterialRegistry.getMaterial(e.content);
			}
			else if(e.tag.equals("materialpalette")) {
				ArrayList<Entry> recoveredPalette = new ArrayList<Entry>();
				String[] stringPalette = e.content.split(";");
				for(String s: stringPalette) {
					String[] stringPaletteEntry = s.split(",");
					Material material = MaterialRegistry.getMaterial(stringPaletteEntry[0]);
					Biome biome = BiomeRegistry.getBiome(stringPaletteEntry[1]);
					
					recoveredPalette.add(new Entry(biome, material));
				}
				materialPalette = new Entry[recoveredPalette.size()];
				recoveredPalette.toArray(materialPalette);
			}
		}
	}
	
	public static class Entry {
		private final SimpleObjectProperty<Biome> biome;
		private final SimpleObjectProperty<Material> material;
		
		public Entry(Biome biome, Material material) {
			this.material = new SimpleObjectProperty<Material>(material);
			this.biome = new SimpleObjectProperty<Biome>(biome);
		}
		
		public final Biome getBiome() {
			return biome.get();
		}
		
		public final void setBiome(Biome biome) {
			this.biome.set(biome);
		}
		
		public final Material getMaterial() {
			return material.get();
		}
		
		public final void setMaterial(Material material) {
			this.material.set(material);
		}
		
		@Override
		protected Entry clone() {
			return new Entry(getBiome(), getMaterial());
		}
	}
}
