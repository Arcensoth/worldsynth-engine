/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.macro;

import java.util.HashMap;
import java.util.Map;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutputRequest;

public abstract class AbstractModuleMacroEntry extends AbstractModuleMacroIO {
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		if(getMacroModule() == null) {
			return null;
		}
		
		ModuleInputRequest inRequest = new ModuleInputRequest(getInput(0), request.data);
		return getMacroModule().buildMacroInput(inRequest);
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		return inputRequests;
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MACRO_ENTRY;
	}
}
