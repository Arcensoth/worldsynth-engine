/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.macro;

import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleOutput;

public class ModuleHeightmapMacroExit extends AbstractModuleMacroExit {


	@Override
	public String getModuleName() {
		return "Macro exit (heightmap)";
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] in = {
				new ModuleInput(new DatatypeHeightmap(), "Macro exit [" + macroIoId + "]")
		};
		return in;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] out = {
				new ModuleOutput(new DatatypeHeightmap(), "Macro exit [" + macroIoId + "]", true)
		};
		return out;
	}
}
