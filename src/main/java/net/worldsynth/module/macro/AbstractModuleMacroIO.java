/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.macro;

import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.ModuleMacro;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public abstract class AbstractModuleMacroIO extends AbstractModule {
	
	private ModuleMacro macroModule;
	
	private static int nextMacroIoId = 0;
	protected int macroIoId = -1;
	
	@Override
	protected void preInit() {
		if(macroIoId == -1) {
			macroIoId = nextMacroIoId();
		}
	}
	
	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		return e -> {};
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("macroioid", String.valueOf(macroIoId)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("macroioid")) {
				macroIoId = Integer.parseInt(e.content);
				if(macroIoId >= nextMacroIoId) {
					nextMacroIoId = macroIoId + 1;
				}
			}
		}
		reregisterIO();
	}
	
	public void setParentMacroModule(ModuleMacro macroModule) {
		this.macroModule = macroModule;
	}
	
	protected ModuleMacro getMacroModule() {
		return macroModule;
	}
	
	private int nextMacroIoId() {
		return nextMacroIoId++;
	}
}
