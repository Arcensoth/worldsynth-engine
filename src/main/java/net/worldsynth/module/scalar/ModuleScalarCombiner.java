/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.scalar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeScalar;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleScalarCombiner extends AbstractModule {
	
	Operation operation = Operation.ADDITION;
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeScalar requestData = (DatatypeScalar) request.data;
		
		if(inputs.get("primary") == null || inputs.get("secondary") == null) {
			//If the primary or secondary input is null, there is not enough input and then just return null
			return null;
		}
		
		double i0 = (double) ((DatatypeScalar) inputs.get("primary")).data;
		double i1 = (double) ((DatatypeScalar) inputs.get("secondary")).data;
		
		double o = 0.0;
		switch (operation) {
		case ADDITION:
			o = i0 + i1;
			break;
		case SUBTRACTION:
			o = i0 - i1;
			break;
		case MULTIPLICATION:
			o = i0 * i1;
			break;
		case DIVISION:
			o = i0 / i1;
			break;
		case MODULO:
			o = i0 % i1;
			break;
		case AVERAGE:
			o = (i0 + i1) / 2;
			break;
		case MAX:
			o = Math.max(i0, i1);
			break;
		case MIN:
			o = Math.min(i0, i1);
			break;
		}
		
		requestData.data = o;
		
		return requestData;
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("primary", new ModuleInputRequest(getInput(0), outputRequest.data));
		inputRequests.put("secondary", new ModuleInputRequest(getInput(1), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Scalar combiner";
	}
	
	@Override
	public String getModuleMetaTag() {
		return operation.name().substring(0, 3);
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.COMBINER_SCALAR;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {new ModuleInput(new DatatypeScalar(), "Primary input"),
				new ModuleInput(new DatatypeScalar(), "Secondary input")};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {new ModuleOutput(new DatatypeScalar(), "Primary output")};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		
		////////// Parameters //////////
		EnumParameterDropdownSelector<Operation> parameterOperation = new EnumParameterDropdownSelector<Operation>("Arithmetic operation", Operation.class, operation);
		
		try {
			parameterOperation.addToGrid(pane, 0);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		//////////
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			operation = parameterOperation.getValue();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		paramenterElements.add(new Element("operation", operation.name()));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("operation")) {
				for(Operation type: Operation.values()) {
					if(e.content.equals(type.name())) {
						operation = type;
						break;
					}
				}
			}
		}
	}
	
	private enum Operation {
		ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, MODULO, AVERAGE, MAX, MIN;
	}
}
