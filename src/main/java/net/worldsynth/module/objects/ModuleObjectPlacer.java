/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.module.objects;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.LocatedCustomObject;
import net.worldsynth.customobject.file.Bo2CustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeFeaturemap;
import net.worldsynth.datatype.DatatypeFeaturespace;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.datatype.DatatypeObjects;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.standalone.ui.parameters.IntegerParameterField;
import net.worldsynth.synth.io.Element;
import net.worldsynth.util.event.module.ModuleApplyParametersEvent;

public class ModuleObjectPlacer extends AbstractModule {
	
	private final LayerManager layerManager = new LayerManager();
	
	private int placementRadius = 16;
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		if(isBypassed()) {
			inputRequests.put("input", new ModuleInputRequest(getInput(0), outputRequest.data));
			return inputRequests;
		}
		
		DatatypeBlockspace rd = (DatatypeBlockspace) outputRequest.data;
		double x = rd.x - placementRadius;
		double y = rd.y;
		double z = rd.z - placementRadius;
		double width = rd.width + placementRadius*2D;
		double height = rd.height;
		double length = rd.length + placementRadius*2D;
		double res = rd.resolution;
		DatatypeBlockspace blockspaceInput = new DatatypeBlockspace(x, y, z, width, height, length, res);
		inputRequests.put("input", new ModuleInputRequest(getInput(0), blockspaceInput));
		
		for(ObjectLayer layer: layerManager.getLayers()) {
			if(!layer.isActive()) {
				continue;
			}
			AbstractDatatype placementRequestData = new DatatypeMultitype(new DatatypeFeaturemap(x, z, width, length, rd.resolution), new DatatypeFeaturespace(x, y, z, width, height, length, rd.resolution));
			inputRequests.put(layer.getName() + " placement", new ModuleInputRequest(getInput(layer.getName() + " placement"), placementRequestData));
		}
		
		return inputRequests;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		
		//----------READ INPUTS----------//
		
		if(isBypassed()) {
			return inputs.get("input");
		}
		else if(inputs.get("input") == null) {
			return null;
		}
		DatatypeBlockspace inputBlockspace = (DatatypeBlockspace) inputs.get("input");
		
		double ix = inputBlockspace.x - placementRadius;
		double iy = inputBlockspace.y;
		double iz = inputBlockspace.z - placementRadius;
		double iwidth = inputBlockspace.width + placementRadius*2D;
		double iheight = inputBlockspace.height;
		double ilength = inputBlockspace.length + placementRadius*2D;
		double ires = inputBlockspace.resolution;
		
		//----------BUILD----------//
		
		DatatypeBlockspace outputBlockspace = extractSubspace(inputBlockspace, (DatatypeBlockspace) request.data);
		
		//Iterate on all the layers
		for(ObjectLayer layer: layerManager.getLayers()) {
			if(!layer.isActive()) {
				continue;
			}
			if(inputs.get(layer.name + " placement") == null) {
				continue;
			}
			
			double[][] featruepoints = null;
			long[] featureSeeds = null;
			if(inputs.get(layer.name + " placement") instanceof DatatypeFeaturemap) {
				DatatypeFeaturemap featuremap = (DatatypeFeaturemap) inputs.get(layer.name + " placement");
				featureSeeds = featuremap.pointSeeds;
				
				featruepoints = new double[featuremap.points.length][3];
				for(int i = 0; i < featruepoints.length; i++) {
					//Find the surface height where the an object is to be placed
					featruepoints[i][0] = featuremap.points[i][0];
					featruepoints[i][2] = featuremap.points[i][1];
					featruepoints[i][1] = getBlockspaceSurfaceHeightAt(inputBlockspace, featruepoints[i][0], featruepoints[i][2]);
				}
				
			}
			else if(inputs.get(layer.name + " placement") instanceof DatatypeFeaturespace) {
				DatatypeFeaturespace featurespace = (DatatypeFeaturespace) inputs.get(layer.name + " placement");
				featruepoints = featurespace.points;
				featureSeeds = featurespace.pointSeeds;
			}
			
			if(layer.hasInternalObjects()) {
				//Place objects from layer object set
				for(int i = 0; i < featruepoints.length; i++) {
					//Get an object from the layer cached objects according to the feature seed
					CustomObject object = layer.getCachedObjects()[(int) (featureSeeds[i] % layer.getCachedObjects().length)];
					//Create a located object
					LocatedCustomObject locatedObject = new LocatedCustomObject(featruepoints[i][0], featruepoints[i][1], featruepoints[i][2], featureSeeds[i], object);
					//Place the object into the blockspace
					placeObjectInBlockspace(outputBlockspace, locatedObject);
				}
			}
			else {
				//Request a set of objects from the layer objects input
				LocatedCustomObject[] requestObjects = new LocatedCustomObject[featruepoints.length];
				for(int i = 0; i < featruepoints.length; i++) {
					//Create a located custom object for ues in the objects input request
					requestObjects[i] = new LocatedCustomObject(featruepoints[i][0], featruepoints[i][1], featruepoints[i][2], featureSeeds[i]);
				}
				//Request the objects to be placed at by this layer
				DatatypeObjects layerObjects = (DatatypeObjects) buildInputData(new ModuleInputRequest(getInput(layer.getName() + " objects"), new DatatypeObjects(requestObjects, ix, iy, iz, iwidth, iheight, ilength, ires)));
				if(layerObjects == null) {
					continue;
				}
				
				//Place objects
				for(LocatedCustomObject locatedObject: layerObjects.getLocatedObjects()) {
					placeObjectInBlockspace(outputBlockspace, locatedObject);
				}
			}
		}
		
		return outputBlockspace;
	}
	
	private DatatypeBlockspace extractSubspace(DatatypeBlockspace parrentSpace, DatatypeBlockspace subspace) {
		int[][][] subspaceMaterials = new int[subspace.spacePointsWidth][subspace.spacePointsHeight][subspace.spacePointsLenght];
		
		int blockspaceExtension = (int) Math.ceil(placementRadius/subspace.resolution);
		
		for(int u = 0; u < subspace.spacePointsWidth; u++) {
			for(int v = 0; v < subspace.spacePointsHeight; v++) {
				for(int w = 0; w < subspace.spacePointsLenght; w++) {
					subspaceMaterials[u][v][w] = parrentSpace.blockspaceMaterialId[u+blockspaceExtension][v][w+blockspaceExtension];
				}
			}
		}
		
		subspace.blockspaceMaterialId = subspaceMaterials;
		return subspace;
	}
	
	private int getBlockspaceSurfaceHeightAt(DatatypeBlockspace blockspace, double x, double z) {
		x -= blockspace.x;
		z -= blockspace.z;
		
		x /= blockspace.resolution;
		z /= blockspace.resolution;
		
		int ix = (int) Math.floor(x);
		int iz = (int) Math.floor(z);
		
		if(ix < 0 || iz < 0 || ix >= blockspace.spacePointsWidth  || iz >= blockspace.spacePointsLenght) {
			return -1;
		}
		
		int iy = blockspace.spacePointsHeight-1;
		while(blockspace.blockspaceMaterialId[ix][iy][iz] == 0 && iy > 0) {
			iy--;
		}
		return iy;
	}
	
	private void placeObjectInBlockspace(DatatypeBlockspace blockspace, LocatedCustomObject locatedObject) {
		double x = locatedObject.getX();
		double y = locatedObject.getY();
		double z = locatedObject.getZ();
		int[][] blocks = locatedObject.getObject().getBlocks();
		
		double blockspaceX = blockspace.x;
		double blockspaceZ = blockspace.z;
		double blockspaceRes = blockspace.resolution;
		
		int spw = blockspace.spacePointsWidth;
		int sph = blockspace.spacePointsHeight;
		int spl = blockspace.spacePointsLenght;
		
		//Convert global object position to local blockspace position
		
		x -= blockspaceX;
		z -= blockspaceZ;
		
		x /= blockspaceRes;
		z /= blockspaceRes;
		
		//Iterate trough the blocks
		for(int[] b: blocks) {
			int materialId = b[3];
			
			//Convert local object block positions to local blockspace positions
			double bx = ((double) b[0]) / blockspaceRes + x;
			double by = ((double) b[1]) / blockspaceRes + y;
			double bz = ((double) b[2]) / blockspaceRes + z;
			
			int ix = (int) Math.floor(bx);
			int iy = (int) Math.floor(by);
			int iz = (int) Math.floor(bz);
			
			//Ignore blocks thatland outside the blockspace
			if(ix < 0 || iy < 0 || iz < 0 || ix >= spw || iy >= sph || iz >= spl) {
				continue;
			}
			
			blockspace.blockspaceMaterialId[ix][iy][iz] = materialId;
		}
	}
	
	@Override
	public String getModuleName() {
		return "Objects placer";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER_BLOCKSPACE;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ArrayList<ModuleInput> inputs = new ArrayList<ModuleInput>();
		inputs.add(new ModuleInput(new DatatypeBlockspace(), "Blockspace"));
		for(ObjectLayer layer: layerManager.getLayers()) {
			inputs.add(new ModuleInput(new DatatypeMultitype(new DatatypeFeaturemap(), new DatatypeFeaturespace()), layer.getName() + " placement"));
			if(!layer.hasInternalObjects()) {
				inputs.add(new ModuleInput(new DatatypeObjects(), layer.getName() + " objects"));
			}
		}
		
		return inputs.toArray(new ModuleInput[inputs.size()]);
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	protected EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ListView<ObjectLayer> layerListView = new ListView<ObjectLayer>();
		ListView<File> objectListView = new ListView<>();
		
		
		IntegerParameterField parameterPlacementRadius = new IntegerParameterField("Placement radius:\t", placementRadius);
		parameterPlacementRadius.addToGrid(pane, 0);
		
		/////////////////////
		//Layer list editor//
		/////////////////////
		
		pane.add(new Label("Layers"), 0, 1);
		
		layerListView.setPrefSize(300, 400);
		layerListView.setCellFactory(param -> {
			LayerCell cell = new LayerCell(layerListView);
			cell.setOnEdit(e -> {
				layerListView.getSelectionModel().select(cell.layer);
			});
			return cell;
		});
		layerListView.getItems().setAll(layerManager.getEditorLayers());
		
		ScrollPane layerListPane = new ScrollPane(layerListView);
		GridPane.setColumnSpan(layerListPane, 2);
		pane.add(layerListPane, 0, 2);
		
		Button addLayerButton = new Button("Add layer");
		addLayerButton.setOnAction(e -> {
			layerListView.getItems().add(new ObjectLayer("New Layer"));
		});
		pane.add(addLayerButton, 0, 3);
		
		//////////////////////////
		//Layer file list editor//
		//////////////////////////
		
		Button addObjectsButton = new Button("Add objects");
		addObjectsButton.setDisable(true);
		Button removeObjectsButton = new Button("Remove objects");
		removeObjectsButton.setDisable(true);
		
		Label fileListLabel = new Label("File list          Layer:");
		GridPane.setColumnSpan(fileListLabel, 3);
		pane.add(fileListLabel, 2, 1);
		
		layerListView.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends ObjectLayer> observable, ObjectLayer oldValue, ObjectLayer newValue) -> {
			if(newValue == null) {
				fileListLabel.setText("File list          Layer:");
				objectListView.setItems(null);
				addObjectsButton.setDisable(true);
				removeObjectsButton.setDisable(true);
				return;
			}
			if(!(newValue.objectFiles instanceof ObservableList)) {
				newValue.objectFiles = FXCollections.observableArrayList(newValue.objectFiles);
			}
			fileListLabel.setText("File list          Layer: " + newValue.getName());
			objectListView.setItems((ObservableList<File>) newValue.objectFiles);
			addObjectsButton.setDisable(false);
			removeObjectsButton.setDisable(false);
		});

		objectListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		objectListView.setPrefSize(600, 400);
		GridPane.setColumnSpan(objectListView, 3);
		pane.add(objectListView, 2, 2);
		
		addObjectsButton.setOnAction(e -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.getExtensionFilters().add(new ExtensionFilter("Object", "*.BO2"));
			List<File> selectedFiles = fileChooser.showOpenMultipleDialog(addObjectsButton.getScene().getWindow());
			if(selectedFiles != null) {
				layerListView.getSelectionModel().getSelectedItem().objectFiles.addAll(selectedFiles);
			}
		});
		
		removeObjectsButton.setOnAction(e -> {
			layerListView.getSelectionModel().getSelectedItem().objectFiles.removeAll(objectListView.getSelectionModel().getSelectedItems());
			
		});
		
		pane.add(addObjectsButton, 2, 3);
		pane.add(removeObjectsButton, 3, 3);
		
		//----------------
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			layerManager.applyEditorLayers(layerListView.getItems());
			placementRadius = parameterPlacementRadius.getValue();
			reregisterIO();
		};
		
		return applyHandler;
	}

	@Override
	public ArrayList<Element> toElementList() {
		ArrayList<Element> paramenterElements = new ArrayList<Element>();
		
		ArrayList<Element> layerElements = new ArrayList<Element>();
		for(ObjectLayer layer: layerManager.getLayers()) {
			layerElements.add(layer.toElement());
		}
		paramenterElements.add(new Element("layers", layerElements));
		paramenterElements.add(new Element("placementradius", String.valueOf(placementRadius)));
		
		return paramenterElements;
	}

	@Override
	public void fromElement(Element element) {
		for(Element e: element.elements) {
			if(e.tag.equals("layers")) {
				layerManager.clearLayers();
				for(Element layerElement: e.elements) {
					layerManager.addLayer(new ObjectLayer(layerElement));
				}
			}
			else if(e.tag.equals("placementradius")) {
				placementRadius = Integer.valueOf(e.content);
			}
		}
		reregisterIO();
	}
	
	private class ObjectLayer {
		
		private String name;
		private boolean active = true;
		
		private List<File> objectFiles = new ArrayList<File>();
		private CustomObject[] cachedObjects = null;
		
		public ObjectLayer(String name) {
			this.name = name;
		}
		
		public ObjectLayer(Element element) {
			fromElement(element);
		}
		
		public String getName() {
			return name;
		}
		
		public boolean isActive() {
			return active;
		}
		
		public boolean hasInternalObjects() {
			return objectFiles.size() > 0;
		}
		
		public CustomObject[] getCachedObjects() {
			if(cachedObjects == null && objectFiles.size() > 0) {
				cachedObjects = readObjectsFromFile(objectFiles);
			}
			return cachedObjects;
		}
		
		private CustomObject[] readObjectsFromFile(List<File> objectFiles) {
			CustomObject[] objects = new CustomObject[objectFiles.size()];
			
			for(int i = 0; i < objects.length; i++) {
				File objectFile = objectFiles.get(i);
				if(!objectFile.getAbsolutePath().toLowerCase().endsWith(".bo2")) {
					return null;
				}
				
				try {
					objects[i] = new Bo2CustomObject(objectFile);
				} catch (IOException ex) {
					ex.printStackTrace();
					return null;
				}
			}
			
			return objects;
		}
		
		public Element toElement() {
			ArrayList<Element> paramenterElements = new ArrayList<Element>();
			
			paramenterElements.add(new Element("name", name));
			paramenterElements.add(new Element("active", String.valueOf(isActive())));
			
			if(objectFiles != null) {
				if(objectFiles.size() > 0) {
					String filePathStringList = "";
					boolean firstEntry = true;
					for(File f: objectFiles) {
						if(firstEntry) {
							firstEntry = false;
						}
						else {
							filePathStringList += ";";
						}
						filePathStringList += f.getAbsolutePath();
					}
					
					paramenterElements.add(new Element("filelist", filePathStringList));
				}
			}
			
			return new Element("layer", paramenterElements);
		}
		
		public void fromElement(Element element) {
			for(Element e: element.elements) {
				if(e.tag.equals("name")) {
					name = e.content;
				}
				else if(e.tag.equals("active")) {
					active = Boolean.parseBoolean(e.content);
				}
				else if(e.tag.equals("filelist")) {
					objectFiles = new ArrayList<File>();
					String[] stringFileList = e.content.split(";");
					for(String s: stringFileList) {
						objectFiles.add(new File(s));
					}
					cachedObjects = null;
				}
			}
		}
		
		@Override
		protected ObjectLayer clone() throws CloneNotSupportedException {
			ObjectLayer clone = new ObjectLayer(name);
			if(objectFiles != null) {
				for(File f: objectFiles) {
					clone.objectFiles.add(new File(f, ""));
				}
			}
			return clone;
		}
	}
	
	private class LayerManager {
		private List<ObjectLayer> layers = new ArrayList<ObjectLayer>();
		
		public LayerManager() {
			layers.add(new ObjectLayer("Default"));
		}
		
		public void addLayer(ObjectLayer layer) {
			layers.add(layer);
		}
		
		public List<ObjectLayer> getLayers() {
			return layers;
		}
		
		public void clearLayers() {
			layers.clear();
		}
		
		public List<ObjectLayer> getEditorLayers() {
			List<ObjectLayer> editorLayers = new ArrayList<ObjectLayer>();
			try {
				for(ObjectLayer layer: layers) {
					editorLayers.add(layer.clone());
				}
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return editorLayers;
		}
		
		public void applyEditorLayers(List<ObjectLayer> layers) {
			List<ObjectLayer> applyLayers = new ArrayList<ObjectLayer>();
			try {
				for(ObjectLayer layer: layers) {
					applyLayers.add(layer.clone());
				}
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			
			this.layers = applyLayers;
		}
	}
	
	private class LayerCell extends ListCell<ObjectLayer> {
		private ObjectLayer layer;
		
		private final HBox hbox;
		private final TextField nameField = new TextField("");
		private final Button editButton = new Button("Edit");
		private final MenuItem deleteLayerItem = new MenuItem("Delete");
		
		public LayerCell(ListView<ObjectLayer> parrentListView) {
			HBox.setHgrow(nameField, Priority.ALWAYS);
			nameField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
				if(newValue.length() > 0) {
					layer.name = newValue;
				}
			});
			
			deleteLayerItem.setOnAction(e -> {
				parrentListView.getItems().remove(layer);
			});
			editButton.setContextMenu(new ContextMenu(deleteLayerItem));
			
			editButton.setOnAction(e -> {
				getOnEdit().handle(e);
			});
			
			hbox = new HBox(nameField, editButton);
		}

		/**
		 * The edit action, which is invoked whenever the edit button is fired. This may
		 * be due to the user clicking on the edit button with the mouse, or by a touch
		 * event, or by a key press, or if the developer programmatically invokes the
		 * {@link #fire()} method.
		 */
		public final ObjectProperty<EventHandler<ActionEvent>> onEditProperty() {
			return onEdit;
		}

		public final void setOnEdit(EventHandler<ActionEvent> value) {
			onEditProperty().set(value);
		}

		public final EventHandler<ActionEvent> getOnEdit() {
			return onEditProperty().get();
		}

		private ObjectProperty<EventHandler<ActionEvent>> onEdit = new SimpleObjectProperty<EventHandler<ActionEvent>>();

		@Override
		protected void updateItem(ObjectLayer item, boolean empty) {
			super.updateItem(item, empty);
			setText(null);
			setGraphic(null);
			
			if(item != null) {
				layer = item;
				nameField.setText(layer.name);
				setGraphic(hbox);
			}
		}
	}
}
