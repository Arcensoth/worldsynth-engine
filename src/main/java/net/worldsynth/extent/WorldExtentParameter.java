/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.extent;

import javafx.event.EventHandler;
import net.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;

public class WorldExtentParameter {

	private String extentString = null;
	private WorldExtent extent = null;

	private WorldExtentManager manager = null;

	private EventHandler<ExtentEvent> extentHandler;

	public WorldExtentParameter(WorldExtent extent, WorldExtentManager manager) {
		extentHandler = e -> {
			if(e.getEventType() == ExtentEvent.EXTENT_REMOVED) {
				this.extent.removeEventHandler(ExtentEvent.ANY, extentHandler);
				this.extent = null;
				extentString = null;
			}
		};

		setExtent(extent);
		setExtentManager(manager);
		this.extent = extent;
		this.manager = manager;
		if(extent != null) {
			extentString = extent.getName() + "#" + extent.getId();
		}
		else {
			extentString = null;
		}
	}

	public WorldExtentParameter(String extentString) {
		this.extentString = extentString;
	}

	public void setExtentManager(WorldExtentManager manager) {
		this.manager = manager;

		if(manager != null && extent == null && extentString != null) {
			setExtentAsString(extentString);
		}
	}

	public void setExtent(WorldExtent extent) {
		if(this.extent != null) {
			this.extent.removeEventHandler(ExtentEvent.ANY, extentHandler);
		}

		this.extent = extent;

		if(extent != null) {
			extent.addEventHandler(ExtentEvent.ANY, extentHandler);
			extentString = extent.getName() + "#" + extent.getId();
		}
		else {
			extentString = null;
		}
	}

	public WorldExtent getExtent() {
		return extent;
	}

	public void setExtentAsString(String extentString) {
		this.extentString = extentString;
		this.extent = null;

		if(manager != null && extentString != null) {
			String s = extentString.substring(extentString.indexOf("#") + 1);
			long id = Long.parseLong(s);
			setExtent(manager.getWorldExtentById(id));
		}
	}

	public String getExtentAsString() {
		return extentString;
	}

	public ExtentParameterDropdownSelector getDropdownSelector(String parameterName) {
		return new ExtentParameterDropdownSelector(parameterName, manager, extent);
	}
}
