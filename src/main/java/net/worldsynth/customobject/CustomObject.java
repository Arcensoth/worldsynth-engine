/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.customobject;

public class CustomObject {
	/**
	 * Array storing entries of {x, y, z, material}
	 */
	public int[][] blocks;
	
	/**
	 * Unit size of the object
	 */
	public int width, height, length;
	
	/**
	 * Minimum coordinate of voxels in the object
	 */
	public int xMin, yMin, zMin;
	/**
	 * Maximum coordinate of voxels in the object
	 */
	public int xMax, yMax, zMax;
	
	public CustomObject(int[][] blocks) {
		this.blocks = blocks;
		
		if(blocks == null) {
			return;
		}
		else if(blocks.length == 0) {
			return;
		}
		
		xMin = xMax = blocks[0][0];
		yMin = yMax = blocks[0][1];
		zMin = zMax = blocks[0][3];
		
		for(int[] block: blocks) {
			//Check for min and max x
			if(block[0] < xMin) {
				xMin = block[0];
			}
			else if(block[0] > xMax) {
				xMax = block[0];
			}
			
			//Check for min and max y
			if(block[1] < yMin) {
				yMin = block[1];
			}
			else if(block[1] > yMax) {
				yMax = block[1];
			}
			
			//Check for min and max z
			if(block[2] < zMin) {
				zMin = block[2];
			}
			else if(block[2] > zMax) {
				zMax = block[2];
			}
		}
		
		width = xMax - xMin + 1;
		height = yMax - yMin + 1;
		length = zMax - zMin + 1;
	}
	
	public int[][] getBlocks() {
		return blocks;
	}
	
	public CustomObject clone() {
		return new CustomObject(getBlocks());
	}
}
