/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.customobject.file;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import net.worldsynth.customobject.CustomObject;

public abstract class FileIoCustomObject extends CustomObject {
	
	/**
	 * Import the object file specified by file
	 * 
	 * @param file  The file to export to
	 * @param world The world to export area from
	 * @param area  The area of the world to export
	 * @throws IOException
	 */
	public FileIoCustomObject(File file) throws IOException {
		super(new int[0][4]);
		
		// Read the file
		if(!file.exists()) {
			return;
		}

		blocks = readObjectFromFile(file).toArray(new int[0][4]);
		
		if(blocks.length == 0) {
			return;
		}
		
		xMin = xMax = blocks[0][0];
		yMin = yMax = blocks[0][1];
		zMin = zMax = blocks[0][3];
		
		for(int[] block: blocks) {
			//Check for min and max x
			if(block[0] < xMin) {
				xMin = block[0];
			}
			else if(block[0] > xMax) {
				xMax = block[0];
			}
			
			//Check for min and max y
			if(block[1] < yMin) {
				yMin = block[1];
			}
			else if(block[1] > yMax) {
				yMax = block[1];
			}
			
			//Check for min and max z
			if(block[2] < zMin) {
				zMin = block[2];
			}
			else if(block[2] > zMax) {
				zMax = block[2];
			}
		}
		
		width = xMax - xMin + 1;
		height = yMax - yMin + 1;
		length = zMax - zMin + 1;
	}

	/**
	 * Method to read a list of blocks from file
	 * 
	 * @param file The object file to read
	 * @return A list of x, y, z and internal blockID representing the object read
	 *         from file
	 * @throws IOException
	 */
	protected abstract ArrayList<int[]> readObjectFromFile(File file) throws IOException;

	/**
	 * Method to write the object to file
	 * 
	 * @param file The object file to write to
	 * @return True if object was successfully written to file
	 * @throws IOException
	 */
	protected abstract boolean writeObjectToFile(File file) throws IOException;
}
