package net.worldsynth.standalone.ui.parameters;

import java.util.ArrayList;
import java.util.List;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.layout.GridPane;

public abstract class ParameterUiElement<T extends Object> {

	private List<EventHandler<Event>> changeEventHandlers;
	
	public void addChengeEventHandler(EventHandler<Event> handler) {
		if(changeEventHandlers == null) {
			changeEventHandlers = new ArrayList<EventHandler<Event>>();
		}
		
		changeEventHandlers.add(handler);
	}
	
	public void removeChangeEventHandler(EventHandler<Event> handler) {
		if(changeEventHandlers == null) return;
		changeEventHandlers.remove(handler);
	}
	
	protected void notifyChangeHandlers() {
		if(changeEventHandlers == null) return;
		for(EventHandler<Event> handler: changeEventHandlers) {
			handler.handle(null);
		}
	}
	
	public abstract void setDisable(boolean disable);
	
	public abstract T getValue();
	
	public abstract void addToGrid(GridPane pane, int row);
}
