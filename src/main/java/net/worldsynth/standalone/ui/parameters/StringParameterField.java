/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class StringParameterField extends ParameterUiElement<String> {
	private String value;
	
	private Label nameLable;
	private TextField parameterField;
	
	public StringParameterField(String lable, String initValue) {
		this.value = initValue;
				
		nameLable = new Label(lable);
		parameterField = new TextField(String.valueOf(value));
		parameterField.setPrefColumnCount(40);
		
		parameterField.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
			value = parameterField.getText();
			notifyChangeHandlers();
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLable.setDisable(disable);
		parameterField.setDisable(disable);
	}
	
	@Override
	public String getValue() {
		return value;
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterField, 1, row);
	}
}
