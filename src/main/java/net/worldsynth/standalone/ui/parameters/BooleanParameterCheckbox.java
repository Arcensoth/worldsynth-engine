/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class BooleanParameterCheckbox extends ParameterUiElement<Boolean> {
	private boolean value;
	
	private Label nameLable;
	private CheckBox parameterCheckbox;
	
	public BooleanParameterCheckbox(String lable, boolean initValue) {
		this.value = initValue;
		
		nameLable = new Label(lable);
		parameterCheckbox = new CheckBox();
		parameterCheckbox.setSelected(initValue);
		
		parameterCheckbox.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
			value = parameterCheckbox.isSelected();
			notifyChangeHandlers();
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLable.setDisable(disable);
		parameterCheckbox.setDisable(disable);
	}
	
	@Override
	public Boolean getValue() {
		return value;
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterCheckbox, 1, row);
	}
}
