/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

public class WorldSynthColorPicker extends GridPane {
	
	private final ObjectProperty<Color> referanceColorProperty;
	private final ObjectProperty<Color> selectedColorProperty;
	
	private DoubleProperty hue;
    private DoubleProperty saturation;
    private DoubleProperty brightness;
    
	public WorldSynthColorPicker(Color color) {
		getStyleClass().add("worldsynth-color-picker");
		
		referanceColorProperty = new SimpleObjectProperty<Color>(color);
		selectedColorProperty = new SimpleObjectProperty<Color>(color);
		
		hue = new SimpleDoubleProperty(color.getHue());
		saturation = new SimpleDoubleProperty(color.getSaturation());
		brightness = new SimpleDoubleProperty(color.getBrightness());
		
		selectedColorProperty.bind(new ObjectBinding<Color>() {
				{
					bind(hue);
					bind(saturation);
					bind(brightness);
				}
				
				@Override
				protected Color computeValue() {
					return Color.hsb(hue.get(), saturation.get(), brightness.get());
				}
		});
		
		HuebarRect hb = new HuebarRect();
		hb.setPrefSize(20, 300);
		
		ColorRect col = new ColorRect();
		col.setPrefSize(300, 300);
		
		ColorCompareRect colComp = new ColorCompareRect();
		colComp.setPrefSize(300, 20);
		
		setHgap(10.0);
		setVgap(10.0);
		
		add(hb, 1, 0);
		add(col, 0, 0);
		
		setColumnSpan(colComp, 2);
		add(colComp, 0, 1);
	}

	/** {@inheritDoc} */
	@Override
	public String getUserAgentStylesheet() {
		return WorldSynthColorGradientEditor.class.getResource("worldsynthcolorpicker.css").toExternalForm();
	}
	
	public ObjectProperty<Color> referanceColorProperty() {
		return referanceColorProperty;
	}
	
	public void setReferanceColor(Color color) {
		referanceColorProperty.set(color);
	}
	
	public Color getReferanceColor() {
		return referanceColorProperty.get();
	}
	
	public ObjectProperty<Color> selectedColorProperty() {
		return selectedColorProperty;
	}
	
	void setSelectedColor(Color color) {
		hue.set(color.getHue());
		saturation.set(color.getSaturation());
		brightness.set(color.getBrightness());
	}
	
	public Color getSelectedColor() {
		return selectedColorProperty.get();
	}
	
	private class HuebarRect extends Pane {
		public HuebarRect() {
			getStyleClass().add("hue-rect");
			
			Stop[] stops = new Stop[256];
			for(int i = 0; i < 256; i++) {
				double f = ((double)i) / 256.0;
				stops[i] = new Stop(f, Color.hsb(f*360, 1.0, 1.0));
			}
			LinearGradient hueGradient = new LinearGradient(0.0, 0.0, 0.0, 1.0, true, CycleMethod.NO_CYCLE, stops);
			setBackground(new Background(new BackgroundFill(hueGradient, CornerRadii.EMPTY, Insets.EMPTY)));
			
			setOnMouseDragged(e -> {
				double norm = Math.max(0.0, Math.min(1.0, e.getY() / getHeight()));
				hue.set(norm * 360.0);
			});
			setOnMouseClicked(e -> {
				double norm = Math.max(0.0, Math.min(1.0, e.getY() / getHeight()));
				hue.set(norm * 360.0);
			});
			
			Region hueBarIndicator = new Region();
			hueBarIndicator.setMouseTransparent(true);
	        hueBarIndicator.getStyleClass().addAll("hue-indicator");
			hueBarIndicator.layoutYProperty().bind(hue.divide(360.0).multiply(heightProperty()));
			
			getChildren().addAll(hueBarIndicator);
		}
	}
	
	private class ColorRect extends Pane {
		public ColorRect() {
			Pane colorRectHue = new Pane();
			colorRectHue.backgroundProperty().bind(new ObjectBinding<Background>() {
				{
					bind(hue);
				}
				
				@Override
				protected Background computeValue() {
					return new Background(new BackgroundFill(Color.hsb(hue.getValue(), 1.0, 1.0), CornerRadii.EMPTY, Insets.EMPTY));
				}
			});
			
			Pane colorRectOverlayOne = new Pane();
			colorRectOverlayOne.setBackground(new Background(new BackgroundFill(
					new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, 
	                new Stop(0, Color.rgb(255, 255, 255, 1)), 
	                new Stop(1, Color.rgb(255, 255, 255, 0))), 
	                CornerRadii.EMPTY, Insets.EMPTY)));
			
			Pane colorRectOverlayTwo = new Pane();
			colorRectOverlayTwo.setBackground(new Background(new BackgroundFill(
					new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE,
					new Stop(0, Color.rgb(0, 0, 0, 0)),
					new Stop(1, Color.rgb(0, 0, 0, 1))),
					CornerRadii.EMPTY, Insets.EMPTY)));
			
			StackPane colorRectOpacityContainer = new StackPane(colorRectHue, colorRectOverlayOne, colorRectOverlayTwo);
	        
			setOnMouseDragged(e -> {
				double normX = Math.max(0.0, Math.min(1.0, e.getX() / getWidth()));
				double normY = 1.0 - Math.max(0.0, Math.min(1.0, e.getY() / getWidth()));
				saturation.set(normX);
				brightness.set(normY);
			});
			setOnMouseClicked(e -> {
				double normX = Math.max(0.0, Math.min(1.0, e.getX() / getWidth()));
				double normY = 1.0 - Math.max(0.0, Math.min(1.0, e.getY() / getWidth()));
				saturation.set(normX);
				brightness.set(normY);
			});
			
	        Region colorRectIndicator = new Region();
	        colorRectIndicator.setMouseTransparent(true);
	        colorRectIndicator.getStyleClass().addAll("color-indicator");
	        colorRectIndicator.layoutXProperty().bind(saturation.multiply(widthProperty()));
	        colorRectIndicator.layoutYProperty().bind(Bindings.subtract(1, brightness).multiply(heightProperty()));
	        
	        colorRectOpacityContainer.setPrefSize(300, 300);
	        
			getChildren().addAll(colorRectOpacityContainer, colorRectIndicator);
		}
	}
	
	private class ColorCompareRect extends HBox {
		public ColorCompareRect() {
			Pane referanceColorPane = new Pane();
			referanceColorPane.backgroundProperty().bind(new ObjectBinding<Background>() {
				{
					bind(referanceColorProperty);
				}
				
				@Override
				protected Background computeValue() {
					return new Background(new BackgroundFill(referanceColorProperty.get(), CornerRadii.EMPTY, Insets.EMPTY));
				}
			});
			referanceColorPane.setOnMouseClicked(e -> {
				setSelectedColor(getReferanceColor());
			});
			
			Pane selectedColorPane = new Pane();
			selectedColorPane.backgroundProperty().bind(new ObjectBinding<Background>() {
				{
					bind(selectedColorProperty);
				}
				
				@Override
				protected Background computeValue() {
					return new Background(new BackgroundFill(selectedColorProperty.get(), CornerRadii.EMPTY, Insets.EMPTY));
				}
			});
			
			HBox.setHgrow(referanceColorPane, Priority.ALWAYS);
			HBox.setHgrow(selectedColorPane, Priority.ALWAYS);
			getChildren().addAll(referanceColorPane, selectedColorPane);
		}
	}
}
