/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class DoubleParameterField extends ParameterUiElement<Double> {
	private double value;
	
	private Label nameLable;
	private TextField parameterField;
	
	public DoubleParameterField(String lable, double initValue) {
		this.value = initValue;
				
		nameLable = new Label(lable);
		parameterField = new TextField(String.valueOf(value));
		parameterField.setPrefColumnCount(40);
		
		parameterField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				applyChange();
				notifyChangeHandlers();
			}
			
			private void applyChange() {
				String text = parameterField.getText().replace(",", ".");
				try {
					value = Double.parseDouble(text);
					parameterField.setStyle(null);
				} catch (NumberFormatException exception) {
					parameterField.setStyle("-fx-background-color: RED;");
				}
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLable.setDisable(disable);
		parameterField.setDisable(disable);
	}
	
	@Override
	public Double getValue() {
		return value;
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterField, 1, row);
	}
}
