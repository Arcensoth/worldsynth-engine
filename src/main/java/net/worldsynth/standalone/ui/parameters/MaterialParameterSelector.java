/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import net.worldsynth.material.Material;
import net.worldsynth.material.MaterialRegistry;

public class MaterialParameterSelector extends ParameterUiElement<Material> {
	private Material value;
	
	private Label nameLable;
	private ComboBox<Material> parameterDropdownSelector = new ComboBox<Material>();
	
	public MaterialParameterSelector(String lable, Material initValue) {
		this.value = initValue;
		nameLable = new Label(lable);
		
		for(Material material: MaterialRegistry.getMaterialsAlphabetically()) {
			parameterDropdownSelector.getItems().add(material);
		}
		parameterDropdownSelector.getSelectionModel().select(value);
		
		parameterDropdownSelector.valueProperty().addListener(new ChangeListener<Material>() {

			@Override
			public void changed(ObservableValue<? extends Material> observable, Material oldValue, Material newValue) {
				value = newValue;
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLable.setDisable(disable);
		parameterDropdownSelector.setDisable(disable);
	}
	
	@Override
	public Material getValue() {
		return value;
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterDropdownSelector, 1, row);
	}
}
