/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class IntegerParameterSlider extends ParameterUiElement<Integer> {
	private int value;
	
	private Label nameLable;
	private Slider parameterSlider;
	private TextField parameterField;
	
	private boolean adjusting = false;
	
	public IntegerParameterSlider(String lable, int low, int high, int initValue) {
		this.value = initValue;
		
		nameLable = new Label(lable);
		parameterSlider = new Slider(low, high, initValue);
		parameterField = new TextField(String.valueOf(value));
		parameterField.setPrefColumnCount(10);
		
		parameterSlider.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if(adjusting) {
					return;
				}
				
				adjusting = true;
				value = (int) Math.round(parameterSlider.getValue());
				parameterField.setText(String.valueOf(value));
				adjusting = false;
				notifyChangeHandlers();
			}
		});
		
		
		parameterField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(adjusting) {
					return;
				}
				
				adjusting = true;
				String text = parameterField.getText();
				try {
					value = Integer.parseInt(text);
					parameterSlider.setValue(value);
					parameterField.setStyle(null);
					
				} catch (NumberFormatException exception) {
					parameterField.setStyle("-fx-background-color: RED;");
				}
				adjusting = false;
				notifyChangeHandlers();
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLable.setDisable(disable);
		parameterSlider.setDisable(disable);
		parameterField.setDisable(disable);
	}
	
	@Override
	public Integer getValue() {
		return value;
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterSlider, 1, row);
		pane.add(parameterField, 2, row);
	}
}
