/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class EnumParameterDropdownSelector<E extends Enum<E>> extends ParameterUiElement<E> {
	private E value;
	
	private Label nameLable;
	private ComboBox<E> parameterDropdownSelector;
	
	public EnumParameterDropdownSelector(String lable, Class<E> typeClass, E initValue) {
		this.value = initValue;
		
		nameLable = new Label(lable);
		parameterDropdownSelector = new ComboBox<E>();
		
		for(E par: typeClass.getEnumConstants()) {
			parameterDropdownSelector.getItems().add(par);
		}
		parameterDropdownSelector.getSelectionModel().select(value);
		
		parameterDropdownSelector.valueProperty().addListener(new ChangeListener<E>() {

			@Override
			public void changed(ObservableValue<? extends E> observable, E oldValue, E newValue) {
				value = newValue;
				notifyChangeHandlers();
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLable.setDisable(disable);
		parameterDropdownSelector.setDisable(disable);
	}
	
	@Override
	public E getValue() {
		return value;
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterDropdownSelector, 1, row);
	}
}
