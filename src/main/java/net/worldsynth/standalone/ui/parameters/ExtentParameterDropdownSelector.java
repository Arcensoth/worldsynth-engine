/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import net.worldsynth.extent.ExtentEvent;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.extent.WorldExtentManager;

public class ExtentParameterDropdownSelector extends ParameterUiElement<WorldExtent> {
	private WorldExtent value;

	private Label nameLable;
	private ComboBox<WorldExtent> parameterDropdownSelector;

	private boolean ignoreSelectionChange = false;

	public ExtentParameterDropdownSelector(String lable, WorldExtentManager extentManager, WorldExtent initValue) {
		// FIXME The event handler is never removed (potential memory leak?)
		extentManager.addEventHandler(ExtentEvent.ANY, e -> {
			if(e.getEventType() == ExtentEvent.EXTENT_REMOVED && e.getExtent() == value) {
				value = null;
			}
			ignoreSelectionChange = true;
			setExtentDropdownItems(extentManager.getObservableExtentsList());
			parameterDropdownSelector.setValue(value);
			ignoreSelectionChange = false;
		});

		value = initValue;

		nameLable = new Label(lable);
		parameterDropdownSelector = new ComboBox<WorldExtent>();
		setExtentDropdownItems(extentManager.getObservableExtentsList());

		parameterDropdownSelector.getSelectionModel().select(value);

		parameterDropdownSelector.setOnAction(e -> {
			if(!ignoreSelectionChange) {
				value = parameterDropdownSelector.getValue();
				notifyChangeHandlers();
			}
		});
	}

	private void setExtentDropdownItems(ObservableList<WorldExtent> items) {
		parameterDropdownSelector.getItems().clear();
		for(WorldExtent par: items) {
			parameterDropdownSelector.getItems().add(par);
		}
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLable.setDisable(disable);
		parameterDropdownSelector.setDisable(disable);
	}
	
	@Override
	public WorldExtent getValue() {
		return value;
	}

	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterDropdownSelector, 1, row);
	}
}
