/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class FloatParameterSlider extends ParameterUiElement<Float> {
	private float value;
	
	private Label nameLable;
	private Slider parameterSlider;
	private TextField parameterField;
	
	private boolean adjusting = false;
	
	public FloatParameterSlider(String lable, float low, float high, float initValue) {
		this(lable, low, high, initValue, 1.0f);
	}
	
	public FloatParameterSlider(String lable, float low, float high, float initValue, float rescale) {
		this.value = initValue;
		
		nameLable = new Label(lable);
		parameterSlider = new Slider(low, high, initValue);
		DecimalFormat df = new DecimalFormat("#.###");
		df.setRoundingMode(RoundingMode.HALF_UP);
		parameterField = new TextField(df.format(value * rescale));
		parameterField.setPrefColumnCount(10);
		
		parameterSlider.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				if(adjusting) {
					return;
				}
				
				adjusting = true;
				value = (float) parameterSlider.getValue();
				DecimalFormat df = new DecimalFormat("#.###");
				df.setRoundingMode(RoundingMode.HALF_UP);
				parameterField.setText(String.valueOf(value * rescale));
				adjusting = false;
				notifyChangeHandlers();
			}
		});
		
		parameterField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(adjusting) {
					return;
				}
				
				adjusting = true;
				String text = parameterField.getText().replace(",", ".");
				try {
					value = Float.parseFloat(text) / rescale;
					parameterSlider.setValue(value);
					parameterField.setStyle(null);
					
				} catch (NumberFormatException exception) {
					parameterField.setStyle("-fx-background-color: RED;");
				}
				adjusting = false;
				notifyChangeHandlers();
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLable.setDisable(disable);
		parameterSlider.setDisable(disable);
		parameterField.setDisable(disable);
	}
	
	@Override
	public Float getValue() {
		return value;
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterSlider, 1, row);
		pane.add(parameterField, 2, row);
	}
}
