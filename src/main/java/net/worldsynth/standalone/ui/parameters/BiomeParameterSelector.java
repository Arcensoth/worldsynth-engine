/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import net.worldsynth.biome.Biome;
import net.worldsynth.biome.BiomeRegistry;

public class BiomeParameterSelector extends ParameterUiElement<Biome> {
	private Biome value;
	
	private Label nameLable;
	private ComboBox<Biome> parameterDropdownSelector = new ComboBox<Biome>();
	
	public BiomeParameterSelector(String lable, Biome initValue) {
		this.value = initValue;
		nameLable = new Label(lable);
		
		for(Biome biome: BiomeRegistry.getBiomesAlphabetically()) {
			parameterDropdownSelector.getItems().add(biome);
		}
		parameterDropdownSelector.getSelectionModel().select(value);
		
		parameterDropdownSelector.valueProperty().addListener(new ChangeListener<Biome>() {

			@Override
			public void changed(ObservableValue<? extends Biome> observable, Biome oldValue, Biome newValue) {
				value = newValue;
				notifyChangeHandlers();
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLable.setDisable(disable);
		parameterDropdownSelector.setDisable(disable);
	}
	
	@Override
	public Biome getValue() {
		return value;
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterDropdownSelector, 1, row);
	}
}
