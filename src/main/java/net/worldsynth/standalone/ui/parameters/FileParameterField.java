/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import java.io.File;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class FileParameterField extends ParameterUiElement<File> {
	private File value;
	
	private Label nameLable;
	private TextField parameterField;
	private Button directoryDialogButton;
	
	public FileParameterField(String lable, File initValue, boolean save, boolean directory, ExtensionFilter filter) {
		this.value = initValue;
				
		nameLable = new Label(lable);
		parameterField = new TextField(String.valueOf(value));
		parameterField.setPrefColumnCount(50);
		directoryDialogButton = new Button("...");
		
		parameterField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				boolean validValue = false;
				File newParameterFile = new File(newValue);
				if(directory) {
					//Validate directory
					if(newParameterFile.isDirectory()) {
						validValue = true;
					}
				}
				else {
					//Validate file
					if(!newParameterFile.isDirectory()) {
						if(filter != null) {
							for(String ext: filter.getExtensions()) {
								ext = ext.replace("*", "");
								if(newParameterFile.getAbsolutePath().endsWith(ext)) {
									validValue = true;
									break;
								}
							}
						}
					}
				}
				
				if(validValue) {
					parameterField.setStyle(null);
					value = newParameterFile;
					notifyChangeHandlers();
				}
				else {
					parameterField.setStyle("-fx-background-color: RED;");
				}
			}
		});
		
		directoryDialogButton.setOnAction(e -> {
			if(directory) {
				DirectoryChooser directoryChooserDialog = new DirectoryChooser();
				if(value != null) {
					if(value.exists()) {
						directoryChooserDialog.setInitialDirectory(value);
					}
				}
				
				File returnVal = directoryChooserDialog.showDialog(parameterField.getScene().getWindow());
				if(returnVal != null) {
					value = returnVal;
					parameterField.setText(value.getAbsolutePath());
					notifyChangeHandlers();
				}
			}
			else {
				FileChooser fileChooserDialog = new FileChooser();
				if(value != null) {
					if(value.exists()) {
						fileChooserDialog.setInitialDirectory(value.getParentFile());
						fileChooserDialog.setInitialFileName(value.getName());
						
					}
				}
				
				if(filter != null) {
					fileChooserDialog.getExtensionFilters().add(filter);
				}
				
				File returnVal = null;
				if(save) {
					returnVal = fileChooserDialog.showSaveDialog(parameterField.getScene().getWindow());
				}
				else {
					returnVal = fileChooserDialog.showOpenDialog(parameterField.getScene().getWindow());
				}
				
				if(returnVal != null) {
					value = returnVal;
					parameterField.setText(value.getAbsolutePath());
					notifyChangeHandlers();
				}
			}
		});
	}
	
	@Override
	public void setDisable(boolean disable) {
		nameLable.setDisable(disable);
		parameterField.setDisable(disable);
		directoryDialogButton.setDisable(disable);
	}
	
	@Override
	public File getValue() {
		return value;
	}
	
	@Override
	public void addToGrid(GridPane pane, int row) {
		pane.add(nameLable, 0, row);
		pane.add(parameterField, 1, row);
		pane.add(directoryDialogButton, 2, row);
	}
}
