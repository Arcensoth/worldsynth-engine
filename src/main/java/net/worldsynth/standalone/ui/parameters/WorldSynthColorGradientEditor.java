/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.parameters;

import javafx.beans.binding.ObjectBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import net.worldsynth.common.color.ColorGradient;
import net.worldsynth.common.color.ColorGradient.GradientStop;
import net.worldsynth.common.math.MathHelperScalar;

public class WorldSynthColorGradientEditor extends VBox {
	
	private final ObjectProperty<EditorGradient> gradientProperty;
	
	private final ObjectProperty<EditorGradientStop> selectedStopProperty = new SimpleObjectProperty<EditorGradientStop>();
	
	private final Pane colorGradientBar;
	private final WorldSynthColorPicker colorPicker;
	
	public WorldSynthColorGradientEditor(ColorGradient gradient) {
		getStyleClass().add("worldsynth-color-gradient-editor");
		
//		ColorGradient g = new ColorGradient(new float[][] {{0.0f, 0.0f}, {0.2f, 0.4f}, {0.6f, 0.5f}, {1.0f, 1.0f}});
		gradientProperty = new SimpleObjectProperty<EditorGradient>(new EditorGradient(gradient));
		
		colorGradientBar = new GradientBar(gradientProperty.get());
		colorPicker = new WorldSynthColorPicker(Color.TRANSPARENT);
		
		setSpacing(20.0);
		getChildren().addAll(colorGradientBar, colorPicker);
		
		//Handle change in selected gradient stop
		selectedStopProperty.addListener((ObservableValue<? extends EditorGradientStop> observable, EditorGradientStop oldValue, EditorGradientStop newValue) -> {
			if(oldValue != null) {
//				oldValue.setSelected(false);
				oldValue.colorProperty().unbind();
			}
			if(newValue != null) {
				colorPicker.setReferanceColor(newValue.getColor());
				colorPicker.setSelectedColor(newValue.getColor());
				newValue.colorProperty().bind(colorPicker.selectedColorProperty());
			}
		});
	}
	
	/** {@inheritDoc} */
	@Override
	public String getUserAgentStylesheet() {
		return WorldSynthColorGradientEditor.class.getResource("worldsynthcolorgradienteditor.css").toExternalForm();
	}
	
	public ColorGradient getGradient() {
		float[][] gradient = new float[gradientProperty.get().gradientStopsList().size()][4];
		
		for(int i = 0; i < gradient.length; i++) {
			EditorGradientStop stop = gradientProperty.get().gradientStopsList().get(i);
			gradient[i][0] = stop.getOffset();
			gradient[i][1] = (float) stop.getColor().getRed();
			gradient[i][2] = (float) stop.getColor().getGreen();
			gradient[i][3] = (float) stop.getColor().getBlue();
		}
		
		return new ColorGradient(gradient);
	}
	
	private class GradientBar extends Pane {
		
		private final Pane gradientOverlay = new Pane();
		
		public GradientBar(EditorGradient gradient) {
			getStyleClass().addAll("gradient-bar");
			setPrefSize(330, 40);
			
			//Setup background binding for the gradient overlay and add it
			gradientOverlay.backgroundProperty().bind(new GradientBackgroundBinding(gradient));
			gradientOverlay.setPrefSize(getPrefWidth(), getPrefHeight());
			getChildren().add(gradientOverlay);
			
			//Add all editable indicators for gradient stops
			for(EditorGradientStop stop: gradient.gradientStopsList()) {
				getChildren().add(new GradientStopIndicator(stop, this));
			}
			
			//Listen for change in gradient stops and add indicators for added stops and remove indicators for removed stops.
			gradient.gradientStopsList().addListener(new ListChangeListener<EditorGradientStop>() {
				@Override
				public void onChanged(Change<? extends EditorGradientStop> c) {
					while(c.next()) {
						//Add bindings to new stops
						for(EditorGradientStop stop: c.getAddedSubList()) {
							addStopIndicator(stop);
						}
						
						//Remove bindings to removed stops
						for(EditorGradientStop stop: c.getRemoved()) {
							removeStopIndicator(stop);
						}
					}
				}
			});
			
			//Setup mouse listener for adding new gradient stop
			setOnMousePressed(e -> {
				float offset = (float) (e.getX() / getWidth());
				EditorGradientStop newStop = new EditorGradientStop(offset, gradient.getColor(offset));
				gradient.addStop(newStop);
				selectedStopProperty.set(newStop);
			});
		}
		
		private void addStopIndicator(EditorGradientStop stop) {
			getChildren().add(new GradientStopIndicator(stop, this));
		}
		
		private void removeStopIndicator(EditorGradientStop stop) {
			//Find the node indicating the stop
			Node removableIndicator = null;
			for(Node node: getChildren()) {
				if(node instanceof GradientStopIndicator) {
					if(((GradientStopIndicator) node).getStop() == stop) {
						removableIndicator = node;
					}
				}
			}
			//Remove the found stop indicator node
			getChildren().remove(removableIndicator);
		}
	}
	
	private class GradientBackgroundBinding extends ObjectBinding<Background> {
		
		private final EditorGradient gradient;
		
		public GradientBackgroundBinding(EditorGradient gradient) {
			this.gradient = gradient;
			
			//Bind all gradient stops
			for(EditorGradientStop stop: gradient.gradientStopsList()) {
				bind(stop.offsetProperty());
				bind(stop.colorProperty());
			}
			
			//Listen for change in gradient stops and add bindings to added stops and remove bindings to removed stops.
			gradient.gradientStopsList().addListener(new ListChangeListener<EditorGradientStop>() {
				@Override
				public void onChanged(Change<? extends EditorGradientStop> c) {
					while(c.next()) {
						//Add bindings to new stops
						for(EditorGradientStop stop: c.getAddedSubList()) {
							bind(stop.offsetProperty());
							bind(stop.colorProperty());
							invalidate();
						}
						
						//Remove bindings to removed stops
						for(EditorGradientStop stop: c.getRemoved()) {
							unbind(stop.offsetProperty());
							unbind(stop.colorProperty());
							invalidate();
						}
					}
				}
			});
		}
		
		@Override
		protected Background computeValue() {
			return new Background(new BackgroundFill(createColorGradient(gradient), CornerRadii.EMPTY, Insets.EMPTY));
		}
		
		private LinearGradient createColorGradient(EditorGradient gradient) {
			Stop[] stops = new Stop[gradient.gradientStopsList().size()];
			for(int x = 0; x < stops.length; x++) {
				EditorGradientStop stop = gradient.gradientStopsList().get(x);
				stops[x] = new Stop(stop.offsetProperty().get(), stop.colorProperty().get());
			}
			return new LinearGradient(0f, 0f, 1f, 0f, true, CycleMethod.NO_CYCLE, stops);
		}
	}
	
	private class GradientStopIndicator extends Region {
		
		private final EditorGradientStop stop;
		
		public GradientStopIndicator(EditorGradientStop stop, Pane parentGradientBar) {
			getStyleClass().add("stop-indicator");
			
			this.stop = stop;
			
			setCache(true);
			
			backgroundProperty().bind(new ObjectBinding<Background>() {
				{
					bind(stop.colorProperty);
				}
				
				@Override
				protected Background computeValue() {
					return new Background(new BackgroundFill(stop.colorProperty().get(), new CornerRadii(5), Insets.EMPTY));
				}
			});
			
			layoutXProperty().bind(stop.offsetProperty().multiply(parentGradientBar.widthProperty()));
			
			setOnMousePressed(e -> {
				selectedStopProperty.set(stop);
				e.consume();
			});
			
			setOnMouseDragged(e -> {
				if(e.isPrimaryButtonDown()) {
					if(e.getY() < -50 || e.getY() > getHeight()+50) {
						if(gradientProperty.get().gradientStopsList().size() > 1) {
							gradientProperty.get().removeStop(stop);
						}
					}
					stop.offsetProperty().set((float) MathHelperScalar.clamp(stop.offsetProperty().get()+e.getX()/parentGradientBar.getWidth()-getWidth()/2/parentGradientBar.getWidth(), 0.0, 1.0));
				}
			});
		}

		public EditorGradientStop getStop() {
			return stop;
		}
	}
	
	private class EditorGradient {
		private SimpleListProperty<EditorGradientStop> gradientStops = new SimpleListProperty<>(FXCollections.observableArrayList());
		
		public EditorGradient(ColorGradient gradient) {
			ColorGradient g = gradient;
			for(GradientStop stop: g.getStops()) {
				gradientStops.add(new EditorGradientStop(stop.getOffset(), stop.getRed(), stop.getGreen(), stop.getBlue(), stop.getOpacity()));
			}
		}
		
		public ObservableList<EditorGradientStop> gradientStopsList() {
			return gradientStops;
		}
		
		public void addStop(EditorGradientStop stop) {
			gradientStops.add(stop);
		}
		
		public void removeStop(EditorGradientStop stop) {
			gradientStops.remove(stop);
		}
		
		private Color getColor(float offset) {
			gradientStops.sort(null);
			
			int i = 0;
			while(gradientStops.get(Math.min(i, gradientStops.size()-1)).getOffset() < offset && i < gradientStops.size()) {
				i++;
			}
			
			EditorGradientStop stop1 = gradientStops.get(Math.max(0, i-1));
			EditorGradientStop stop2 = gradientStops.get(Math.min(i, gradientStops.size()-1));
			
			float y = (offset - stop1.getOffset()) / (stop2.getOffset() - stop1.getOffset());
			if(!Float.isFinite(y)) {
				y = 1.0f;
			}
			
			Color color1 = stop1.getColor();
			Color color2 = stop2.getColor();
			
			float[] colorComponents1 = new float[] {(float) color1.getRed(), (float) color1.getGreen(), (float) color1.getBlue(), (float) color1.getOpacity()};
			float[] colorComponents2 = new float[] {(float) color2.getRed(), (float) color2.getGreen(), (float) color2.getBlue(), (float) color2.getOpacity()};
			
			float[] colorComponents = new float[4];
			for(int j = 0; j < 4; j++) {
				colorComponents[j] = colorComponents2[j] * y + colorComponents1[j] * (1.0f - y);
			}
			
			return Color.color(colorComponents[0], colorComponents[1], colorComponents[2], colorComponents[3]);
		}
	}
	
	private class EditorGradientStop implements Comparable<EditorGradientStop> {
		private final SimpleFloatProperty offsetProperty;
		private final SimpleObjectProperty<Color> colorProperty;
		
		public EditorGradientStop(float offset, double r, double g, double b, double a) {
			this(offset, Color.color(r, g, b));
		}
		
		public EditorGradientStop(float offset, Color color) {
			offsetProperty = new SimpleFloatProperty(offset);
			colorProperty = new SimpleObjectProperty<Color>(color);
		}
		
		public SimpleFloatProperty offsetProperty() {
			return offsetProperty;
		}
		
		public float getOffset() {
			return offsetProperty.get();
		}
		
		public SimpleObjectProperty<Color> colorProperty() {
			return colorProperty;
		}
		
		public Color getColor() {
			return colorProperty.get();
		}

		@Override
		public int compareTo(EditorGradientStop o) {
			if(getOffset() < o.getOffset()) return -1;
			else if(getOffset() > o.getOffset()) return 1;
			return 0;
		}
	}
}
