/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.preview;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import net.worldsynth.datatype.AbstractDatatype;

public abstract class AbstractPreviewRenderCanvas extends AbstractPreviewRender {

	protected final Canvas canvas;
	
	public AbstractPreviewRenderCanvas() {
		canvas = new Canvas(450, 450);
		getChildren().add(canvas);
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        // Java 9 - snapSize is depricated used snapSizeX() and snapSizeY() accordingly
        final double w = snapSize(getWidth()) - x - snappedRightInset();
        final double h = snapSize(getHeight()) - y - snappedBottomInset();
        canvas.setLayoutX(x);
        canvas.setLayoutY(y);
        canvas.setWidth(w);
        canvas.setHeight(h);
        
        paint();
    }
	
	protected final GraphicsContext getGraphicsContext2D() {
		return canvas.getGraphicsContext2D();
	}
	
	public abstract void pushDataToRender(AbstractDatatype data);
	
	/**
	 * Use getGraphicsContext2D() to get the graphics context
	 */
	protected abstract void paint();
}
