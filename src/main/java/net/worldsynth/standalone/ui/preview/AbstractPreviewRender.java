/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.standalone.ui.preview;

import javafx.scene.layout.Pane;
import net.worldsynth.datatype.AbstractDatatype;

public abstract class AbstractPreviewRender extends Pane {

	public AbstractPreviewRender() {
		setPrefSize(450, 450);
	}
	
	public abstract void pushDataToRender(AbstractDatatype data);
}
