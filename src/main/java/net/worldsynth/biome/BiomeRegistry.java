/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.biome;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import net.worldsynth.common.color.WsColor;

public class BiomeRegistry {
	
	public static ArrayList<Biome> REGISTER = new ArrayList<Biome>();
	
	public BiomeRegistry(File worldSynthConfigDirectory) {
		registerBiomes(new File(worldSynthConfigDirectory, "biomes"));
	}
	
	private void registerBiomes(File biomesDirectory) {
		//Load materials from files
		if(!biomesDirectory.exists()) {
			biomesDirectory.mkdir();
		}
		loadeBiomesLib(biomesDirectory);
	}
	
	private void loadeBiomesLib(File biomessDirectory) {
		if(!biomessDirectory.isDirectory()) {
			System.err.println("Lib directory \"" + biomessDirectory.getAbsolutePath() + "\" does not exist");
			return;
		}
		
		for(File sub: biomessDirectory.listFiles()) {
			if(sub.isDirectory()) {
				loadeBiomesLib(sub);
			}
			else if(sub.getName().endsWith(".csv")) {
				try {
					loadBiomessFromFile(sub);
				} catch (IOException e) {
					System.err.println("Problem occoured while trying to read biome file: " + sub.getName());
					e.printStackTrace();
				}
			}
		}
	}
	
	private void loadBiomessFromFile(File biomesFile) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(biomesFile));
		
		String line = null;
		while((line = reader.readLine()) != null) {
			//Remove any tabs in the line
			line = line.replace("\t", "");
			
			String[] separatedValues = line.split(";");
			
			//Expecting 4 entries
			if(separatedValues.length < 4) {
				continue;
			}
			
			String biomeName = separatedValues[0];
			String biomeIdName = separatedValues[1];
			int biomeIdNumber;
			WsColor biomeColor;
			
			try {
				biomeIdNumber = Integer.parseInt(separatedValues[2]);
			} catch (NumberFormatException e) {
				biomeIdNumber = -1;
			}
			
			//Read color
			if(separatedValues[3].matches("#[0123456789ABCDEF]{6}") || separatedValues[3].matches("\\d{1,3},\\d{1,3},\\d{1,3}")) {
				biomeColor = new WsColor(separatedValues[3]);
			}
			else {
				//No valid color value
				continue;
			}
			
			Biome newMaterial = new Biome(biomeName, biomeIdName, biomeIdNumber, biomeColor);
			REGISTER.add(newMaterial);
		}
		
		reader.close();
	}
	
	public static Biome getDefaultBiome() {
		return REGISTER.get(0);
	}
	
	public static Biome getBiome(int id) {
		for(Biome b: REGISTER) {
			if(id == b.getId()) {
				return b;
			}
		}
		return new Biome("Unknown biome " + id, "unknown", id, WsColor.MAGENTA);
	}
	
	public static Biome getBiome(String idName) {
		if(idName != null) {
			for(Biome b: REGISTER) {
				if(idName.equals(b.getIdName())) {
					return b;
				}
			}
		}
		
		return new Biome("Unknown biome", idName, -1, WsColor.MAGENTA);
	}
	
	public static Biome getBiomeByInternalId(int internalId) {
		if(internalId < 0 || internalId >= REGISTER.size()) {
			return new Biome("Unknown biome i" + internalId, "unknown", internalId, WsColor.MAGENTA);
		}
		return REGISTER.get(internalId);
	}
	
	public static int getInternalId(Biome material) {
		return REGISTER.indexOf(material);
	}
	
	public static Biome[] getBiomesAlphabetically() {
		//Sort biomes alphabetically by name
		Biome[] biomeSort = new Biome[REGISTER.size()];
		REGISTER.toArray(biomeSort);
		
		Arrays.sort(biomeSort);
		
		return biomeSort;
	}
	
	public static int size() {
		return REGISTER.size();
	}
}
