/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.common.color;

import javafx.scene.paint.Color;

public class WsColor {
	
	public static final WsColor MAGENTA = new WsColor(1.0f, 0.0f, 1.0f);
	
	Color fxColor = null;
	
	public final float getRed() { return red; }
	private float red;
	
	public final float getGreen() { return green; }
	private float green;
	
	public final float getBlue() { return blue; }
	private float blue;
	
	public final float getOpacity() { return opacity; }
	private float opacity;
	
	public WsColor(float red, float green, float blue, float opacity) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.opacity = opacity;
	}
	
	public WsColor(float red, float green, float blue) {
		this(red, green, blue, 1.0f);
	}
	
	public WsColor(int red, int green, int blue, int opacity) {
		this.red = (float) red / 255.0f;
		this.green = (float) green / 255.0f;
		this.blue = (float) blue / 255.0f;
		this.opacity = (float) opacity / 255.0f;
	}
	
	public WsColor(int red, int green, int blue) {
		this(red, green, blue, 255);
	}
	
	public WsColor(String colorstring) {
		colorstring = colorstring.toLowerCase();
		//Hex RGB
		if(colorstring.matches("#[\\dabcdef]{6}")) {
			this.red = ((float) Integer.parseInt(colorstring.substring(1, 3), 16)) / 255.0f;
			this.green = ((float) Integer.parseInt(colorstring.substring(3, 5), 16)) / 255.0f;
			this.blue = ((float) Integer.parseInt(colorstring.substring(5, 7), 16)) / 255.0f;
			this.opacity = 1.0f;
		}
		//Hex RGBA
		else if(colorstring.matches("#[\\dabcdef]{8}")) {
			this.red = ((float) Integer.parseInt(colorstring.substring(1, 3), 16)) / 255.0f;
			this.green = ((float) Integer.parseInt(colorstring.substring(3, 5), 16)) / 255.0f;
			this.blue = ((float) Integer.parseInt(colorstring.substring(5, 7), 16)) / 255.0f;
			this.opacity = ((float) Integer.parseInt(colorstring.substring(7, 9), 16)) / 255.0f;
		}
		//Int RGB
		else if(colorstring.matches("\\d{1,3},\\d{1,3},\\d{1,3}")) {
			String[] colorValues = colorstring.split(",");
			this.red = ((float) Integer.parseInt(colorValues[0])) / 255.0f;
			this.green = ((float) Integer.parseInt(colorValues[1])) / 255.0f;
			this.blue = ((float) Integer.parseInt(colorValues[2])) / 255.0f;
			this.opacity = 1.0f;
		}
		//Int RGBA
		else if(colorstring.matches("\\d{1,3},\\d{1,3},\\d{1,3},\\d{1,3}")) {
			String[] colorValues = colorstring.split(",");
			this.red = ((float) Integer.parseInt(colorValues[0])) / 255.0f;
			this.green = ((float) Integer.parseInt(colorValues[1])) / 255.0f;
			this.blue = ((float) Integer.parseInt(colorValues[2])) / 255.0f;
			this.opacity = ((float) Integer.parseInt(colorValues[3])) / 255.0f;
		}
		else {
			throw new IllegalArgumentException("Invalid hex color specification");
		}
	}
	
	public float[] getColorComponents() {
		return new float[] {red, green, blue, opacity};
	}
	
	public Color getFxColor() {
		if(fxColor == null) {
			fxColor = new Color(red, green, blue, opacity);
		}
		return fxColor;
	}
}
